<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

  require('includes/application_top.php');
  if ($cart->count_contents() < 1) {
    tep_redirect(tep_href_link(FILENAME_SHOPPING_CART));
  }
		
		if (!tep_session_is_registered('customer_id')) {
		 tep_redirect(tep_href_link('direct_checkout.php'));
		}
		
// redirect the customer to a friendly cookie-must-be-enabled page if cookies are disabled (or the session has not started)
  if ($session_started == false) {
    tep_redirect(tep_href_link(FILENAME_COOKIE_USAGE));
  }

  if (!tep_session_is_registered('comments')) tep_session_register('comments');
  if (tep_not_null($HTTP_POST_VARS['comments'])) {
    $comments = tep_db_prepare_input($HTTP_POST_VARS['comments']);
  }


  require(DIR_WS_CLASSES . 'order.php');
  $order = new order;

		if (!tep_session_is_registered('cartID')) tep_session_register('cartID');
		$cartID = $cart->cartID;

  if (tep_session_is_registered('shipping')) tep_session_unregister('shipping');

  $total_weight = $cart->show_weight();
  $total_count = $cart->count_contents();
		
		require(DIR_WS_CLASSES . 'shipping.php');
		$shipping_modules = new shipping();
		$quotes = $shipping_modules->quote();
		
		//check free shipping
	 if ( defined('MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING') && (MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING == 'true') ) {
    $pass = false;

    switch (MODULE_ORDER_TOTAL_SHIPPING_DESTINATION) {
      case 'national':
        if ($order->delivery['country_id'] == STORE_COUNTRY) {
          $pass = true;
        }
        break;
      case 'international':
        if ($order->delivery['country_id'] != STORE_COUNTRY) {
          $pass = true;
        }
        break;
      case 'both':
        $pass = true;
        break;
    }

    $free_shipping = false;
    if ( ($pass == true) && ($order->info['total'] >= MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_OVER) ) {
      $free_shipping = true;

      include(DIR_WS_LANGUAGES . $language . '/modules/order_total/ot_shipping.php');
    }
  } else {
    $free_shipping = false;
  }
 //end free shipping check
		
		//load payment
		include(DIR_WS_CLASSES . 'payment.php');
		$payment_modules = new payment();
		$selection = $payment_modules->selection();


$error = false;
if (isset($HTTP_GET_VARS['action']) && ($HTTP_GET_VARS['action'] == 'process') && isset($HTTP_POST_VARS['formid']) && ($HTTP_POST_VARS['formid'] == $sessiontoken)&&($_POST['address']=='')) {
 if (ACCOUNT_GENDER == 'true') {
      if (isset($HTTP_POST_VARS['gender'])) {
        $gender = tep_db_prepare_input($HTTP_POST_VARS['gender']);
      } else {
        $gender = false;
      }
    }
 if (ACCOUNT_COMPANY == 'true') $company = tep_db_prepare_input($HTTP_POST_VARS['company']);
 if (ACCOUNT_STATE == 'true') {
      $state = tep_db_prepare_input($HTTP_POST_VARS['state']);
      if (isset($HTTP_POST_VARS['zone_id'])) {
        $zone_id = tep_db_prepare_input($HTTP_POST_VARS['zone_id']);
      } else {
        $zone_id = false;
      }
    }
	 $firstname = tep_db_prepare_input($HTTP_POST_VARS['firstname']);
  $lastname = tep_db_prepare_input($HTTP_POST_VARS['lastname']);
  $street_address = tep_db_prepare_input($HTTP_POST_VARS['street_address']);
		$postcode = tep_db_prepare_input($HTTP_POST_VARS['postcode']);
  $city = tep_db_prepare_input($HTTP_POST_VARS['city']);
  $country = tep_db_prepare_input($HTTP_POST_VARS['country']);
  $telephone = tep_db_prepare_input($HTTP_POST_VARS['telephone']);
  $fax = tep_db_prepare_input($HTTP_POST_VARS['fax']);
//verify
    if (ACCOUNT_GENDER == 'true') {
      if ( ($gender != 'm') && ($gender != 'f') ) {
        $error = true;

        $messageStack->add('create_account', ENTRY_GENDER_ERROR);
      }
    }

    if (strlen($firstname) < ENTRY_FIRST_NAME_MIN_LENGTH) {
      $error = true;

      $messageStack->add('create_account', ENTRY_FIRST_NAME_ERROR);
    }

    if (strlen($lastname) < ENTRY_LAST_NAME_MIN_LENGTH) {
      $error = true;

      $messageStack->add('create_account', ENTRY_LAST_NAME_ERROR);
    }
			
			if (strlen($street_address) < ENTRY_STREET_ADDRESS_MIN_LENGTH) {
      $error = true;

      $messageStack->add('create_account', ENTRY_STREET_ADDRESS_ERROR);
    }

    if (strlen($postcode) < ENTRY_POSTCODE_MIN_LENGTH) {
      $error = true;

      $messageStack->add('create_account', ENTRY_POST_CODE_ERROR);
    }

    if (strlen($city) < ENTRY_CITY_MIN_LENGTH) {
      $error = true;

      $messageStack->add('create_account', ENTRY_CITY_ERROR);
    }

    if (is_numeric($country) == false) {
      $error = true;

      $messageStack->add('create_account', ENTRY_COUNTRY_ERROR);
    }

    if (ACCOUNT_STATE == 'true') {
      // +Country-State Selector
      if ($zone_id == 0) {
      // -Country-State Selector

        if (strlen($state) < ENTRY_STATE_MIN_LENGTH) {
          $error = true;

          $messageStack->add('create_account', ENTRY_STATE_ERROR);
        }
      }
    }
				
    if (strlen($telephone) < ENTRY_TELEPHONE_MIN_LENGTH) {
      $error = true;

      $messageStack->add('create_account', ENTRY_TELEPHONE_NUMBER_ERROR);
    }
//verify end
if (strlen($_POST['shipping']) == 0) {
      $error = true;
      $messageStack->add('create_account', 'Plesse select your shipping method ');
}

if (strlen($_POST['payment']) == 0) {
      $error = true;
      $messageStack->add('create_account', 'Plesse select your payment method ');
}

//create account
  if ($error == false) {
      $sql_data_array = array('customers_firstname' => $firstname,
                              'customers_lastname' => $lastname,
                              'customers_telephone' => $telephone,
                              'customers_fax' => $fax);
					if (ACCOUNT_GENDER == 'true') $sql_data_array['customers_gender'] = $gender;
					tep_db_perform(TABLE_CUSTOMERS, $sql_data_array, 'update', "customers_id = '" . (int)$customer_id . "'");
					
					$sql_data_array = array('customers_id' => $customer_id,
                              'entry_firstname' => $firstname,
                              'entry_lastname' => $lastname,
                              'entry_street_address' => $street_address,
                              'entry_postcode' => $postcode,
                              'entry_city' => $city,
                              'entry_country_id' => $country);
				  if (ACCOUNT_GENDER == 'true') $sql_data_array['entry_gender'] = $gender;
      if (ACCOUNT_COMPANY == 'true') $sql_data_array['entry_company'] = $company;
      if (ACCOUNT_SUBURB == 'true') $sql_data_array['entry_suburb'] = $suburb;
      if (ACCOUNT_STATE == 'true') {
        if ($zone_id > 0) {
          $sql_data_array['entry_zone_id'] = $zone_id;
          $sql_data_array['entry_state'] = '';
        } else {
          $sql_data_array['entry_zone_id'] = '0';
          $sql_data_array['entry_state'] = $state;
        }
      }
					tep_db_perform(TABLE_ADDRESS_BOOK, $sql_data_array);
					$address_id = tep_db_insert_id();
					
     tep_db_query("update " . TABLE_CUSTOMERS . " set customers_default_address_id = '" . (int)$address_id . "' where customers_id = '" . (int)$customer_id . "'");
     tep_db_query("insert into " . TABLE_CUSTOMERS_INFO . " (customers_info_id, customers_info_number_of_logons, customers_info_date_account_created) values ('" . (int)$customer_id . "', '0', now())");
					
					$customer_first_name = $firstname;
					$customer_default_address_id = $address_id;
     $customer_country_id = $country;
     $customer_zone_id = $zone_id;
     tep_session_register('customer_first_name');
     tep_session_register('customer_default_address_id');
     tep_session_register('customer_country_id');
     tep_session_register('customer_zone_id');
					$sessiontoken = md5(tep_rand() . tep_rand() . tep_rand() . tep_rand());
					//end db
					//send email
					 if (ACCOUNT_GENDER == 'true') {
         if ($gender == 'm') {
           $email_text = sprintf(EMAIL_GREET_MR, $lastname);
         } else {
           $email_text = sprintf(EMAIL_GREET_MS, $lastname);
         }
      } else {
        $email_text = sprintf(EMAIL_GREET_NONE, $firstname);
      }

      $email_text .= EMAIL_WELCOME . EMAIL_TEXT . EMAIL_CONTACT . EMAIL_WARNING;
      tep_mail($name, $email_address, EMAIL_SUBJECT, $email_text, STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS);
    //end email
		}
}

if ($error == false){
//REGISTER PAYMENT
				if (!tep_session_is_registered('payment')) tep_session_register('payment');
				
				if (isset($HTTP_POST_VARS['payment'])) $payment = $HTTP_POST_VARS['payment'];
				if (!tep_session_is_registered('billto')) {
								tep_session_register('billto');
				    
				} 
				$billto = $customer_default_address_id;
//END REGISTER PAYMENT AND SHIPPING

//REGISTER SHIPPING
if (!tep_session_is_registered('shipping')) tep_session_register('shipping');
if (!tep_session_is_registered('sendto')) {
	tep_session_register('sendto');
	$sendto = $customer_default_address_id;
}
elseif($_POST['address']&&is_numeric($_POST['address'])){
	$sendto = (int)$_POST['address'];
}

if ( (tep_count_shipping_modules() > 0)) {

      if ( (isset($HTTP_POST_VARS['shipping'])) && (strpos($HTTP_POST_VARS['shipping'], '_')) ) {

        $shipping = $HTTP_POST_VARS['shipping'];

        list($module, $method) = explode('_', $shipping);

        if ( is_object($$module) || ($shipping == 'free_free') ) {

          if ($shipping == 'free_free') {

            $quote[0]['methods'][0]['title'] = FREE_SHIPPING_TITLE;

            $quote[0]['methods'][0]['cost'] = '0';

          } else {

            $quote = $shipping_modules->quote($method, $module);

          }

          if (isset($quote['error'])) {

            tep_session_unregister('shipping');

          } else {

            if ( (isset($quote[0]['methods'][0]['title'])) && (isset($quote[0]['methods'][0]['cost'])) ) {

              $shipping = array('id' => $shipping,

                                'title' => (($free_shipping == true) ?  $quote[0]['methods'][0]['title'] : $quote[0]['module'] . ' (' . $quote[0]['methods'][0]['title'] . ')'),

                                'cost' => $quote[0]['methods'][0]['cost']);

              tep_redirect(tep_href_link(FILENAME_CHECKOUT_CONFIRMATION, '', 'SSL'));

            }

          }

        } else {

          tep_session_unregister('shipping');

        }

      }

    }
}
  $breadcrumb->add('Login or Register', tep_href_link('direct_checkout.php', '', 'SSL'));
		$breadcrumb->add('Shipping & Payment', tep_href_link('direct_option.php', '', 'SSL'));
  require(DIR_WS_INCLUDES . 'template_top.php');
?>
<?php require('includes/form_check.js.php'); ?>
<div class="newcheckout">
<div id="bodyContent" class="grid_20 push_4" style="padding-top:10px">
  <div class="contentContainer" style="padding:0px 20px">
  <h2 class="checkout_till">Shipping Address</h2>
  <?php
  if ($messageStack->size('create_account') > 0) {
    echo $messageStack->output('create_account');
  }
  ?>
<?php 
  $account_query = tep_db_query("select customers_gender, customers_firstname, customers_lastname, customers_telephone, customers_fax from " . TABLE_CUSTOMERS . " where customers_id = '" . (int)$customer_id . "'");
  $account = tep_db_fetch_array($account_query);
		$gender = $account['customers_gender'];
		$firstname = $account['customers_firstname'];
		$lastname = $account['customers_lastname'];
		$telephone = $account['customers_telephone'];
		$fax = $account['customers_fax'];
?>
		<?php echo tep_draw_form('checkout', tep_href_link('direct_option.php', 'action=process', 'SSL'), 'post', 'onsubmit="return check_form(checkout);"', true); ?>
		<?php 
  if (tep_session_is_registered('customer_default_address_id')&&($customer_default_address_id > 0)) { 
  ?>
				<?php 
				 $address_book_query = tep_db_query("select * from address_book where customers_id = '" . (int)$customer_id . "'");
					
					while($address_book = tep_db_fetch_array($address_book_query)){
					echo '<div style="background:#f5f5f5; padding:10px; margin:5px; 0px">
					<input type="radio" name="address" '.($sendto==$address_book['address_book_id']?'checked="checked"':'').' value="'.$address_book['address_book_id'].'" />&nbsp;<b>'.$address_book['entry_firstname'].'  '.$address_book['entry_lastname'].'</b>
					<div style="margin-left:22px">
					'.$address_book['entry_street_address'].'<br />
					'.$address_book['entry_city'].', '.tep_get_zone_name($address_book['entry_country_id'],$address_book['entry_zone_id'],'').', '.tep_get_country_name($address_book['entry_country_id']).', '.$address_book['entry_postcode'].'.<br /> 
					Phone: <b>'.$telephone.'</b>
					</div>
					</div>
					';
					}
				?>
    <div style="text-align:right"><?php echo tep_draw_button('Add New Address', 'home', tep_href_link(FILENAME_CHECKOUT_SHIPPING_ADDRESS, '', 'SSL')); ?></div>
<?php 
}else {
?>
<table border="0" cellspacing="0" cellpadding="2" style="margin:0px 10px">
<?php
  if (ACCOUNT_GENDER == 'true') {
    if (isset($gender)) {
      $male = ($gender == 'm') ? true : false;
      $female = ($gender == 'f') ? true : false;
    } else {
      $male = false;
      $female = false;
    }
?>

    <tr>
      <td class="fieldKey"><?php echo ENTRY_GENDER; ?></td>
      <td class="fieldValue fradio"><?php echo tep_draw_radio_field('gender', 'm', $male) . '&nbsp;&nbsp;' . MALE . '&nbsp;&nbsp;' . tep_draw_radio_field('gender', 'f', $female) . '&nbsp;&nbsp;' . FEMALE . '&nbsp;' . (tep_not_null(ENTRY_GENDER_TEXT) ? '<span class="inputRequirement">' . ENTRY_GENDER_TEXT . '</span>': ''); ?></td>
    </tr>

<?php
  }
?>

    <tr>
      <td class="fieldKey"><?php echo ENTRY_FIRST_NAME; ?></td>
      <td class="fieldValue"><?php echo tep_draw_input_field('firstname',$firstname) . '&nbsp;' . (tep_not_null(ENTRY_FIRST_NAME_TEXT) ? '<span class="inputRequirement">' . ENTRY_FIRST_NAME_TEXT . '</span>': ''); ?></td>
    </tr>
    <tr>
      <td class="fieldKey"><?php echo ENTRY_LAST_NAME; ?></td>
      <td class="fieldValue"><?php echo tep_draw_input_field('lastname',$lastname) . '&nbsp;' . (tep_not_null(ENTRY_LAST_NAME_TEXT) ? '<span class="inputRequirement">' . ENTRY_LAST_NAME_TEXT . '</span>': ''); ?></td>
    </tr>

<?php
  if (ACCOUNT_COMPANY == 'true') {
?>

    <tr>
      <td class="fieldKey"><?php echo ENTRY_COMPANY; ?></td>
      <td class="fieldValue"><?php echo tep_draw_input_field('company') . '&nbsp;' . (tep_not_null(ENTRY_COMPANY_TEXT) ? '<span class="inputRequirement">' . ENTRY_COMPANY_TEXT . '</span>': ''); ?></td>
    </tr>

<?php
  }
?>

    <tr>
      <td class="fieldKey"><?php echo ENTRY_STREET_ADDRESS; ?></td>
      <td class="fieldValue"><?php echo tep_draw_input_field('street_address') . '&nbsp;' . (tep_not_null(ENTRY_STREET_ADDRESS_TEXT) ? '<span class="inputRequirement">' . ENTRY_STREET_ADDRESS_TEXT . '</span>': ''); ?></td>
    </tr>

<?php
  if (ACCOUNT_SUBURB == 'true') {
?>

    <tr>
      <td class="fieldKey"><?php echo ENTRY_SUBURB; ?></td>
      <td class="fieldValue"><?php echo tep_draw_input_field('suburb') . '&nbsp;' . (tep_not_null(ENTRY_SUBURB_TEXT) ? '<span class="inputRequirement">' . ENTRY_SUBURB_TEXT . '</span>': ''); ?></td>
    </tr>

<?php
  }
?>

    <tr>
      <td class="fieldKey"><?php echo ENTRY_POST_CODE; ?></td>
      <td class="fieldValue"><?php echo tep_draw_input_field('postcode') . '&nbsp;' . (tep_not_null(ENTRY_POST_CODE_TEXT) ? '<span class="inputRequirement">' . ENTRY_POST_CODE_TEXT . '</span>': ''); ?></td>
    </tr>
    <tr>
      <td class="fieldKey"><?php echo ENTRY_CITY; ?></td>
      <td class="fieldValue"><?php echo tep_draw_input_field('city') . '&nbsp;' . (tep_not_null(ENTRY_CITY_TEXT) ? '<span class="inputRequirement">' . ENTRY_CITY_TEXT . '</span>': ''); ?></td>
    </tr>

<?php
  if (ACCOUNT_STATE == 'true') {
?>

    <tr>
      <td class="fieldKey"><?php echo ENTRY_STATE; ?></td>
        <td class="fieldValue">
         <span id="states"><?php echo ajax_get_zones_html((isset($_POST['country'])?$_POST['country']:STORE_COUNTRY),'',false);?></span>&nbsp;<span class="inputRequirement">*</span>
        </td>
    </tr>

<?php
  }
?>

    <tr>
      <td class="fieldKey"><?php echo ENTRY_COUNTRY; ?></td>
      <td class="fieldValue"><?php echo tep_get_country_list('country',STORE_COUNTRY,'onchange="getStates(this.value, \'states\');"') . '&nbsp;' . (tep_not_null(ENTRY_COUNTRY_TEXT) ? '<span class="inputRequirement">' . ENTRY_COUNTRY_TEXT . '</span>': ''); ?><span id="indicator"><?php echo tep_image(DIR_WS_IMAGES . 'layout/indicator.gif'); ?></span></td>
    </tr>
    <tr>
        <td class="fieldKey"><?php echo ENTRY_TELEPHONE_NUMBER; ?></td>
        <td class="fieldValue"><?php echo tep_draw_input_field('telephone',$telephone) . '&nbsp;' . (tep_not_null(ENTRY_TELEPHONE_NUMBER_TEXT) ? '<span class="inputRequirement">' . ENTRY_TELEPHONE_NUMBER_TEXT . '</span>': ''); ?></td>
      </tr>
      <tr>
        <td class="fieldKey"><?php echo ENTRY_FAX_NUMBER; ?></td>
        <td class="fieldValue"><?php echo tep_draw_input_field('fax',$fax) . '&nbsp;' . (tep_not_null(ENTRY_FAX_NUMBER_TEXT) ? '<span class="inputRequirement">' . ENTRY_FAX_NUMBER_TEXT . '</span>': ''); ?></td>
    </tr>
  </table>
<?php } ?>  
  <h2 class="checkout_till">Shipping Method </h2>
  	<table border="0" width="100%" cellspacing="0" cellpadding="2">
	<?php 
	if ($free_shipping == true) {
?>

      <tr>
        <td><?php echo FREE_SHIPPING_TITLE; ?>&nbsp;<?php echo $quotes[$i]['icon']; ?></td>
      </tr>
      <tr id="defaultSelected" class="moduleRowSelected">
        <td style="padding-left: 15px;"><?php echo sprintf(FREE_SHIPPING_DESCRIPTION, $currencies->format(MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_OVER)) . tep_draw_hidden_field('shipping', 'free_free'); ?></td>
      </tr>

<?php
    } else { 
?>				
<?php
	if(sizeof($quotes)==1){
	echo '
	 <tr><td colspan="3">'.TEXT_ENTER_SHIPPING_INFORMATION.'</td></tr>
		<tr><td></td><td colspan="3" class="main"><b>'.$quotes[0]['module'].'</b></td></tr>
		<tr>
		<td width="10"></td><td class="main">'.tep_draw_hidden_field('shipping',$quotes[0]['id'].'_'.$quotes[0]['methods'][0]['id']).'&nbsp;&nbsp;'.$quotes[0]['methods'][0]['title'].'</td><td class="main">'.$currencies->format(tep_add_tax($quotes[0]['methods'][0]['cost'], $quotes[0]['tax'])).'</td>
		</tr>';
	}

	else{
	   for($i=0,$n=sizeof($quotes); $i<$n; $i++){
      echo '<tr><td></td><td colspan="3" class="main">'.$quotes[$i]['module'].'</td></tr>';
	     if(isset($quotes[$i]['error']))
	     echo '<tr><td></td><td colspan="3">'.$quotes[$i]['error'].'</td></tr>';
	     else{
       for($j=0,$k=sizeof($quotes[$i]['methods']); $j<$k; $j++){
        echo '
        <tr>
        <td width="10"></td><td class="main">'.tep_draw_radio_field('shipping',$quotes[$i]['id'].'_'.$quotes[$i]['methods'][$j]['id'], false, 'id="'.$quotes[$i]['id'].'_'.$quotes[$i]['methods'][$j]['id'].'"').'&nbsp;&nbsp;'.$quotes[$i]['methods'][$j]['title'].'</td><td class="main" id="'.$quotes[$i]['id'].'_'.$quotes[$i]['methods'][$j]['id'].'_price">'.$currencies->format(tep_add_tax($quotes[$i]['methods'][$j]['cost'], (isset($quotes[$i]['tax']) ? $quotes[$i]['tax'] : 0))).'</td>
        </tr>
        <tr><td></td><td colspan="3" height="10"></td></tr>
      		';
      }
	  }
  }
	}	
	?>
<?php } ?>	
</td></tr>
	</table>

  <h2 class="checkout_till">Payment Method  </h2>
  <table border="0" width="100%" cellspacing="0" cellpadding="2">
		<?php
		for($i=0,$n=sizeof($selection); $i<$n; $i++){
		
				echo '
		  <tr><td colspan="3" height="5"></td></tr>
				<tr>
					<td width="10"></td><td class="main" width="10">'.((sizeof($selection)>1)? tep_draw_radio_field('payment',$selection[$i]['id'],false,'id="'.$selection[$i]['id'].'"'):tep_draw_hidden_field('payment', $selection[$i]['id'])).'</td><td class="main">'.$selection[$i]['module'].'</td>
				</tr>';
						if(sizeof($selection[$i]['fields'])>0){
			  	$out_fields.= '<table border="0" class="main" cellpadding="2" cellspacing="0">';
						for ($j=0, $n2=sizeof($selection[$i]['fields']); $j<$n2; $j++) {
						$out_fields.= '
						<tr>           
									<td class="main" valign="top" style="padding-top:15px">'.$selection[$i]['fields'][$j]['title'].'</td>
									<td class="main">'.$selection[$i]['fields'][$j]['field'].'</td>
						</tr>';
						}
				$out_fields.= '</table>';
				echo '<tr><td></td><td></td><td>'.$out_fields.'</td></tr>';
				$out_fields = '';
				}
				?>
						<tr><td colspan="3" height="5"></td></tr>
		<?php
		}
		?>
  </table>
<?php
/* kgt - discount coupons */
    if( MODULE_ORDER_TOTAL_DISCOUNT_COUPON_STATUS == 'true' ) {
?>
  <div class="contentText" style="border:1px solid #ddd; width:200px; background:#f5f5f5; padding:10px; margin:10px">
        Enter your coupon code if you have one <br>
        <?php echo '<b>'.ENTRY_DISCOUNT_COUPON.'</b> '.tep_draw_input_field('coupon', '', 'size="10"', $coupon); ?>
  </div>
<?php
    }
/* end kgt - discount coupons */
?>
  <h2 class="checkout_till">Add Comments About Your Order</h2>
  <div class="contentText">
    <?php echo tep_draw_textarea_field('comments', 'soft', '60', '5', $comments); ?>
  </div>
  <?php echo tep_draw_button(IMAGE_BUTTON_CONTINUE, 'triangle-1-e', null, 'primary'); ?>
  </form>
  </div>
</div>
<div id="columnLeft" class="grid_4 pull_20">
  <div class="contentContainer">
  <h2 class="left_h2">Shopping Cart</h2>
  <div style="width:172px; margin:auto; background:#f5f5f5; padding:10px">
		<?php
        $cart_contents_string = '<table border="0" width="100%" cellspacing="0" cellpadding="0">';
        $products = $cart->get_products();
        for ($i=0, $n=sizeof($products); $i<$n; $i++) {
          $cart_contents_string .= '<tr><td align="right" valign="top">';

          if ((tep_session_is_registered('new_products_id_in_cart')) && ($new_products_id_in_cart == $products[$i]['id'])) {
            $cart_contents_string .= '<span class="newItemInCart">';
          }

          $cart_contents_string .= $products[$i]['quantity'] . '&nbsp;x&nbsp;';

          if ((tep_session_is_registered('new_products_id_in_cart')) && ($new_products_id_in_cart == $products[$i]['id'])) {
            $cart_contents_string .= '</span>';
          }

          $cart_contents_string .= '</td><td valign="top"><a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $products[$i]['id']) . '">';

          if ((tep_session_is_registered('new_products_id_in_cart')) && ($new_products_id_in_cart == $products[$i]['id'])) {
            $cart_contents_string .= '<span class="newItemInCart">';
          }

          $cart_contents_string .= $products[$i]['name'];

          if ((tep_session_is_registered('new_products_id_in_cart')) && ($new_products_id_in_cart == $products[$i]['id'])) {
            $cart_contents_string .= '</span>';
          }

          $cart_contents_string .= '</a></td></tr>';

          if ((tep_session_is_registered('new_products_id_in_cart')) && ($new_products_id_in_cart == $products[$i]['id'])) {
            tep_session_unregister('new_products_id_in_cart');
          }
        }

        $cart_contents_string .= '<tr><td colspan="2" style="padding-top: 5px; padding-bottom: 2px;">' . tep_draw_separator() . '</td></tr>' .
                                 '<tr><td colspan="2" align="right">Sub-Total: ' . $currencies->format($cart->show_total()) . '</td></tr>' .
                                 '</table>';
						  echo $cart_contents_string;
						?>
      </div>
  </div>
</div>
<div class="clear"></div>
</div>
<?php
  require(DIR_WS_INCLUDES . 'template_bottom.php');
  require(DIR_WS_INCLUDES . 'application_bottom.php');
?>