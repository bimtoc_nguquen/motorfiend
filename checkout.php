<?php
/*
 * Dynamo Checkout by Dynamo Effects
 * Copyright 2008 - Dynamo Effects.  All Rights Reserved
 * http://www.dynamoeffects.com
 */

if (isset($_GET['blank']) && $_GET['blank'] == '1')
    die('<html></html>');

error_reporting(E_ALL ^ E_WARNING ^ E_NOTICE);
@ini_set('display_errors', '1');
define('FILENAME_CHECKOUT', 'checkout.php');

require_once('includes/checkout/checkout.config.php');

if (isset($_GET['payment'])) {
    $payment = preg_replace('/[^A-Za-z0-9_]/', '', $_GET['payment']);
}

define('FILENAME_CHECKOUT', 'checkout.php');
define('FILENAME_CHECKOUT_PAYMENT', 'checkout.php');
define('FILENAME_CHECKOUT_PAYMENT_ADDRESS', 'checkout.php');
define('FILENAME_CHECKOUT_PROCESS', 'checkout_process.php');
define('FILENAME_CHECKOUT_SHIPPING', 'checkout.php');
define('FILENAME_CHECKOUT_SHIPPING_ADDRESS', 'checkout.php');
define('FILENAME_CHECKOUT_DIRECT', 'direct_checkout.php');

if (CHECKOUT_CONTRIB_CCGV || CHECKOUT_CONTRIB_DISCOUNT_COUPON) {
    define('CHECKOUT_ENABLE_COUPON', true);
} else {
    define('CHECKOUT_ENABLE_COUPON', false);
}

//Disables Express Checkout button in PayPal WPP contribution
define('MODULE_PAYMENT_PAYPAL_DP_BUTTON_PAYMENT_PAGE', 'No');

require_once('includes/application_top.php');
if (!tep_session_is_registered('customer_id')) {
    tep_redirect(tep_href_link(FILENAME_CHECKOUT_DIRECT));
}

$ec_query = tep_db_query(
        "SELECT configuration_id " .
        "FROM " . TABLE_CONFIGURATION . " " .
        "WHERE configuration_key = 'MODULE_PAYMENT_PAYPAL_DP_BUTTON_PAYMENT_PAGE' " .
        "  AND configuration_value = 'Yes' " .
        "LIMIT 1"
);

if (tep_db_num_rows($ec_query) > 0) {
    define('MODULE_PAYMENT_PAYPAL_DP_BUTTON_PAYMENT_METHOD', 'Yes');
} else {
    define('MODULE_PAYMENT_PAYPAL_DP_BUTTON_PAYMENT_METHOD', 'No');
}

$express_checkout_config = array(
    'enabled' => $ec_enabled,
    'processing' => ($ec_enabled && !isset($_GET['ec_cancel']) && (tep_session_is_registered('paypal_ec_payer_id') || tep_session_is_registered('paypal_ec_payer_info'))),
    'payment_method' => (MODULE_PAYMENT_PAYPAL_DP_BUTTON_PAYMENT_METHOD == 'Yes' ? true : false),
    'address_editable' => (MODULE_PAYMENT_PAYPAL_EC_ADDRESS_OVERRIDE == 'Store' ? true : false)
);

if ($cart->count_contents() < 1) {
    tep_redirect(tep_href_link(FILENAME_REAL_SHOPPING_CART));
}

if (($_SERVER['HTTPS'] != '1' && $_SERVER['HTTPS'] != 'on' && $_SERVER['SERVER_PORT'] != 443) && ENABLE_SSL) {
    header('Location: ' . tep_href_link(FILENAME_CHECKOUT, '', 'SSL'));
}

if (CHECKOUT_DEBUG_MODE) {
    error_reporting(E_ALL ^ E_WARNING ^ E_NOTICE);
    @ini_set('display_errors', '1');
} else {
    error_reporting(0);
    @ini_set('display_errors', '0');
}

$shipping_enabled = false;
$shipping_dynamic = CHECKOUT_DYNAMIC_SHIPPING;

if (($cart->show_weight() > 0 || !CHECKOUT_DYNAMIC_SHIPPING) && strpos($cart->content_type, 'virtual') === false) {
    $shipping_enabled = true;
}

if (!function_exists('get_zone')) {

    function get_zone($id, $state) {
        return true;
    }

}

$navigation->set_snapshot();

require_once('includes/classes/http_client.php');
require_once(CHECKOUT_WS_INCLUDES . 'languages/' . $language . '/checkout.php');

include(DIR_WS_LANGUAGES . $language . '/checkout_shipping.php');
include(DIR_WS_LANGUAGES . $language . '/checkout_payment.php');
include(DIR_WS_LANGUAGES . $language . '/checkout_confirmation.php');
include(DIR_WS_LANGUAGES . $language . '/checkout_process.php');

require_once(DIR_WS_CLASSES . 'payment.php');
include(CHECKOUT_WS_LIB . 'checkout.class.php');

$checkout = new checkout;

if (isset($_POST['bs'])) {
    if (!tep_session_is_registered('customer_zone_id'))
        tep_session_register('customer_zone_id');

    $_SESSION['customer_zone_id'] = (is_numeric($_POST['bs']) && (int) $_POST['bs'] > 0 ? (int) $_POST['bs'] : '');
    $customer_zone_id = $_SESSION['customer_zone_id'];
}

if (isset($_POST['bcn'])) {
    $customer_country_id = (isset($_POST['bcn']) && (int) $_POST['bcn'] > 0 ? (int) $_POST['bcn'] : '');
}

require_once(DIR_WS_CLASSES . 'order.php');
$order = new order;
function update_order_not_checkout(){
          global $order,$customer_id;

          //$info_c = tep_db_query(" select * from customers where customers_id = ". $customer_id);
          //$info_cc = tep_db_fetch_array($info_c);

          //$user_email = $info_cc['customers_email_address'];
          //$user_name = $info_cc['customers_firstname']." ".$info_cc['customers_lastname'];
          //$time = time();	
          //$date_time_add = date('d/m/Y',$time);
          for ($i=0, $n=sizeof($order->products); $i<$n; $i++) {
              $product_id = $order->products[$i]['id'];
              //$product_price = $order->products[$i]['price'];
              //$products_name = $order->products[$i]['name'];
              $products_qty = $order->products[$i]['qty'];
              $product_name = tep_db_query(" select id from " . TABLE_PRODUCT_ADD_CART." where product_id = ".(int)$product_id." and user_id = ". $customer_id);
              $product_c = tep_db_fetch_array($product_name);
              if(!$product_c){
                  tep_db_query("insert into " . TABLE_PRODUCT_ADD_CART." (product_id, date_time_add, product_qty, user_id) values
                ('" .(int)$product_id . "',now(),'".$products_qty."','".$customer_id."')");
              }
          }
      }
      if(tep_session_is_registered('customer_id')) {
          update_order_not_checkout();
      }
$any_out_of_stock = false;
if (STOCK_CHECK == 'true') {
    for ($i = 0, $n = sizeof($order->products); $i < $n; $i++) {
        if (tep_check_stock($order->products[$i]['id'], $order->products[$i]['qty'])) {
            $any_out_of_stock = true;
        }
    }

    if ((STOCK_ALLOW_CHECKOUT != 'true') && ($any_out_of_stock == true)) {
        tep_redirect(tep_href_link(FILENAME_REAL_SHOPPING_CART));
    }
}

require_once(DIR_WS_CLASSES . 'order_total.php');
$order_total_modules = new order_total;

$total_weight = $cart->show_weight();
$total_count = $cart->count_contents();

$breadcrumb->add(NAVBAR_TITLE, tep_href_link(FILENAME_CHECKOUT, '', 'SSL'));

if (isset($_POST['x'])) {
    switch ($_POST['x']) {

        case 'order_total':
            header('Content-type: application/json; charset=UTF-8');
            $checkout->order_total();
            break;
        case 'get_zones':
            header('Content-type: application/json; charset=UTF-8');
            $checkout->get_zones($_POST['c'], $_POST['n'], $_POST['d']);
            break;
        case 'get_payment_shipping':
            header('Content-type: application/json; charset=UTF-8');
            $checkout->get_payment_shipping();
            break;
        case 'apply_coupon':
            header('Content-type: application/json; charset=UTF-8');
            $checkout->apply_coupon($_POST['coupon']);
            break;
        case 'remove_coupon':
            header('Content-type: application/json; charset=UTF-8');
            $checkout->remove_coupon();
            break;
        case 'update_cart':
            header('Content-type: text/html; charset=UTF-8');
            $checkout->update_cart();
            break;
        case 'login_window':
            header('Content-type: text/html; charset=UTF-8');
            $checkout->login_window();
            break;
        case 'remove_product':
            header('Content-type: application/json; charset=UTF-8');
            $checkout->remove_product();
            break;
        case 'log_error':
            header('Content-type: application/json; charset=UTF-8');
            $checkout->log_error($_POST['error']);
            break;
        case 'apply_points':
            header('Content-type: application/json; charset=UTF-8');
            $checkout->apply_points($_POST['s']);
            break;
        case 'apply_giftwrap':
            header('Content-type: application/json; charset=UTF-8');
            $checkout->apply_giftwrap($_POST['gw']);
            break;
    }

    die();
    ob_flush();
}

if (!tep_session_is_registered('shipping'))
    tep_session_unregister('shipping');
if (!tep_session_is_registered('shipping'))
    tep_session_register('shipping');


if (!tep_session_is_registered('cartID'))
    tep_session_register('cartID');
$cartID = $cart->cartID;

$error = false;

if (isset($_GET['action']) && ($_GET['action'] == 'login')) {
    $email_address = tep_db_prepare_input($_POST['login_email_address']);
    $password = tep_db_prepare_input($_POST['login_password']);

    $error = do_login($email_address, $password);

    if ($error == true) {
        $messageStack->add('checkout', TEXT_LOGIN_ERROR);
    }
}

if ($order->delivery['zone_id'] > 0) {
    $ship_zone_id = $order->delivery['zone_id'];
} elseif ($order->delivery['state'] != '' && is_numeric($order->delivery['country']['id'])) {
    $ship_zone_id = get_zone($order->delivery['country']['id'], $order->delivery['state']);
}
if ($order->billing['zone_id'] > 0) {
    $bill_zone_id = $order->billing['zone_id'];
} elseif ($order->billing['state'] != '' && is_numeric($order->billing['country']['id'])) {
    $bill_zone_id = get_zone($order->billing['country']['id'], $order->billing['state']);
}
$payment_modules = new payment;

//Catch all payment module error responses variations
$payment_module_error = $checkout->detect_payment_module_error();

if ($payment_module_error) {
    $messageStack->add('checkout_payment', $payment_module_error);
}

if (!tep_session_is_registered('comments'))
    tep_session_register('comments');

if (tep_session_is_registered('customer_id')) {
    $customer_query = tep_db_query("SELECT c.*, ab.* FROM " . TABLE_CUSTOMERS . " c LEFT JOIN " . TABLE_ADDRESS_BOOK . " ab ON (c.customers_default_address_id = ab.address_book_id) WHERE c.customers_id = '" . (int) $_SESSION['customer_id'] . "'");
    $customer = tep_db_fetch_array($customer_query);
} else {
    $customer_default_country = CHECKOUT_COUNTRY_DEFAULT_ID;
    /*
     * TODO: This does not correctly recognize the user's language
      if (!tep_session_is_registered('customer_id')) {
      preg_match('/[a-z]{2}\-([A-Za-z]{2})/', $_SERVER['HTTP_ACCEPT_LANGUAGE'], $matches);
      if (count($matches) == 2) {
      $country_query = tep_db_query("SELECT countries_id FROM " . TABLE_COUNTRIES . " WHERE countries_iso_code_2 = '" . strtoupper($matches[1]) . "' LIMIT 1");
      if (tep_db_num_rows($country_query) > 0) {
      $country = tep_db_fetch_array($country_query);
      $customer_default_country = $country['countries_id'];
      }
      }
      }
     */
}

$payment_shipping = $checkout->get_payment_shipping(false);
$selections = $payment_shipping['payment'];

for ($x = 0; $x < count($selections); $x++) {
    include(DIR_WS_LANGUAGES . $language . '/modules/payment/' . $selections[$x]['id'] . '.php');

    /* If this is Protx Direct v5, pull the fields from the confirmation method */
    if ($selections[$x]['id'] == 'protx_direct' && isset($GLOBALS[$selections[$x]['id']]->protocol)) {
        $selections[$x] = $GLOBALS[$selections[$x]['id']]->confirmation();
    }
}

$radio_buttons = 0;

$hidden_variables = array(); //array('name' => 'shipping', 'value' => ''));

$step_count = 1;

if (tep_session_is_registered('checkout_order_id') && (int) $_SESSION['checkout_order_id'] > 0) {
    $checkout->delete_order($_SESSION['checkout_order_id']);
    tep_session_unregister('checkout_order_id');
}

if (CHECKOUT_CONTRIB_STS) {
    require(DIR_WS_INCLUDES . 'column_left.php');
    require(DIR_WS_INCLUDES . 'column_right.php');
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/html"
      xmlns="http://www.w3.org/1999/html" <?php echo HTML_PARAMS; ?>>
    <head profile="http://www.w3.org/2005/10/profile">
        <meta http-equiv="Content-Type" content="text/html; charset="<?php echo CHARSET; ?>">
                    <?php
                    if (file_exists(DIR_WS_INCLUDES . 'header_tags.php')) {
                        require(DIR_WS_INCLUDES . 'header_tags.php');
                    } else {
                        echo '<title>' . TITLE . '</title>';
                    }
                    ?>
              <base href="<?php echo (($request_type == 'SSL') ? HTTPS_SERVER : HTTP_SERVER) . DIR_WS_CATALOG; ?>">
                <link rel="stylesheet" type="text/css" href="stylesheet.css">
                    <?php
                    if (CHECKOUT_CONTRIB_STS_4) {
                        $sts->restart_capture();
                    }

                    $checkout_stylesheet = '<link rel="stylesheet" type="text/css" href="' . CHECKOUT_WS_CSS . 'checkout.css" />';

                    if (CHECKOUT_CONTRIB_STS_4) {
                        echo $checkout_stylesheet;
                        $sts->restart_capture('checkout_stylesheet');
                    } elseif (CHECKOUT_CONTRIB_STS_2) {
                        $sts_block_name = 'checkout_stylesheet';
                        $template[$sts_block_name] = $checkout_stylesheet;
                    } else {
                        echo $checkout_stylesheet;
                    }
                    ?>
                    <script type="text/javascript" src="<?php echo CHECKOUT_WS_JS; ?>jquery.js"></script>
                    <script type="text/javascript" src="<?php echo CHECKOUT_WS_JS; ?>lightbox.js"></script>
                    <script type="text/javascript"><?php include(CHECKOUT_WS_JS . 'checkout.js.php'); ?></script>

                    <link rel="stylesheet" type="text/css" href="ext/jquery/fancybox/jquery.fancybox-1.3.4.css" />
                    <link rel="stylesheet" type="text/css" href="ext/jquery/jquery.selectbox/css/jquery.selectbox.css" />
                    <link rel="stylesheet" type="text/css" href="ext/slider_thumb/svwp_style.css" />
                    <link rel="stylesheet" type="text/css" href="ext/slider_thumb/jquery.lightbox-0.5.css" />
                    <link rel="stylesheet" type="text/css" href="stylesheet.css" />
                    <!--phone tooltip-->
                    <link rel="stylesheet" type="text/css" href="ext/jquery/tooltip/tipsy.css"></link> 
                    <script type="text/javascript" src="ext/jquery/tooltip/jquery.tipsy.js"></script>         
                    <!--end phone tooltip-->
                            <!--[if IE]>
                    <link rel="stylesheet" type="text/css" href="stylesheet-ie.css" />
                    <![endif]-->
<?php echo $oscTemplate->getBlocks('header_tags'); ?>
                    <link rel="icon" type="image/png" href="favicon.png" />
                    <script type="text/javascript">
                       $(document).ready(function(){
                           $('#tooltip_billing').tipsy({gravity: 'se'});
                           $("#same_infor").click(function() {
                            if ($('#same_infor').is(':checked')) {
                                var first_name=$("#bill_firstname").val();
                                var last_name=$("#bill_lastname").val();
                                var country=$("#bill_country").val();
                                var street_address=$("#bill_street_address").val();
                                var suburb=$("#bill_suburb").val();
                                var city=$("#bill_city").val();
                                var state=$("#bill_state").val();
                                var postcode=$("#bill_postcode").val();
                                $("#ship_firstname").val(first_name);
                                $("#ship_last_name").val(last_name);
                                $("#ship_country").val(country);
                                $("#ship_street_address").val(street_address);
                                $("#ship_suburb").val(suburb);
                                $("#ship_city").val(city);
                                var type="ship";
                                $.post(Checkout.d.file_checkout,
                                { x: "get_zones",
                                    c: $("select#"+type+"_country").val(),
                                    n: type+"_state"},
                                function (data) {
                                    var html = '';

                                    if (data.zones.length > 1) {
                                    $("div#"+type+"_state_blk").html(
                                        '<select name="'+type+'_state" class="checkout-select" id="'+type+'_state"></select>'
                                    );

                                    $.each(data.zones, function(i, n) {
                                        $("#"+type+"_state").append(
                                        '<option value="'+n.id+'">'+n.text+'</option>'
                                        );
                                    });
                                    $("#ship_state").val(state);   
                                    $("#"+type+"_state").bind("change", function() {
                                        Checkout.GetPaymentShipping();
                                    });
                                    } else {
                                    $("div#"+type+"_state_blk").html(
                                        '<input type="text" name="'+type+'_state" class="checkout-input-text" id="'+type+'_state" />'
                                    );
                                    $("#ship_state").val(state);   
                                    $("#"+type+"_state").bind("keyup", function() {
                                        Checkout.TimeoutGetPaymentShipping();
                                    });
                                    }
                                }, 'json');
                                          
                                $("#ship_postcode").val(postcode);
                            }
                            else{
                                
                            }
                           });
                       });
                    </script>
                    <style type="text/css">
                        #topMenu ul {
                            list-style: none outside none;
                            margin-left: 10px;
                            padding: 0;
                        }

                        #topMenu_c ul li {
                            display: block;
                            float: left;
                            width: 275px;
                            margin-top: 20px;

                        }

                        #topMenu_c ul li p{
                            padding: 0px;
                            margin: 0px;
                            line-height: normal;

                        }

                        #topMenu_c ul li img{
                            float: left;
                            margin-top: -3px;
                        }

                        .p1{
                            font-size: 16px;
                            color: #646464;
                            font-family: arial;
                        }

                        .table1 table tr td.text {
                            font-size: 10px;
                            color: #494949;
                            font-family : Arial;
                            font-weight: bold;
                        }

                        .table1 table tr td span {
                            font-size: 8px;
                            color: #494949;
                            font-style: italic;
                        }
                        .table2 tr td.td1 {
                            text-align: right;
                        }

                        .table2 tr td.td2 input {
                            height: 20px;
                            width: 225px;
                        }

                        .table2 tr:last-child td input {
                            height: 20px !important;
                            width: 55px !important;
                            float: left;
                        }

                        div.ccv_code_img{
                            float:left;
                            padding: 0px !important;
                        }

                        .table2 tr td.td2 select {
                            height: 25px;
                            width: 113px;
                            padding: 2px;
                        }


                        .table3 table tr td {
                            font-size: 10px;
                            color: #494949;
                            font-family : Arial;
                            font-weight: bold;
                        }

                        #password_input input{
                            width: 267px;
                        }

                        #email_address{
                            width: 285px;
                        }

                        #bill_lastname,#ship_lastname {
                            width: 133px;
                        }

                        #img img{
                            float:left;
                        }

                        #topMenu_c{
                            margin-left: 62px;
                            margin-top: 8px;
                        }

                        ul.checkout-payment-methods li div {
                            padding-left: 0px;
                        }




                    </style>
                    </head>


                    <?php
                    if (CHECKOUT_CONTRIB_STS_4) {
                        $sts->restart_capture('applicationtop2header');
                    } elseif (CHECKOUT_CONTRIB_STS_2) {
                        $sts_block_name = 'applicationtop2header';
                        require(STS_RESTART_CAPTURE);
                    }
                    ?>
                    <body>
<?php
if (CHECKOUT_CONTRIB_STS_4) {
    $sts->restart_capture();
}
?>

                        <noscript>
                            <div class="checkout-overlay" style="height:3000px;visibility:visible"></div>
                            <div class="checkout-dialog-box checkout-js-required"><?php echo CHECKOUT_JAVASCRIPT_REQUIRED; ?></div>
                        </noscript>


                        <div id="fullwrap">
                            <div id="mainWrapper">
                                <div id="bodyWrapper" style="width: 1006px;" class="container_<?php echo $oscTemplate->getGridContainerWidth(); ?>">


                                    <!--cuongnm edit 23/5/2012 begin-->
                                    <div class="checkoutstep1" style="height: 120px;">
                                        <div id="Logo" style="margin-left: 379px; height: 32px; margin-top: 7px;"><img src="images/store_logo.png" /></div>

                                        <div id="topMenu_c" >
                                            <img src="images/menu.jpg" alt=""/>
                                        </div>

                                    </div>
                                    <!--cuongnm edit 23/5/2012 end-->

                                    <div class="newcheckout" style="width: 940px; margin-left: 13px; ">
                                                    <?php echo tep_draw_form('checkout', tep_href_link(CHECKOUT_WS_INCLUDES . 'checkout_process.php', '', 'SSL'), 'post', 'onSubmit="return Checkout.ProcessOrder();" target="checkout-gateway" autocomplete="off"') . tep_draw_hidden_field('action', 'checkout'); ?>
                                        <div id="bodyContent" class="grid_20 push_4" style="width: 218px;">
                                            <div class="contentContainerxxxx">
                                             	

                                                <div style="border: 1px solid #dddddd;">
                                                    <h2 class="" style="width: 216px;height:37px;background-color: #f5f5f5; position: relative; margin-top: 0px;border-bottom: 1px solid #DDDDDD;">
                                                    	<div style="position: absolute; top: 7px; right: 13px; font-size: 17px; color: #494949;">Need Help? Contact us!</div>
                                                    </h2>
                                                    <div style="padding:15px 20px;position: relative; height:15px;">
                                                    	<img src="images/layout/tell_help.png" style="position: absolute;float:left; width:24px; height:19px;margin-left:15px;" /><h2 style="font-size: 17px;color: #494949;position: absolute;float:left;margin-left:47px;">800-977-8761</h2>
                                                    </div>
                                                    <div style="padding:10px 20px 15px;position: relative;height:15px;">
                                                    	<a href="http://www.motorfiend.com/#"><img src="images/layout/live_support.png" style="position: absolute;float:left; width:24px; height:19px;margin-left:15px;" /><h2 style="font-size: 17px;color: #494949;position: absolute;float:left;margin-left:47px;">Live Support</h2></a>
                                                    </div>
                                                </div>

                                                <div style="border: 1px solid #dddddd;margin-top:15px;">
                                                    <h2 class="" style="width: 216px;height:37px;background-color: #f5f5f5; position: relative; margin-top: 0px;border-bottom: 1px solid #DDDDDD;"><div style="position: absolute; top: 7px; right: 62px; font-size: 18px; color: #494949;">Your Items!</div></h2>
                                                    <?php
                                                    $cart_contents_string = '<table border="0" width="100%" cellspacing="0" cellpadding="0" style="width: 70%; margin-left: 30px;margin-top: 10px;">';
                                                    $products = $cart->get_products();
                                                    for ($i = 0, $n = sizeof($products); $i < $n; $i++) {
                                                        $cart_contents_string .= '<tr><td align="right" valign="top">';

                                                        if ((tep_session_is_registered('new_products_id_in_cart')) && ($new_products_id_in_cart == $products[$i]['id'])) {
                                                            $cart_contents_string .= '<span class="newItemInCart">';
                                                        }

                                                        $cart_contents_string .= $products[$i]['quantity'] . '&nbsp;x&nbsp;';

                                                        if ((tep_session_is_registered('new_products_id_in_cart')) && ($new_products_id_in_cart == $products[$i]['id'])) {
                                                            $cart_contents_string .= '</span>';
                                                        }

                                                        $cart_contents_string .= '</td><td valign="top"><a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $products[$i]['id']) . '">';

                                                        if ((tep_session_is_registered('new_products_id_in_cart')) && ($new_products_id_in_cart == $products[$i]['id'])) {
                                                            $cart_contents_string .= '<span class="newItemInCart">';
                                                        }

                                                        $cart_contents_string .= $products[$i]['name'];

                                                        if ((tep_session_is_registered('new_products_id_in_cart')) && ($new_products_id_in_cart == $products[$i]['id'])) {
                                                            $cart_contents_string .= '</span>';
                                                        }

                                                        $cart_contents_string .= '</a></td></tr>';

                                                        if ((tep_session_is_registered('new_products_id_in_cart')) && ($new_products_id_in_cart == $products[$i]['id'])) {
                                                            tep_session_unregister('new_products_id_in_cart');
                                                        }
                                                    }

                                                    $cart_contents_string .= '<tr><td colspan="2" align="right" style="padding-top: 15px; padding-bottom: 5px;"><span style="color:#368b08;font-size:15px; text-align: center; ">Sub-Total: ' . $currencies->format($cart->show_total()) . '</span></td></tr>' .
                                                            '</table>';
                                                    $cart_contents_string .= '<div style="border-bottom: 1px solid #f5f5f5;  margin-left: 7px;position: relative; top: -35px;width: 200px;"></div>';
                                                    echo $cart_contents_string;
                                                    ?>
                                                </div>
                                                <!--cuongnm edit 23/5/2012 begin-->
                                                <div style="border: 1px solid #dddddd; margin-top: 17px; padding-top: 10px; width: 217px; height: 300px;">
                                                    <div style="margin-bottom: 0px; margin-left: 60px; margin-top: -3px;">
                                                        <a href="https://www.sitelock.com/verify.php?site=motorfiend.com" target="_blank" id="sitelock"><img alt="website security" title="SiteLock" border="0" src="//shield.sitelock.com/shield/motorfiend.com"/></a>

                                                        <!--<img src="images/Sitelock.jpg"/>--> </div>
                                                    <div style="margin-left: 18px; width: 179px; height: 192px;">
                                                        <p style="font-size: 12px; color: #000000;">This website is <b>secured by a high-grade 256-bit SSL certificate</b> that encrypts (scrambles) your information to ensure a <b>safe and secure</b> transaction.</p>
                                                        <p style="font-size: 12px; color: #000000;">All credit card information passes directly to our payment processor and is <u>never</u> stored by us. Your <b>security</b> is our first priority.</p>
                                                    </div>
                                                </div>
                                                <!--cuongnm edit 23/5/2012 end-->




                                            </div>
                                        </div>
                                        <div id="columnLeft" class="grid_4 pull_20" style="width: 708px; padding-top: 0px;position: relative; ">
                                            <div class="contentContainersss" style="border: 1px solid #dddddd;  height: 37px;  width: 708px; border-bottom: 0px;" >
                                                <h2 class="" style="width: 708px; height:37px;background-color: #f5f5f5; margin-top: 0px;">
                                                    <span style="left: 13px;position: absolute; top: 8px; font-size: 19px; color: #b41313;">Billing Information </span>
                                                    <img style="position: absolute;left:185px;top:9px;" id="tooltip_billing" title='Please make sure the billing address you enter below matches the name and address associated with the credit card you plan to use.' src="images/tool-tip-icon.png"/>
                                                    <span style="left: 380px;position: absolute; top: 8px; font-size: 19px; color: #b41313;">Shipping Information</span>
                                                </h2>

                                            </div>
                                            <!--billing & shipping-->
                                            <div style="border: 1px solid #dddddd; margin-top: 0px; width: 708px; display: block; height:423px;" class="table1">
                                            <!--billing-->
                                            <div style="margin-top: 0px; width: 354px;position: absolute;" class="table1">
                                                <table cellpadding="2" cellspacing="2" style="margin-left: 5px;">

                                                    <tr>
                                                        <td class="main" valign="top">
                                                            <label for="bill_firstname"><?php echo CHECKOUT_FIRST_NAME; ?><br />
<?php echo tep_draw_input_field('bill_firstname', $customer['customers_firstname'], 'class="checkout-input-text" maxlength="255" id="bill_firstname"'); ?>
                                                            </label>
                                                            <label for="bill_lastname"><?php echo CHECKOUT_LAST_NAME; ?><br />
                                                    <?php echo tep_draw_input_field('bill_lastname', $customer['customers_lastname'], 'class="checkout-input-text" maxlength="255" id="bill_lastname"'); ?>
                                                            </label>
                                                            <div class="checkout-tip"><?php echo CHECKOUT_TIP_NAME; ?></div>
                                                            <div class="checkout-form-error" id="ERROR_bill_name"></div>
                                                        </td>
                                                    </tr>

<?php
//if (!tep_session_is_registered('customer_id') && CHECKOUT_REQUIRE_EMAIL != 'H') { 
if (CHECKOUT_REQUIRE_EMAIL != 'H') {
    ?>
                                                        <tr>
                                                            <td class="main" style="padding-bottom: 8px">
                                                                <label for="email_address"<?php echo (CHECKOUT_REQUIRE_EMAIL == 'O' ? ' class="optional"' : ''); ?>><?php echo CHECKOUT_EMAIL_ADDRESS; ?><br />
                                                        <?php echo tep_draw_input_field('email_address', $customer['customers_email_address'], 'class="checkout-input-text" id="email_address" maxlength="255"'); ?>
                                                                </label>
                                                                <div class="checkout-tip"><?php echo CHECKOUT_TIP_EMAIL; ?></div>
                                                                <div class="checkout-form-error" id="ERROR_email_address"></div>
                                                            </td>
                                                        </tr>
<?php } ?>

                                                                <?php
                                                                if (!tep_session_is_registered('customer_id') && (CHECKOUT_CREATE_ACCOUNT && !CHECKOUT_GENERATE_PASSWORD)) {
                                                                    ?>

                                                        <tr>
                                                            <td class="main" valign="top" colspan="2" id="password_input">
                                                                <label for="password"><?php echo CHECKOUT_PASSWORD; ?><br />
                                                        <?php echo tep_draw_password_field('password', '', 'class="checkout-input-text" maxlength="255"'); ?>
                                                                </label>
                                                                <div class="checkout-tip"><?php echo CHECKOUT_ENTER_PASSWORD; ?></div>
                                                                <div class="checkout-form-error" id="ERROR_password"></div>
                                                            </td>
                                                        </tr>
                                                                    <?php
                                                                }
                                                                ?>


                                                    <tr>
                                                        <td>
                                                            <label for="bill_country"><?php echo CHECKOUT_COUNTRY; ?>
                                                                <?php
                                                                $country_count_query = tep_db_query("SELECT countries_id, countries_name FROM " . TABLE_COUNTRIES);
                                                                $country_count = tep_db_num_rows($country_count_query);
                                                                ?>
                                                                <?php
                                                                if ($country_count > 1) {
                                                                    echo '<br />' . tep_get_country_list('bill_country', ($customer['entry_country_id'] > 0 ? $customer['entry_country_id'] : $customer_default_country), 'class="checkout-select" id="bill_country" onchange="Checkout.UpdateZones(\'bill\')"');
                                                                } else {
                                                                    $country = tep_db_fetch_array($country_count_query);
                                                                    echo '&nbsp;&nbsp;<b>' . $country['countries_name'] . '</b>';
                                                                    echo tep_draw_hidden_field('bill_country', $country['countries_id'], 'id="bill_country"');
                                                                }
                                                                ?>
                                                            </label>
                                                            <div class="checkout-tip" ><?php echo CHECKOUT_TIP_COUNTRY; ?></div>
                                                            <div class="checkout-form-error" id="ERROR_bill_country"></div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text">
                                                            <label for="bill_street_address"><?php echo CHECKOUT_STREET_ADDRESS; ?><br />
<?php echo tep_draw_input_field('bill_street_address', $customer['entry_street_address'], 'class="checkout-input-text" id="bill_street_address" maxlength="255"'); ?>
                                                            </label>
                                                            <div class="checkout-tip" style="color: #000000;"><?php echo CHECKOUT_TIP_STREET_ADDRESS; ?></div>
                                                            <div class="checkout-form-error" id="ERROR_bill_street_address"></div>

                                                        </td>

                                                    </tr>

<?php if (CHECKOUT_REQUIRE_SUBURB != 'H') { ?>
                                                        <tr>
                                                            <td class="main" valign="top" colspan="3">
                                                                <label for="bill_suburb"<?php echo (CHECKOUT_REQUIRE_SUBURB == 'O' ? ' class="optional"' : ''); ?>><?php echo CHECKOUT_SUBURB; ?><br />
    <?php echo tep_draw_input_field('bill_suburb', $customer['entry_suburb'], 'class="checkout-input-text" id="bill_suburb" maxlength="255"'); ?>
                                                                </label>
                                                                <div class="checkout-tip"><?php echo CHECKOUT_TIP_SUBURB; ?></div>
                                                            </td>
                                                        </tr>
<?php } ?>
                                                    <tr>
                                                        <td>
                                                            <label for="bill_city"><?php echo CHECKOUT_CITY; ?><br />
<?php echo tep_draw_input_field('bill_city', $customer['entry_city'], 'class="checkout-input-text" id="bill_city" maxlength="255"'); ?>
                                                            </label>
                                                            <?php if (CHECKOUT_REQUIRE_STATE != 'H') { ?>
                                                                <div class="state_container">
                                                                    <label for="bill_state"<?php echo (CHECKOUT_REQUIRE_STATE == 'O' ? ' class="optional"' : ''); ?>><?php echo CHECKOUT_ZONE; ?>
                                                                        <div id="bill_state_blk">
    <?php $checkout->display_zones(($customer['entry_country_id'] > 0 ? $customer['entry_country_id'] : $customer_default_country), 'bill_state', ($customer['entry_zone_id'] < 1 ? $customer['entry_state'] : $customer['entry_zone_id']), false); ?>
                                                                        </div>
                                                                    </label>
                                                                </div>
<?php } ?>
                                                            <label for="bill_postcode"<?php echo (CHECKOUT_REQUIRE_POSTCODE == 'O' ? ' class="optional"' : ''); ?>><?php echo CHECKOUT_ZIP; ?><br />
<?php echo tep_draw_input_field('bill_postcode', $customer['entry_postcode'], 'class="checkout-input-text" id="bill_postcode" maxlength="10"'); ?>
                                                            </label>
                                                            <div class="checkout-form-error" id="ERROR_bill_city"></div>
                                                            <div class="checkout-form-error" id="ERROR_bill_state"></div>
                                                            <div class="checkout-form-error" id="ERROR_bill_postcode"></div>
                                                        </td>

                                                    </tr>

<?php if (CHECKOUT_REQUIRE_TELEPHONE != 'H') { ?>
                                                        <tr>
                                                            <td class="main" valign="top">
                                                                <label for="telephone"<?php echo (CHECKOUT_REQUIRE_TELEPHONE == 'O' ? ' class="optional"' : ''); ?>><?php echo CHECKOUT_TELEPHONE; ?><br />
                                                        <?php echo tep_draw_input_field('telephone', $customer['customers_telephone'], 'class="checkout-input-text" maxlength=32'); ?>
                                                                </label>
                                                                <div class="checkout-tip"><?php echo CHECKOUT_TIP_TELEPHONE; ?></div>
                                                                <div class="checkout-form-error" id="ERROR_telephone"></div>
                                                            </td>
                                                        </tr>
    <?php
}
?>

                                                </table>
                                            </div>
                                            <!--shipping-->
                                             <div style="border-left: 1px solid #dddddd; margin-top: 0px; width: 354px;position: absolute; left:354px; padding-bottom: 9px; height: 414px;" class="table1" class="table1">
                                                <table cellpadding="2" cellspacing="2" style="margin-left: 10px;">
                                                    <tr>
                                                        <td class="main" valign="top">
                                                            <p><input type="checkbox" id="same_infor"/><span id="same_txt">Click here if your Shipping is the same as your Billing.</span></p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="main" valign="top">
                                                            <label for="ship_firstname"><?php echo CHECKOUT_FIRST_NAME; ?><br />
<?php echo tep_draw_input_field('ship_firstname', $customer['customers_firstname'], 'class="checkout-input-text" maxlength="255" id="ship_firstname"'); ?>
                                                            </label>
                                                            <label for="ship_lastname"><?php echo CHECKOUT_LAST_NAME; ?><br />
                                                    <?php echo tep_draw_input_field('ship_lastname', $customer['customers_lastname'], 'class="checkout-input-text" maxlength="255" id="ship_lastname"'); ?>
                                                            </label>
                                                            <div class="checkout-tip"><?php echo CHECKOUT_TIP_NAME; ?></div>
                                                            <div class="checkout-form-error" id="ERROR_ship_name"></div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label for="ship_country"><?php echo CHECKOUT_COUNTRY; ?>
                                                                <?php
                                                                $country_count_query = tep_db_query("SELECT countries_id, countries_name FROM " . TABLE_COUNTRIES);
                                                                $country_count = tep_db_num_rows($country_count_query);
                                                                ?>
                                                                <?php
                                                                if ($country_count > 1) {
                                                                    echo '<br />' . tep_get_country_list('ship_country', ($customer['entry_country_id'] > 0 ? $customer['entry_country_id'] : $customer_default_country), 'class="checkout-select" id="ship_country" onchange="Checkout.UpdateZones(\'ship\')"');
                                                                } else {
                                                                    $country = tep_db_fetch_array($country_count_query);
                                                                    echo '&nbsp;&nbsp;<b>' . $country['countries_name'] . '</b>';
                                                                    echo tep_draw_hidden_field('ship_country', $country['countries_id'], 'id="ship_country"');
                                                                }
                                                                ?>
                                                            </label>
                                                            <div class="checkout-tip" ><?php echo CHECKOUT_TIP_COUNTRY; ?></div>
                                                            <div class="checkout-form-error" id="ERROR_ship_country"></div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text">
                                                            <label for="ship_street_address"><?php echo CHECKOUT_STREET_ADDRESS; ?><br />
<?php echo tep_draw_input_field('ship_street_address', $customer['entry_street_address'], 'class="checkout-input-text" id="ship_street_address" maxlength="255"'); ?>
                                                            </label>
                                                            <div class="checkout-tip" style="color: #000000;"><?php echo CHECKOUT_TIP_STREET_ADDRESS; ?></div>
                                                            <div class="checkout-form-error" id="ERROR_ship_street_address"></div>

                                                        </td>

                                                    </tr>

<?php if (CHECKOUT_REQUIRE_SUBURB != 'H') { ?>
                                                        <tr>
                                                            <td class="main" valign="top" colspan="3">
                                                                <label for="ship_suburb"<?php echo (CHECKOUT_REQUIRE_SUBURB == 'O' ? ' class="optional"' : ''); ?>><?php echo CHECKOUT_SUBURB; ?><br />
    <?php echo tep_draw_input_field('ship_suburb', $customer['entry_suburb'], 'class="checkout-input-text" id="ship_suburb" maxlength="255"'); ?>
                                                                </label>
                                                                <div class="checkout-tip"><?php echo CHECKOUT_TIP_SUBURB; ?></div>
                                                            </td>
                                                        </tr>
<?php } ?>
                                                    <tr>
                                                        <td>
                                                            <label for="ship_city"><?php echo CHECKOUT_CITY; ?><br />
<?php echo tep_draw_input_field('ship_city', $customer['entry_city'], 'class="checkout-input-text" id="ship_city" maxlength="255"'); ?>
                                                            </label>
                                                            <?php if (CHECKOUT_REQUIRE_STATE != 'H') { ?>
                                                                <div class="state_container">
                                                                    <label for="ship_state"<?php echo (CHECKOUT_REQUIRE_STATE == 'O' ? ' class="optional"' : ''); ?>><?php echo CHECKOUT_ZONE; ?>
                                                                        <div id="ship_state_blk">
    <?php $checkout->display_zones(($customer['entry_country_id'] > 0 ? $customer['entry_country_id'] : $customer_default_country), 'ship_state', ($customer['entry_zone_id'] < 1 ? $customer['entry_state'] : $customer['entry_zone_id']), false); ?>
                                                                        </div>
                                                                    </label>
                                                                </div>
<?php } ?>
                                                            <label for="ship_postcode"<?php echo (CHECKOUT_REQUIRE_POSTCODE == 'O' ? ' class="optional"' : ''); ?>><?php echo CHECKOUT_ZIP; ?><br />
<?php echo tep_draw_input_field('ship_postcode', $customer['entry_postcode'], 'class="checkout-input-text" id="ship_postcode" maxlength="10"'); ?>
                                                            </label>
                                                            <div class="checkout-form-error" id="ERROR_ship_city"></div>
                                                            <div class="checkout-form-error" id="ERROR_ship_state"></div>
                                                            <div class="checkout-form-error" id="ERROR_ship_postcode"></div>
                                                        </td>

                                                    </tr>

                                                </table>
                                            </div>
                                            <!--end shipping-->
                                            </div>
                                            <!--end billing & shipping-->
                                            <div class="contentContainersss111" style="position: relative;background-color: #f5f5f5;border: 1px solid #dddddd; margin-top: 20px; " >
                                                <div style="border-bottom: 0px;  height:37px;background-color: #f5f5f5;">
                                                    <h2 class="" style="width: 700px; height:10px;background-color: #f5f5f5;"><span style="left: 20px;position: absolute; top: 7px; font-size: 18px; color:#b41313;">Enter Payment Information</span></h2>
                                                </div>
                                                <div style="border-top: 1px solid #dddddd;">           
                                                    <table border="0" width="100%" cellspacing="0" cellpadding="3">
<?php
if (!$ec_enabled || isset($_GET['ec_cancel']) || (!tep_session_is_registered('paypal_ec_payer_id') && !tep_session_is_registered('paypal_ec_payer_info'))) {
    ?>
                                                            <tr>
                                                                <td>
                                                                    <div class="checkout-form-error" id="ERROR_payment"></div>
                                                                    <div id="checkout-payment-methods">
                                                                        <ul class="checkout-payment-methods">

                                                                            <?php
                                                                            $radio_buttons = 0;
                                                                            $total = 0;
                                                                            /*echo "<pre>";
                                                                            print_r($checkout_payment_disable);
                                                                            echo "</pre>";
                                                                            echo "<pre>";
                                                                            print_r($selections);
                                                                            echo "</pre>";*/
                                                                            foreach ($selections as $selection) {
                                                                                if (!in_array($selection['id'], $checkout_payment_disable)) {
                                                                                    if ($payment == '') {
                                                                                        $payment = $selection['id'];
                                                                                    }

                                                                                    if ((count($selections) == 1 && $selection['id'] != 'paypal_wpp') || (count($selections) == 1 && $selection['id'] == 'paypal_wpp' && MODULE_PAYMENT_PAYPAL_DP_BUTTON_PAYMENT_METHOD == 'No')) {
                                                                                        $payment_input = tep_draw_hidden_field('payment', $selection['id'], $selection['id']);
                                                                                    } else {
                                                                                        $payment_input = tep_draw_radio_field('payment', $selection['id'], ($selection['id'] == $payment), 'id="radio-' . $selection['id'] . '"');
                                                                                    }
                                                                                    if($selection['id']=='authorizenet_cc_aim') $selection['module']=''; else $selection['module']=$selection['module'];
                                                                                    ?>
                                                                                    <li><a class="payment-title" id="a-<?php echo $selection['id']; ?>"><?php echo $payment_input . $selection['module']; ?></a>
                                                                                        <div>
                                                                                            <?php
                                                                                            if (isset($selection['error'])) {
                                                                                                ?>
                                                                                                <p class="main"><?php echo $selection['error']; ?></p>
                                                                                                    <?php
                                                                                                } elseif (isset($selection['fields']) && is_array($selection['fields'])) {
                                                                                                    ?>
                                                                                                <table border="0" cellspacing="0" cellpadding="2">

                                                                                                    <?php
                                                                                                    for ($j = 0, $n2 = sizeof($selection['fields']); $j < $n2; $j++) {
                                                                                                        ?>
                                                                                                        <tr>
                                                                                                            <td class="main title_first"><?php echo $selection['fields'][$j]['title']; ?></td>
                                                                                                            <td class="main feld_payment"><?php echo $selection['fields'][$j]['field']; ?></td>
                                                                                                        </tr>
                                                                                                    <?php
                                                                                                }
                                                                                                ?>
                                                                                                </table>

                                                                                        <?php
                                                                                    }
                                                                                    ?>
                                                                                        </div>
                                                                                    </li>
                                                                                    <?php
                                                                                    $radio_buttons++;
                                                                                }

                                                                                if ($selection['id'] == 'paypal_wpp' && MODULE_PAYMENT_PAYPAL_DP_BUTTON_PAYMENT_METHOD == 'Yes') {
                                                                                    $payment_input = tep_draw_radio_field('payment', 'paypal_wpp_ec', false, 'id="radio-paypal_wpp_ec"');
                                                                                    ?>
                                                                                    <li><a class="payment-title" id="a-paypal_wpp_ec"><?php echo $payment_input . '&nbsp;&nbsp;' . MODULE_PAYMENT_PAYPAL_EC_TEXT_TITLE; ?></a>
                                                                                        <div><span class="main"><?php echo TEXT_PAYPALWPP_EC_HEADER; ?></span></div>
                                                                                    </li>
            <?php
        }
    }
    ?>
                                                                        </ul>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                        } else {
                                                            tep_paypal_wpp_switch_checkout_method(FILENAME_CHECKOUT);
                                                            echo '<input type="hidden" name="payment" value="paypal_wpp" />';
                                                        }
                                                        if (CHECKOUT_ENABLE_COUPON && CHECKOUT_DISPLAY_CART === false) {
                                                            ?>
                                                            <tr>
                                                                <td class="checkout-spacing-1"></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table border="0" cellspacing="0" cellpadding="2">
                                                                        <tr>
                                                                            <td valign="middle" class="main" width="195" colspan="2"><?php echo CHECKOUT_PROMO_CODE; ?></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="main" valign="middle"><?php echo tep_draw_input_field('coupon', $_SESSION['coupon'], 'id="checkout-coupon" class="checkout-input-text"'); ?></td>
                                                                            <td class="main"><a href="javascript:void(0);" onclick="Checkout.ApplyCoupon();"><?php echo tep_image_button('button_update.gif'); ?></a></td>
                                                                        </tr>
                                                                    </table>
                                                                    <div id="checkout-coupon-status"></div>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                        }

                                                        if (CHECKOUT_CONTRIB_POINTS_AND_REWARDS && USE_POINTS_SYSTEM == 'true' && USE_REDEEM_SYSTEM == 'true') {
                                                            $customer_shopping_points = tep_get_shopping_points();

                                                            if ($customer_shopping_points > 0 && get_redemption_rules($order) == true && get_points_rules_discounted($order) == true &&
                                                                    $customer_shopping_points >= POINTS_LIMIT_VALUE && (POINTS_MIN_AMOUNT == '' || $cart->show_total() >= POINTS_MIN_AMOUNT)) {

                                                                if (tep_session_is_registered('customer_shopping_points_spending'))
                                                                    tep_session_unregister('customer_shopping_points_spending');

                                                                $max_points = $order->info['total'] / REDEEM_POINT_VALUE > POINTS_MAX_VALUE ? POINTS_MAX_VALUE : $order->info['total'] / REDEEM_POINT_VALUE;
                                                                $max_points = $customer_shopping_points > $max_points ? $max_points : $customer_shopping_points;

                                                                if ($order->info['total'] > tep_calc_shopping_pvalue($max_points)) {
                                                                    $note = '<br /><small>' . TEXT_REDEEM_SYSTEM_NOTE . '</small>';
                                                                }

                                                                $customer_shopping_points_spending = $max_points;
                                                                ?>
                                                                <tr>
                                                                    <td class="checkout-spacing-1"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
                                                                            <tr>
                                                                                <td class="main"><b><?php echo CHECKOUT_HEADING_REDEEM_SYSTEM; ?></b></td>
                                                                            </tr>
                                                                        </table></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><table border="0" width="100%" cellspacing="1" cellpadding="2" class="infoBox">
                                                                            <tr class="infoBoxContents">
                                                                                <td style="padding: 0 10px"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                                                                                        <tr>
                                                                                            <td class="main"><?php printf(CHECKOUT_REDEEM_SYSTEM_START, $currencies->format(tep_calc_shopping_pvalue($customer_shopping_points))); ?></td>
                                                                                        </tr>
                                                                                        <tr class="moduleRow">
                                                                                            <td class="main">
        <?php
        echo tep_draw_checkbox_field('customer_shopping_points_spending', '1', '', 'onclick="Checkout.ApplyPoints(this.checked)"');
        printf(CHECKOUT_REDEEM_SYSTEM_SPENDING, number_format($max_points, POINTS_DECIMAL_PLACES));
        ?>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table></td>
                                                                            </tr>
                                                                        </table></td>
                                                                </tr>
        <?php
    }
}
?>

                                                    </table>
                                                </div>
                                            </div>







                                            <div class="contentContainersss111" style="position: relative; margin-top: 16px; " >
                                                <div style="border: 1px solid #dddddd; border-bottom:0px;  height:37px;background-color: #f5f5f5;">
                                                    <h2 class="" style="width: 710px;"><span style="left: 20px;position: absolute; top: 7px; font-size: 18px; color: #B41313;">Select a Shipping Method</span>


                                                    </h2></div>
                                                <div style="border: 1px solid #dddddd; margin-top: 0px;" class="table3">
                                                    <table border="0" width="100%" cellspacing="0" cellpadding="2" class="">

                                                        <tr class="productListing">
                                                            <td class="checkout-productListing-data">
                                                                <div class="checkout-form-error" id="ERROR_shipping"></div>
                                                                <div id="checkout-shipping-quotes">
                                                                    <?php
                                                                    if (!CHECKOUT_DYNAMIC_SHIPPING) {
                                                                        $shipping_options = $checkout->get_payment_shipping(false);
                                                                        $shipping_options = $shipping_options['shipping'];

                                                                        echo '<table border="0" width="100%" cellspacing="0" cellpadding="2">';

                                                                        $row = 0;
                                                                        foreach ($shipping_options as $quote) {
                                                                            if ($row > 0) {
                                                                                echo '<tr><td colspan="3" class="checkout-spacing-1">&nbsp;</td></tr>';
                                                                            }

                                                                            echo '<tr><td class="main" colspan="2">';
                                                                            if (isset($quote['icon']) && $quote['icon'] != '') {
                                                                                echo $quote['icon'] . '&nbsp;';
                                                                            }

                                                                            echo '<b>' . $quote['module'] . '</b>';

                                                                            echo '</td></tr>';
                                                                            echo '<tr>';

                                                                            if (isset($quote['error']) && $quote['error'] != '') {
                                                                                echo '<tr><td class="main" colspan="2">' . $quote['error'] . '</td></tr>';
                                                                            } else {
                                                                                foreach ($quote['methods'] as $method) {
                                                                                    $checked = '';

                                                                                    if ($method['cheapest'] == '1') {
                                                                                        $checked = ' CHECKED';
                                                                                    }
                                                                                    echo '<tr>';
                                                                                    echo '<td class="main"><span style="font-weight:bold;"><input type="radio" name="shipping" id="' . $quote['id'] . '_' . $method['id'] . '" value="' . $quote['id'] . '_' . $method['id'] . '"' . $checked . ' /></span>&nbsp;' . $method['title'] . '</td>';
                                                                                    echo '<td class="main" width="80" align="right">' . $method['cost'] . '</td>';
                                                                                    echo '</tr>';
                                                                                }
                                                                            }



                                                                            $row++;
                                                                        }

                                                                        echo '</table>';
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </td>
                                                        </tr>

                                                    </table>
                                                </div>
                                            </div>
                                            <script type="text/javascript">
                                                $().ready(function(){
                                                    var val1;
                                                    $("#radio input").click(function() {
                                                        $("#per_item").html();
                                                        $("#per_item").html( $("#radio input:checked").val());

                                                        var per_item = $("#per_item").text();
                                                        var sub_total = $("#sub_total").text();
                                                        //var oldText = "Save Earth and smile!";
                                                        var per_item = per_item.replace("$", "");
                                                        var sub_total = sub_total.replace("$", "");
                                                        var total = parseFloat(per_item)+parseFloat(sub_total);
                                                        if(total){
                                                            $("#total").html( "$"+total);
                                                        }
                                                    });
                                                })
                                            </script>

                                            <div class="contentContainersss111" style="position: relative;margin-top: 16px;" >
                                                <div style="border: 1px solid #dddddd; border-bottom: 0px; height:37px;background-color: #f5f5f5;">
                                                    <h2 class="" style="width: 710px;"><span style="left: 20px;position: absolute; top: 7px;font-size: 18px; color: #B41313;">Confirm Order</span></h2>
                                                </div>
                                                <div style="border: 1px solid #dddddd;">
                                                    <table border="0" width="100%" cellspacing="0" cellpadding="2" class="">

                                                        <tr class="productListing">
                                                            <td class="checkout-productListing-data">

                                                                <table border="0" width="100%" cellspacing="0" cellpadding="4">
                                                                    <tr>
                                                                        <td>
                                                                            <div id="checkout-lower-shopping-cart">
                                                                                <table border="0" width="100%" cellspacing="0" cellpadding="2">
                                                                                    <?php
                                                                                    $products = $cart->get_products();
                                                                                    $i = 0;
                                                                                    if (count($products) > 0) {
                                                                                        foreach ($products as $key => $p) {

                                                                                            echo '<tr>';
                                                                                            echo '  <td class="main" align="right" valign="top" width="30">' . $p['quantity'] . '&nbsp;x</td>';
                                                                                            echo '  <td class="main" valign="top"><span class="orderEdit2">' . $p['name'] . '</span><br>';
                                                                                            if (count($p['attributes'])) {
                                                                                                foreach ($p['attributes'] as $option => $value) {
                                                                                                    $attributes = tep_db_query("select popt.products_options_name, poval.products_options_values_name, pa.options_values_price, pa.price_prefix
                                      from " . TABLE_PRODUCTS_OPTIONS . " popt, " . TABLE_PRODUCTS_OPTIONS_VALUES . " poval, " . TABLE_PRODUCTS_ATTRIBUTES . " pa
                                      where pa.products_id = '" . (int) $p['id'] . "'
                                       and pa.options_id = '" . (int) $option . "'
                                       and pa.options_id = popt.products_options_id
                                       and pa.options_values_id = '" . (int) $value . "'
                                       and pa.options_values_id = poval.products_options_values_id
                                       and popt.language_id = '" . (int) $languages_id . "'
                                       and poval.language_id = '" . (int) $languages_id . "'");
                                                                                                    $attributes_values = tep_db_fetch_array($attributes);

                                                                                                    if (CHECKOUT_CONTRIB_OPTIONS_TYPE) {
                                                                                                        if ($value == PRODUCTS_OPTIONS_VALUE_TEXT_ID) {

                                                                                                            $attr_value = $p['attributes_values'][$option];
                                                                                                            $attr_name_sql_raw = 'SELECT po.products_options_name FROM ' .
                                                                                                                    TABLE_PRODUCTS_OPTIONS . ' po, ' .
                                                                                                                    TABLE_PRODUCTS_ATTRIBUTES . ' pa WHERE ' .
                                                                                                                    ' pa.products_id="' . tep_get_prid($p['id']) . '" AND ' .
                                                                                                                    ' pa.options_id="' . $option . '" AND ' .
                                                                                                                    ' pa.options_id=po.products_options_id AND ' .
                                                                                                                    ' po.language_id="' . $languages_id . '" ';
                                                                                                            $attr_name_sql = tep_db_query($attr_name_sql_raw);
                                                                                                            if ($arr = tep_db_fetch_array($attr_name_sql)) {
                                                                                                                $attr_name = $arr['products_options_name'];
                                                                                                            }
                                                                                                        } else {
                                                                                                            $attr_value = $attributes_values['products_options_values_name'];
                                                                                                            $attr_name = $attributes_values['products_options_name'];
                                                                                                        }
                                                                                                    } else {
                                                                                                        $attr_name = $attributes_values['products_options_name'];
                                                                                                        $attr_value = $attributes_values['products_options_values_name'];
                                                                                                    }

                                                                                                    echo '    <nobr><small>&nbsp;<i> - ' . $attr_name . ': ' . $attr_value . '</i></small></nobr></span><br>';
                                                                                                }
                                                                                            }
                                                                                            echo '  </td>';
                                                                                            echo '  <td class="main" align="right" valign="top">' . $currencies->display_price($p['final_price'], tep_get_tax_rate($p['tax_class_id'])) . '</td>';
                                                                                            echo '</tr>';
                                                                                            $i++;
                                                                                        }
                                                                                    }
                                                                                    ?>
                                                                                </table>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="checkout-dashed-line" colspan="3">
                                                                            <table border="0" width="100%" cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td>
                                                                                        <table border="0" cellspacing="0" cellpadding="2">
                                                                                            <?php
                                                                                            if (CHECKOUT_CONTRIB_GIFTWRAP) {
                                                                                                require(DIR_WS_CLASSES . 'gift.php');
                                                                                                $giftwrap_modules = new gift;
                                                                                                if (tep_session_is_registered('giftwrap_info'))
                                                                                                    tep_session_unregister('giftwrap_info');

                                                                                                $quote1 = $giftwrap_modules->quote1();

                                                                                                if (tep_count_giftwrap_modules() > 0) {
                                                                                                    $yes_price = 0;
                                                                                                    $no_price = 0;

                                                                                                    foreach ($quote1[0]['methods'] as $q) {
                                                                                                        if ($q['id'] == 'giftwrap') {
                                                                                                            $yes_price = $q['cost'];
                                                                                                        } elseif ($q['id'] == 'nogiftwrap') {
                                                                                                            $no_price = $q['cost'];
                                                                                                        }
                                                                                                    }

                                                                                                    if (DISPLAY_PRICE_WITH_TAX == true && MODULE_ORDER_TOTAL_GIFTWRAP_TAX_CLASS > 0) {
                                                                                                        $giftwrap_tax = tep_get_tax_rate(MODULE_ORDER_TOTAL_GIFTWRAP_TAX_CLASS);
                                                                                                        $yes_price = $yes_price + tep_calculate_tax($yes_price, $giftwrap_tax);
                                                                                                        $no_price = $no_price + tep_calculate_tax($no_price, $giftwrap_tax);
                                                                                                    }
                                                                                                    ?>

                                                                                                    <tr>
                                                                                                        <td class="main" valign="top"><b><?php echo CHECKOUT_GIFTWRAP_METHOD; ?></b></td>
                                                                                                        <td class="main" valign="top"><input type="radio" name="giftwrap" value="giftwrap_giftwrap" onclick="Checkout.ApplyGiftWrap()" />Yes (+<?php echo $currencies->format($yes_price); ?>)<br /><input type="radio" name="giftwrap" value="nogiftwrap_nogiftwrap" onclick="Checkout.ApplyGiftWrap()" CHECKED />No (+<?php echo $currencies->format($no_price); ?>)</td>
                                                                                                    </tr>
                                                                                                    <?php
                                                                                                } else {
                                                                                                    tep_session_unregister('giftwrap_info');
                                                                                                }
                                                                                            }
                                                                                            ?>
                                                                                            <tr>
                                                                                                <td class="main" valign="top" colspan="2">
<?php
echo '<b>' . CHECKOUT_COMMENTS . '</b><br />';
echo CHECKOUT_TIP_COMMENTS . '<br />';
echo tep_draw_textarea_field('comments', 'soft', '5', '5', '', 'style="width:300px;height:40px"');
?>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td align="right" valign="top" width="150" style="padding-right:0">
                                                                                        <div class="checkout-order-total">
                                                                                            Calculating totals...
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>

                                                    </table>
                                                </div>
                                                <div style="float: right; margin-top: 10px;">
                                                    <div id="confirm_button">

<?php if (CHECKOUT_CONTRIB_TERMS_CONDITIONS) { ?>
                                                            <div class="checkout-form-error" id="ERROR_accept_terms"></div>
                                                            <input type="checkbox" name="accept_terms" value="1" /><?php echo sprintf(CHECKOUT_ACCEPT_TERMS, '<a href="#checkout-terms" class="checkout-lightbox-link">'); ?><br /><br />
<?php } ?>
                                                        <a href="#checkout-process" id="checkout-process-link">
                                                            <img src="<?php echo DIR_WS_LANGUAGES . $language . '/images/buttons/confirm_order.jpg'; ?>" border="0">
                                                        </a>
                                                    </div>
                                                </div>

                                            </div>
                                            </form>

                                            <div id="checkout-login-window" class="checkout-lightbox-inline">
                                                <div id="checkout-login">
                                                    <?php include(CHECKOUT_WS_TPL . 'checkout_login.php'); ?>
                                                </div>
                                            </div>
                                            <div id="checkout-process-window" class="checkout-lightbox-inline">
                                                <div id="checkout-process">
                                                    <?php include(CHECKOUT_WS_TPL . 'checkout_process.php'); ?>
                                                </div>
                                            </div>
                                            <div id="checkout-terms-window" class="checkout-lightbox-inline">
                                                <div id="checkout-terms">
                                            <?php include(CHECKOUT_WS_TPL . 'checkout_terms.php'); ?>
                                                </div>
                                            </div>
<?php if (CHECKOUT_CONTRIB_STS) { ?>
                                                <script type="text/javascript">try { Checkout.loadingTimer = setTimeout("Checkout.Init();", 4000); } catch(e) {}</script>
<?php } ?>

                                        </div>


                                        <div class="clear"></div>

                                    </div>

                                    <?php
                                    if (CHECKOUT_CONTRIB_STS_2) {
                                        $sts_block_name = 'columnleft2columnright';
                                        include(STS_RESTART_CAPTURE);
                                    } elseif (CHECKOUT_CONTRIB_STS_4) {
                                        $sts->restart_capture('content');
                                    }
                                    ?>
                                    <!-- footer_eof //-->
                                    <script type="text/javascript">try { Checkout.loadingTimer = setTimeout("Checkout.Init();", 4000); } catch(e) {}</script>
                                    <style>
                                        body{background: none;}
                                        #mainWrapper {
                                            background: none;
                                        }
                                    </style>
                                </div>
                            </div>
                        </div>
                        <!--
                        <div style="border: 1px solid #f5f5f5; width: 100%; position: relative;"></div>
                        <div style="border: 1px solid #f5f5f5; width: 100%; position: absolute; top: 98px;"></div>
                        <div style="margin-top: 20px;margin-bottom: 50px;"><center><span style="font-size: 12px; font-family: arial; text-align: center">Copyright &copy; 2010 - 2012 Motorfiend. All Rights Reserved</span></center></div>
-->
                    <div style="border: 1px solid #f5f5f5; width: 100%; margin-top: 35px;"></div>
                    <div style="width: 980px;margin: 0 auto;text-align: center;">
                        <span style="font-size: 12px; font-family: arial; "><p style="margin-top: 25px;">Copyright &copy; 2010 - 2012 Motorfiend. All Rights Reserved</p></span>
                    </div>
                    </body>

                    </html>