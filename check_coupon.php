<?php

/**
 * Short description of file
 *
 * Long description of file (if any)...
 *
 * @author your name
 * @copyright  2012 CYASoft
 * @license    CYASoft Licence 1.0
 * @version    1.0
 * @link       http://cyasoft.com 
 */
require('includes/application_top.php');
require(DIR_WS_CLASSES . 'order.php');
$order = new order;
require_once( DIR_WS_CLASSES . 'discount_coupon.php' );
$coupon = $_REQUEST['coupon'];
//kgt - discount coupons
if (tep_not_null($coupon) && is_object($order->coupon)) {
    $order->coupon->verify_code();
    if (MODULE_ORDER_TOTAL_DISCOUNT_COUPON_DEBUG != 'true') {
        if (!$order->coupon->is_errors()) {
            echo 1;
            /*if ($order->coupon->is_recalc_shipping())
                echo ENTRY_DISCOUNT_COUPON_SHIPPING_CALC_ERROR;*/
        } else {
            echo json_encode($order->coupon->get_messages());
           /* if (tep_session_is_registered('coupon'))
                tep_session_unregister('coupon');
            echo $order->coupon->get_messages(); //redirect to the payment page*/
        }
    }
} else {
    if (tep_session_is_registered('coupon')) {
        tep_session_unregister('coupon');
    }
}
//var_dump($order->coupon->get_messages());
//end kgt - discount coupons
?>
