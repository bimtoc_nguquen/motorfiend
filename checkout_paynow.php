<?php

/*
  $Id: checkout_success.php 1749 2007-12-21 04:23:36Z hpdl $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2007 osCommerce

  Released under the GNU General Public License
 */

require('includes/application_top.php');

// if the customer is not logged on, redirect them to the shopping cart page
if (!tep_session_is_registered('customer_id')) {
    tep_redirect(tep_href_link(FILENAME_SHOPPING_CART));
}

require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_CHECKOUT_SUCCESS);

function delete_product() {
    global $customer_id;
    tep_db_query(" delete from " . TABLE_PRODUCT_ADD_CART . " where user_id = " . $customer_id);
}

if (tep_session_is_registered('customer_id')) {
    delete_product();
}
require(DIR_WS_INCLUDES . 'template_top.php');
?>
<div id="cp_wraper">
    <div id="cp_header">
        Your Order Has Been Placed. <span style="color: #ce2625;">Thank You!</span>
    </div>
    <div id="cp_txt">Thank you for ordering from MotorFiend.com, if you have questions please contact us here.
    </div>
    <div id="cp_line1"><img src="images/layout/cp_line1.png"/></div>
    <div id="cp_content"><img src="images/layout/cp_content.png"/></div>
    <div id="cp_icon">
        <a href="http://twitter.com/share?url=<?php echo HTTP_SERVER;?>&text=<?php echo STORE_NAME; ?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" style="padding-right: 15px;"><img src="images/layout/cp_twitter.png"/></a>
        <a href="http://www.facebook.com/sharer.php?u=<?php echo HTTP_SERVER;?>&t=<?php echo STORE_NAME; ?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" style="padding-right: 15px;"><img src="images/layout/cp_fb.png"/></a>
        <a href="https://plus.google.com/share?url=<?php echo HTTP_SERVER;?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="images/layout/cp_google.png"/></a>
    </div>
    <div id="cp_line2"><img src="images/layout/cp_line2.png"/></div>
</div>

<?php
require(DIR_WS_INCLUDES . 'template_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
