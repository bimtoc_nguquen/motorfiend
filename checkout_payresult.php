<?php
/*
  $Id: checkout_payresult.php 1749 2007-12-21 04:23:36Z hpdl $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2007 osCommerce

  Released under the GNU General Public License
*/
  require('includes/application_top.php');
  
	// load the selected payment module
  require(DIR_WS_CLASSES . 'payment.php');
  $payment_modules = new payment('globebill');
  $payment_modules->update_status();
	 if (is_array($payment_modules->modules)) 
	 {
	 	$payment_modules->pre_confirmation_check();
	 }
	
	// load the selected order module
	require(DIR_WS_CLASSES . 'order.php');
	$order = new order;	
	
	require(DIR_WS_CLASSES . 'order_total.php');
	$order_total_modules = new order_total;
	$order_totals = $order_total_modules->process();
/////////////////////////////	
 $word = '----------------------------------'."\n";
	foreach($_POST as $name=>$vaule){
	$word.= $name.': '.$vaule."\n";
	}
	
	$fp = fopen("pay_log.txt","a");	
	flock($fp, LOCK_EX) ;
	fwrite($fp,$word."time: ".date("Y-m-d h:i:s")."\r\n");
	flock($fp, LOCK_UN); 
	fclose($fp);
/////////////////////////////	
	
	$BillNo = $_POST["BillNo"];
	$Currency = $_POST["Currency"];
	$Amount = $_POST["Amount"];
	$Succeed = $_POST["Succeed"];
	$TradeNo = $_POST["TradeNo"];
	$Result = $_POST["Result"];
	$MD5info = $_POST["MD5info"]; 
	$Remark = $_POST["Remark"];
	$Drawee = $_POST["Drawee"];
	if(strlen($BillNo) > 14)
	{
		$insert_id = substr($BillNo,14);
	}
	else
	{
		$Succeed = '0';
	}		
	
	$MD5key = MODULE_PAYMENT_GLOBEBILL_MD5KEY;
	$md5src = $BillNo.$Currency.$Amount.$Succeed.$MD5key;
	$md5sign = strtoupper(md5($md5src));
	
	
	if(($MD5info==$md5sign) && (($Succeed=='1') || (strtolower($Succeed) =='true')))
	{
  	$sql_data_array = array('orders_id' => $insert_id, 
                          	'orders_status_id' => '1', 
                          	'date_added' => 'now()', 
                          	'customer_notified' => '0',
                          	'comments' => 'Pay Success [ TradeNo: ' . $TradeNo . ' - Drawee: ' . $Drawee . ' - BillNo: ' . $BillNo . ' - Currency: ' . $Currency . ' - Amount:' . $Amount . ' - Succeed:' . $Succeed . ' - Result:' . $Result . ' - Remark:' . $Remark . ' ]');
  	tep_db_perform(TABLE_ORDERS_STATUS_HISTORY, $sql_data_array);
  	
  $payment_modules->after_process();
	}
	else
	{
		$messageStack->add('checkout_payresult', 'pay result: fail', 'error');
		$sql_data_array = array('orders_id' => $insert_id, 
                          	'orders_status_id' => '1', 
                          	'date_added' => 'now()', 
                          	'customer_notified' => '0',
                          	'comments' => 'Pay Fail [ TradeNo: ' . $TradeNo . ' - Drawee: ' . $Drawee . ' - BillNo: ' . $BillNo . ' - Currency: ' . $Currency . ' - Amount:' . $Amount . ' - Succeed:' . $Succeed . ' - Result:' . $Result . ' - Remark:' . $Remark . ' ]');
  	tep_db_perform(TABLE_ORDERS_STATUS_HISTORY, $sql_data_array);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
<div id="rs" style="font-size:12px; line-height:2; color:#ff0000; font-family:Arial, Helvetica, sans-serif; border:1px solid #ccc; font-weight:bold; padding:10px; margin:0px"></div>
<script type="text/javascript">
var time = 5; 
function Redirect(){
    top.location = "direct_option.php?payment=paypal_standard";
}
var i = 0;
function dis(){
    document.getElementById("rs").innerHTML = "Payment Faid &nbsp;(" + (time - i) + "). <br />Please Select Other Payment Method.";
    i++;
}
timer=setInterval('dis()', 1000); 
timer=setTimeout('Redirect()',time * 1000); 
</script>
</body>
</html>
<?php			
	}
?>