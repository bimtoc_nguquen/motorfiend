<?php
/*
  $Id: checkout_payresult.php 1749 2007-12-21 04:23:36Z hpdl $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2007 osCommerce

  Released under the GNU General Public License
*/

  require('includes/application_top.php');

// if the customer is not logged on, redirect them to the shopping cart page
//  if (!tep_session_is_registered('customer_id')) {
//    tep_redirect(tep_href_link(FILENAME_SHOPPING_CART));
//  }
  
  $messageStack->reset();

  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_CHECKOUT_PAYRESULT);

  $breadcrumb->add(NAVBAR_TITLE_1);
  $breadcrumb->add(NAVBAR_TITLE_2);
  
	// load the selected payment module
  require(DIR_WS_CLASSES . 'payment.php');
  $payment_modules = new payment('globebill');
  $payment_modules->update_status();
	if (is_array($payment_modules->modules)) 
	{
		$payment_modules->pre_confirmation_check();
	}
	
	// load the selected order module
	require(DIR_WS_CLASSES . 'order.php');
	$order = new order;	
	
	require(DIR_WS_CLASSES . 'order_total.php');
	$order_total_modules = new order_total;
	$order_totals = $order_total_modules->process();
	
	//订单号
	$BillNo = $_POST["BillNo"];
	//币种
	$Currency = $_POST["Currency"];
	//金额
	$Amount = $_POST["Amount"];
	//支付状态
	$Succeed = $_POST["Succeed"];
	//支付平台流水号
	$TradeNo = $_POST["TradeNo"];
	//支付结果
	$Result = $_POST["Result"];
	//取得的MD5校验信息
	$MD5info = $_POST["MD5info"]; 
	//备注
	$Remark = $_POST["Remark"];
	//支付人名称
	$Drawee = $_POST["Drawee"];
	if(strlen($BillNo) > 14)
	{
		$insert_id = substr($BillNo,14);
	}
	else
	{
		$Succeed = '0';
	}		
	
	$MD5key = MODULE_PAYMENT_GLOBEBILL_MD5KEY;
	//校验源字符串
	$md5src = $BillNo.$Currency.$Amount.$Succeed.$MD5key;
	//MD5检验结果
	$md5sign = strtoupper(md5($md5src));
	
	if(($MD5info==$md5sign) && (($Succeed=='1') || (strtolower($Succeed) =='true')))
	{
		$messageStack->add('checkout_payresult', 'pay result: success', 'success');
  	$sql_data_array = array('orders_id' => $insert_id, 
                          	'orders_status_id' => '1', 
                          	'date_added' => 'now()', 
                          	'customer_notified' => '0',
                          	'comments' => 'Pay Success [ TradeNo: ' . $TradeNo . ' - Drawee: ' . $Drawee . ' - BillNo: ' . $BillNo . ' - Currency: ' . $Currency . ' - Amount:' . $Amount . ' - Succeed:' . $Succeed . ' - Result:' . $Result . ' - Remark:' . $Remark . ' ]');
  	tep_db_perform(TABLE_ORDERS_STATUS_HISTORY, $sql_data_array);
  	
  	// lets start with the email confirmation
//  $email_order = STORE_NAME . "\n" . 
//                 EMAIL_SEPARATOR . "\n" . 
//                 EMAIL_TEXT_ORDER_NUMBER . ' ' . $insert_id . "\n" .
//                 EMAIL_TEXT_INVOICE_URL . ' ' . tep_href_link(FILENAME_ACCOUNT_HISTORY_INFO, 'order_id=' . $insert_id, 'SSL', false) . "\n" .
//                 EMAIL_TEXT_DATE_ORDERED . ' ' . strftime(DATE_FORMAT_LONG) . "\n\n";
//  if ($sql_data_array['comments']) {
//    $email_order .= tep_db_output($sql_data_array['comments']) . "\n\n";
//  }
//  $products_ordered = '';
//  if (isset($_SESSION["products_ordered"]) && $_SESSION["products_ordered"]!='' ) {
//  	$products_ordered = $_SESSION["products_ordered"];
//  }
//  else {
//  	  for ($i=0, $n=sizeof($order->products); $i<$n; $i++) {
//  	  	$products_ordered .= $order->products[$i]['qty'] . ' x ' . $order->products[$i]['name'] . ' (' . $order->products[$i]['model'] . ') = ' . $currencies->display_price($order->products[$i]['final_price'], $order->products[$i]['tax'], $order->products[$i]['qty']) . "\n";
//  	  }
//  }   
//
//  $email_order .= EMAIL_TEXT_PRODUCTS . "\n" . 
//                  EMAIL_SEPARATOR . "\n" . 
//                  $products_ordered . 
//                  EMAIL_SEPARATOR . "\n";
//
//  for ($i=0, $n=sizeof($order_totals); $i<$n; $i++) {
//    $email_order .= strip_tags($order_totals[$i]['title']) . ' ' . strip_tags($order_totals[$i]['text']) . "\n";
//  }
//
//  if ($order->content_type != 'virtual') {
//    $email_order .= "\n" . EMAIL_TEXT_DELIVERY_ADDRESS . "\n" . 
//                    EMAIL_SEPARATOR . "\n" .
//                    tep_address_label($customer_id, $sendto, 0, '', "\n") . "\n";
//  }
//
//  $email_order .= "\n" . EMAIL_TEXT_BILLING_ADDRESS . "\n" .
//                  EMAIL_SEPARATOR . "\n" .
//                  tep_address_label($customer_id, $billto, 0, '', "\n") . "\n\n";
//
//    $email_order .= EMAIL_TEXT_PAYMENT_METHOD . "\n" . 
//                    EMAIL_SEPARATOR . "\n";
////    $payment_class = $$payment;
//    $email_order .= $order->info['payment_method'] . "\n\n";
////    if ($payment_class->email_footer) { 
////      $email_order .= $payment_class->email_footer . "\n\n";
////    }
//
//	define('SEND_EMAILS', 'true');
//  tep_mail($order->customer['firstname'] . ' ' . $order->customer['lastname'], $order->customer['email_address'], EMAIL_TEXT_SUBJECT, $email_order, STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS);
//
//// send emails to other people
//  if (SEND_EXTRA_ORDER_EMAILS_TO != '') {
//    tep_mail('', SEND_EXTRA_ORDER_EMAILS_TO, EMAIL_TEXT_SUBJECT, $email_order, STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS);
//  }

// load the after_process function from the payment modules
  $payment_modules->after_process();

  $cart->reset(true);

// unregister session variables used during checkout
  tep_session_unregister('sendto');
  tep_session_unregister('billto');
  tep_session_unregister('shipping');
  tep_session_unregister('payment');
  tep_session_unregister('comments');


	}
	else
	{
		$messageStack->add('checkout_payresult', 'pay result: fail', 'error');
		$sql_data_array = array('orders_id' => $insert_id, 
                          	'orders_status_id' => '1', 
                          	'date_added' => 'now()', 
                          	'customer_notified' => '0',
                          	'comments' => 'Pay Fail [ TradeNo: ' . $TradeNo . ' - Drawee: ' . $Drawee . ' - BillNo: ' . $BillNo . ' - Currency: ' . $Currency . ' - Amount:' . $Amount . ' - Succeed:' . $Succeed . ' - Result:' . $Result . ' - Remark:' . $Remark . ' ]');
  	tep_db_perform(TABLE_ORDERS_STATUS_HISTORY, $sql_data_array);
	}


  if (isset($HTTP_GET_VARS['action']) && ($HTTP_GET_VARS['action'] == 'update')) {
    $notify_string = '';

    if (isset($HTTP_POST_VARS['notify']) && !empty($HTTP_POST_VARS['notify'])) {
      $notify = $HTTP_POST_VARS['notify'];

      if (!is_array($notify)) {
        $notify = array($notify);
      }

      for ($i=0, $n=sizeof($notify); $i<$n; $i++) {
        if (is_numeric($notify[$i])) {
          $notify_string .= 'notify[]=' . $notify[$i] . '&';
        }
      }

      if (!empty($notify_string)) {
        $notify_string = 'action=notify&' . substr($notify_string, 0, -1);
      }
    }

    tep_redirect(tep_href_link(FILENAME_DEFAULT, $notify_string));
  }

  $global_query = tep_db_query("select global_product_notifications from " . TABLE_CUSTOMERS_INFO . " where customers_info_id = '" . (int)$customer_id . "'");
  $global = tep_db_fetch_array($global_query);

  if ($global['global_product_notifications'] != '1') {
    $orders_query = tep_db_query("select orders_id from " . TABLE_ORDERS . " where customers_id = '" . (int)$customer_id . "' order by date_purchased desc limit 1");
    $orders = tep_db_fetch_array($orders_query);

    $products_array = array();
    $products_query = tep_db_query("select products_id, products_name from " . TABLE_ORDERS_PRODUCTS . " where orders_id = '" . (int)$orders['orders_id'] . "' order by products_name");
    while ($products = tep_db_fetch_array($products_query)) {
      $products_array[] = array('id' => $products['products_id'],
                                'text' => $products['products_name']);
    }
  }

 // require(DIR_WS_INCLUDES . 'template_top.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php echo HTML_PARAMS; ?>>
<head profile="http://www.w3.org/2005/10/profile">
<?php
/*** Begin Header Tags SEO ***/
if ( file_exists(DIR_WS_INCLUDES . 'header_tags.php') ) {
  require(DIR_WS_INCLUDES . 'header_tags.php');
} else {
?>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title><?php echo TITLE; ?></title>
<?php
}
/*** End Header Tags SEO ***/
?>
<base href="<?php echo (($request_type == 'SSL') ? HTTPS_SERVER : HTTP_SERVER) . DIR_WS_CATALOG; ?>" />
<link rel="stylesheet" type="text/css" href="ext/jquery/ui/overcast/jquery-ui-1.8.6.css" />
<script type="text/javascript" src="ext/jquery/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="ext/jquery/ui/jquery-ui-1.8.6.min.js"></script>

<?php
  if (tep_not_null(JQUERY_DATEPICKER_I18N_CODE)) {
?>
<script type="text/javascript" src="ext/jquery/ui/i18n/jquery.ui.datepicker-<?php echo JQUERY_DATEPICKER_I18N_CODE; ?>.js"></script>
<script type="text/javascript">
$.datepicker.setDefaults($.datepicker.regional['<?php echo JQUERY_DATEPICKER_I18N_CODE; ?>']);
</script>
<?php
  }
?>

<script type="text/javascript" src="ext/jquery/bxGallery/jquery.bxGallery.1.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="ext/jquery/fancybox/jquery.fancybox-1.3.4.css" />
<script type="text/javascript" src="ext/jquery/jquery.selectbox/js/jquery.selectbox-0.1.3.min.js"></script>
<link rel="stylesheet" type="text/css" href="ext/jquery/jquery.selectbox/css/jquery.selectbox.css" />
 <link rel="stylesheet" type="text/css" href="stylesheet.css" />
 <script type="text/javascript">
  setInterval(checkScreen, 1);
  checkScreen();
  function checkScreen(){
     if ( $(window).width() < 1149 ){
$("#headerShortcuts").css({
  "background": "#f7f7f7",
  "color": "#2e2e2e",
  "text-align": "right",
  "margin": "0 auto 0 auto",
  "width": "975px",
  "border-width": "0 1px 1px 1px",
  "border-style": "solid",
  "border-color": "#efefef"
});
$("#bodyWrapper").css({
 "width": "auto",
 "max-width": "975px",
 "margin": "0 auto 0 auto",
 "position": "relative",
 "border-width": "0 1px 0 1px",
 "border-style": "solid",
 "border-color": "#efefef",
 "background": "#ffffff"
});
$("#mainWrapper").css({
 "width": "auto",
 "margin": "0 auto",
 "background": "none",
});
     } else {
$("#mainWrapper").css({
 "width":"1149px",
 "margin": "0 auto",
 "background": "url(/images/layout/main-back.png) repeat-y"
});
$("#bodyWrapper").css({
 "width": "975px",
 "margin": "0 auto 0 90px",
 "position": "relative",
 "border": "none"
});
$("#headerShortcuts").css({
  "background": "#f7f7f7",
  "color": "#2e2e2e",
  "text-align": "right",
  "margin": "0 auto 0 75px",
  "width": "1006px",
  "border": "none",
  "border-bottom": "1px solid #efefef"
});
   }
  }
 </script>
<!--[if IE]>
 <link rel="stylesheet" type="text/css" href="stylesheet-ie.css" />
<![endif]-->
<?php echo $oscTemplate->getBlocks('header_tags'); ?>
<?php
if (basename($_SERVER['SCRIPT_NAME']) == FILENAME_CHECKOUT_SHIPPING){
  if (CHECKOUT_CONTRIB_STS_4) {
    $sts->restart_capture();
  }

  $checkout_stylesheet = '<link rel="stylesheet" type="text/css" href="' . CHECKOUT_WS_CSS . 'checkout.css" />';

  if (CHECKOUT_CONTRIB_STS_4) {
    echo $checkout_stylesheet;
    $sts->restart_capture('checkout_stylesheet');
  } elseif (CHECKOUT_CONTRIB_STS_2) {
    $sts_block_name = 'checkout_stylesheet';
    $template[$sts_block_name] = $checkout_stylesheet;

  } else {
    echo $checkout_stylesheet;
  }
?>
  <script type="text/javascript" src="<?php echo CHECKOUT_WS_JS; ?>lightbox.js"></script>
  <script type="text/javascript"><?php include(CHECKOUT_WS_JS . 'checkout.js.php'); ?></script>
<?php } else { ?>
  <script type="text/javascript" src="ext/jquery/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<?php } ?>
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>
 <link rel="icon" type="image/png" href="favicon.png" />
 <script type="text/javascript">
var time = 5; //时间,秒
function Redirect(){
    top.location = "index.php";
}
var i = 0;
function dis(){
    document.getElementById("rs").innerHTML = "Go back to homepage after " + (time - i) + " seconds";
    i++;
}
timer=setInterval('dis()', 1000); //显示时间
timer=setTimeout('Redirect()',time * 1000); //跳转
</script>
</head>
<body>

    	<?php echo tep_draw_form('order', tep_href_link(FILENAME_CHECKOUT_PAYRESULT, 'action=update', 'SSL')); ?>
    	<table border="0" width="100%" cellspacing="0" cellpadding="0">
      <tr>
        <td><table border="0" width="100%" cellspacing="4" cellpadding="2">
          <tr>
            <td valign="top"><?php echo tep_image(DIR_WS_IMAGES . 'table_background_man_on_board.gif', HEADING_TITLE); ?></td>
            <td valign="top" class="main">
            	<?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?>
            	<div align="center" class="pageHeading">
            		<?php 
            		echo $messageStack->output('checkout_payresult'); 
            		?>
            	</div><br><br><br>
<?php
  if ($global['global_product_notifications'] != '1') {
    echo TEXT_NOTIFY_PRODUCTS . '<br><p class="productsNotifications">';

    $products_displayed = array();
    for ($i=0, $n=sizeof($products_array); $i<$n; $i++) {
      if (!in_array($products_array[$i]['id'], $products_displayed)) {
        echo tep_draw_checkbox_field('notify[]', $products_array[$i]['id']) . ' ' . $products_array[$i]['text'] . '<br>';
        $products_displayed[] = $products_array[$i]['id'];
      }
    }

    echo '</p>';
  } else {
    echo TEXT_SEE_ORDERS . '<br><br>' . TEXT_CONTACT_STORE_OWNER;
  }
?>
            <h3><?php echo TEXT_THANKS_FOR_SHOPPING; ?></h3>
			<p id="rs"></p></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
      </tr>
      <tr>
        <td align="right" class="main">
<?php //echo tep_image_submit('button_continue.gif', IMAGE_BUTTON_CONTINUE); ?></td>
      </tr>
      <!--<tr>
        <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td width="25%"><table border="0" width="100%" cellspacing="0" cellpadding="0">
              <tr>
                <td width="50%" align="right"><?php echo tep_draw_separator('pixel_silver.gif', '1', '5'); ?></td>
                <td width="50%"><?php echo tep_draw_separator('pixel_silver.gif', '100%', '1'); ?></td>
              </tr>
            </table></td>
            <td width="25%"><?php echo tep_draw_separator('pixel_silver.gif', '100%', '1'); ?></td>
            <td width="25%"><?php echo tep_draw_separator('pixel_silver.gif', '100%', '1'); ?></td>
            <td width="25%"><table border="0" width="100%" cellspacing="0" cellpadding="0">
              <tr>
                <td width="50%"><?php echo tep_draw_separator('pixel_silver.gif', '100%', '1'); ?></td>
                <td width="50%"><?php echo tep_image(DIR_WS_IMAGES . 'checkout_bullet.gif'); ?></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td align="center" width="25%" class="checkoutBarFrom"><?php echo CHECKOUT_BAR_DELIVERY; ?></td>
            <td align="center" width="25%" class="checkoutBarFrom"><?php echo CHECKOUT_BAR_PAYMENT; ?></td>
            <td align="center" width="25%" class="checkoutBarFrom"><?php echo CHECKOUT_BAR_CONFIRMATION; ?></td>
            <td align="center" width="25%" class="checkoutBarCurrent"><?php echo CHECKOUT_BAR_FINISHED; ?></td>
          </tr>
        </table></td>
      </tr>-->
<?php if (DOWNLOAD_ENABLED == 'true') include(DIR_WS_MODULES . 'downloads.php'); ?>
    </table></form>
<!-- body_eof //-->


</body>
</html>
<?php
  //require(DIR_WS_INCLUDES . 'template_bottom.php');
  require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
