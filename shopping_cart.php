<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

  require("includes/application_top.php");

	

  if ($cart->count_contents() > 0) {
    include(DIR_WS_CLASSES . 'payment.php');
    $payment_modules = new payment;
  }

  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_SHOPPING_CART);

  $breadcrumb->add(NAVBAR_TITLE, tep_href_link(FILENAME_SHOPPING_CART));

  require(DIR_WS_INCLUDES . 'template_top.php');
?>



<?php
  if ($cart->count_contents() > 0) {
?>

<?php echo tep_draw_form('cart_quantity', tep_href_link(FILENAME_SHOPPING_CART, 'action=update_product')); ?>
<h1 id="cart-header"><?php echo HEADING_TITLE; ?></h1>
<div class="contentContainer">

  <div class="contentText">

<?php


      require(DIR_WS_CLASSES . 'order.php');
      $order = new order;
      

    $any_out_of_stock = 0;
    $products = $cart->get_products();
    for ($i=0, $n=sizeof($products); $i<$n; $i++) {
// Push all attributes information in an array
      if (isset($products[$i]['attributes']) && is_array($products[$i]['attributes'])) {
        while (list($option, $value) = each($products[$i]['attributes'])) {
          echo tep_draw_hidden_field('id[' . $products[$i]['id'] . '][' . $option . ']', $value);
          $attributes = tep_db_query("select popt.products_options_name, poval.products_options_values_name, pa.options_values_price, pa.price_prefix
                                      from " . TABLE_PRODUCTS_OPTIONS . " popt, " . TABLE_PRODUCTS_OPTIONS_VALUES . " poval, " . TABLE_PRODUCTS_ATTRIBUTES . " pa
                                      where pa.products_id = '" . (int)$products[$i]['id'] . "'
                                       and pa.options_id = '" . (int)$option . "'
                                       and pa.options_id = popt.products_options_id
                                       and pa.options_values_id = '" . (int)$value . "'
                                       and pa.options_values_id = poval.products_options_values_id
                                       and popt.language_id = '" . (int)$languages_id . "'
                                       and poval.language_id = '" . (int)$languages_id . "'");
          $attributes_values = tep_db_fetch_array($attributes);

          //clr 030714 determine if attribute is a text attribute and assign to $attr_value temporarily
          if ($value == PRODUCTS_OPTIONS_VALUE_TEXT_ID) {
            echo tep_draw_hidden_field('id[' . $products[$i]['id'] . '][' . TEXT_PREFIX . $option . ']',  $products[$i]['attributes_values'][$option]);
            $attr_value = $products[$i]['attributes_values'][$option];
          } else {
            echo tep_draw_hidden_field('id[' . $products[$i]['id'] . '][' . $option . ']', $value);
            $attr_value = $attributes_values['products_options_values_name'];
          }

          $products[$i][$option]['products_options_name'] = $attributes_values['products_options_name'];
          $products[$i][$option]['options_values_id'] = $value;
          //clr 030714 assign $attr_value
          $products[$i][$option]['products_options_values_name'] = $attr_value ;
          $products[$i][$option]['options_values_price'] = $attributes_values['options_values_price'];
          $products[$i][$option]['price_prefix'] = $attributes_values['price_prefix'];
        }
      }
    }
?>

    <table border="0" width="100%" cellspacing="0" cellpadding="0" id="cart-table">
     <tr>
      <th colspan="2"><h2>Your Shopping Cart</h2></th>
      <th>Item Price</th>
      <th style="width: 100px; text-align: center;">Qty</th>
      <th>Subtotal</th>
     </tr>

<?php

    for ($i=0, $n=sizeof($products); $i<$n; $i++) {
      $products_name = '  <tr>' .
                       '    <td align="center" style="width: 150px"><a class="cart-table-image" href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $products[$i]['id']) . '">' . tep_image(DIR_WS_IMAGES . $products[$i]['image'], $products[$i]['name'], SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT) . '</a></td>' .
                       '    <td valign="top" align="left"><a class="cart-table-product" href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $products[$i]['id']) . '"><strong>' . $products[$i]['name'] . '</strong></a>';

      if (STOCK_CHECK == 'true') {
        $stock_check = tep_check_stock($products[$i]['id'], $products[$i]['quantity']);
        if (tep_not_null($stock_check)) {
          $any_out_of_stock = 1;

          $products_name .= $stock_check;
        }
      }

      $products_name .= tep_draw_hidden_field('products_id[]', $products[$i]['id']) . ' <a href="' . tep_href_link(FILENAME_SHOPPING_CART, 'products_id=' . $products[$i]['id'] . '&action=remove_product') . '" class="cart-table-remove">remove</a>';
      
      if (isset($products[$i]['attributes']) && is_array($products[$i]['attributes'])) {
        reset($products[$i]['attributes']);
        while (list($option, $value) = each($products[$i]['attributes'])) {
          $products_name .= '<div style="clear:both;" ><div class="cart-table-attributes"> - ' . $products[$i][$option]['products_options_name'] . ' ' . $products[$i][$option]['products_options_values_name'] . '</div>';
        }
      }

      

      $products_name .= '    </td>';
      echo $products_name .
           '        <td class="cart-table-price">' . $currencies->format($products[$i]['final_price']) . '</td>' .
           '        <td class="cart-table-qty">' . tep_draw_input_field('cart_quantity[]', $products[$i]['quantity'], 'size="4"') . '</td>' .
           '        <td align="right" valign="top" class="cart-table-price"><strong>' . $currencies->display_price($products[$i]['final_price'], tep_get_tax_rate($products[$i]['tax_class_id']), $products[$i]['quantity']) . '</strong></td>' .
           '      </tr>';
    }
?>

    </table>
    <p align="right" id="shopping-cart-subtotal"><strong><?php echo SUB_TITLE_SUB_TOTAL; ?> <span class="motogreen"><?php echo $currencies->format($cart->show_total()); ?></span></strong></p>

<?php
    if ($any_out_of_stock == 1) {
      if (STOCK_ALLOW_CHECKOUT == 'true') {
?>

    <p class="stockWarning" align="center"><?php echo OUT_OF_STOCK_CAN_CHECKOUT; ?></p>

<?php
      } else {
?>

    <p class="stockWarning" align="center"><?php echo OUT_OF_STOCK_CANT_CHECKOUT; ?></p>

<?php
      }
    }
?>

  </div>

  <div class="buttonSet">
    <span style="float: right"><a class="btn-checkout" href="<?php echo tep_href_link(FILENAME_CHECKOUT_SHIPPING,'','SSL'); ?>"><?php echo IMAGE_BUTTON_CHECKOUT; ?></a></span>
    <span style="float: right"><?php echo tep_draw_button(IMAGE_BUTTON_UPDATE,null,null,null,array('type'=>'submit','params' => 'class="btn-update"')); ?></span>
    <span style="float: right"><a class="btn-continue" href="<?php echo tep_href_link(FILENAME_DEFAULT); ?>"><?php echo IMAGE_BUTTON_CONTINUE; ?></a></span>
    <span style="float: left">
        <div class="btn-coupon">&nbsp;</div>
    </span>
    <span style="float: left">
        <div id="input_coupon"><input type="text" class="input_coupon1" name="coupon" value="" id="" placeholder="Enter Your Coupon Here!"></div>
        <div class="coupon_msg"></div>
    </span>
  </div>
    <script type="text/javascript">
        $(document).ready(function() { 
            $('.coupon_msg').hide();
            $('.btn-coupon').bind('click', function() {              
                var coupon=$('input.input_coupon1').val();
                if($.trim(coupon)=='')
                    {
                        $('.coupon_msg').show();
                        $(".coupon_msg").html('Please enter your coupon code').css("color","red");
                    }
                    else {
                        $.ajax({
                        url: 'check_coupon.php',
                        type: 'post',
                        data: {'coupon':coupon},
                        dataType:'json',
                        success: function(html){
                            $('.coupon_msg').show();
                            if($.trim(html)=='1')
                                {
                                    $(".coupon_msg").html('Your Coupon Code Was Accepted!').css("color","blue");
                                }
                            else{
                                $(".coupon_msg").html("Sorry this coupon doesn't work or is incorrect, bummer").css("color","red");
                            }
                                
                        }
                });
                    }
                
            });
        });
    </script>
<?php
    $initialize_checkout_methods = $payment_modules->checkout_initialization_method();

    if (!empty($initialize_checkout_methods)) {
?>

  <p align="right" style="clear: both; padding: 15px 50px 0 0;"><?php echo TEXT_ALTERNATIVE_CHECKOUT_METHODS; ?></p>

<?php
      reset($initialize_checkout_methods);
      while (list(, $value) = each($initialize_checkout_methods)) {
?>

  <p align="right"><?php echo $value; ?></p>

<?php
      }
    }
?>
</form>
<?php
          // *** BEGIN GOOGLE CHECKOUT ***
          if (defined('MODULE_PAYMENT_GOOGLECHECKOUT_STATUS') && MODULE_PAYMENT_GOOGLECHECKOUT_STATUS == 'True') {
            include_once('googlecheckout/gcheckout.php');
          }
          // *** END GOOGLE CHECKOUT ***
?>

</div>

<?php
  } else {
?>

<div class="contentContainer">
  <div class="contentTextempty" >
    <?php echo TEXT_CART_EMPTY; ?>
      <p align='center' style="padding-top: 5px;">
    <a class="btn_back_sc" href="<?php echo tep_href_link(FILENAME_DEFAULT); ?>"><?php echo IMAGE_BUTTON_CONTINUE; ?></a></p>
  </div>
</div>

<?php
  }

  require(DIR_WS_INCLUDES . 'template_bottom.php');
  require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
