<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

  require('includes/application_top.php');

  require(DIR_WS_LANGUAGES . $language . '/sitemap.php');

  $breadcrumb->add(NAVBAR_TITLE, tep_href_link('Sitemap'));

  require(DIR_WS_INCLUDES . 'template_top.php');
?>

<h1>Sitemap</h1>

<div class="contentContainer">
  <div class="contentText">
     <table>
      <tr><td width="50%" class="main" valign="top">
<?php require DIR_WS_CLASSES . 'category_tree.php'; $osC_CategoryTree = new osC_CategoryTree; echo $osC_CategoryTree->buildTree(); ?></td>
            <td width="50%" class="main" valign="top">
              <ul>
                <li><?php echo '<a href="' . tep_href_link(FILENAME_CONTACT, '', 'SSL') . '">' . PAGE_CONTACT . '</a>'; ?></li>
                <li><?php echo '<a href="' . tep_href_link(FILENAME_ACCOUNT, '', 'SSL') . '">' . PAGE_ACCOUNT . '</a>'; ?></li>
                <ul>
                  <li><?php echo '<a href="' . tep_href_link(FILENAME_ACCOUNT_EDIT, '', 'SSL') . '">' . PAGE_ACCOUNT_EDIT . '</a>'; ?></li>
                  <li><?php echo '<a href="' . tep_href_link(FILENAME_ADDRESS_BOOK, '', 'SSL') . '">' . PAGE_ADDRESS_BOOK . '</a>'; ?></li>
                  <li><?php echo '<a href="' . tep_href_link(FILENAME_ACCOUNT_HISTORY, '', 'SSL') . '">' . PAGE_ACCOUNT_HISTORY . '</a>'; ?></li>
                  <li><?php echo '<a href="' . tep_href_link(FILENAME_ACCOUNT_NEWSLETTERS, '', 'SSL') . '">' . PAGE_ACCOUNT_NOTIFICATIONS . '</a>'; ?></li>
                </ul>
                  <li><?php echo '<a href="' . tep_href_link(FILENAME_SHOPPING_CART) . '">' . PAGE_SHOPPING_CART . '</a>'; ?></li>
                  <li><?php echo '<a href="' . tep_href_link(FILENAME_CHECKOUT_SHIPPING, '', 'SSL') . '">' . PAGE_CHECKOUT_SHIPPING . '</a>'; ?></li>
                  <li><?php echo '<a href="' . tep_href_link(FILENAME_ADVANCED_SEARCH) . '">' . PAGE_ADVANCED_SEARCH . '</a>'; ?></li>
                  <li><?php echo '<a href="' . tep_href_link(FILENAME_PRODUCTS_NEW) . '">' . PAGE_PRODUCTS_NEW . '</a>'; ?></li>
                  <li><?php echo '<a href="' . tep_href_link(FILENAME_SPECIALS) . '">' . PAGE_SPECIALS . '</a>'; ?></li>
                  <li><?php echo '<a href="' . tep_href_link(FILENAME_REVIEWS) . '">' . PAGE_REVIEWS . '</a>'; ?></li>
<?php
// Extra Info Pages
// BEGIN

  $information_query = mysql_query('SELECT information_id, language_id, information_title FROM ' . TABLE_INFORMATION .' WHERE language_id ='.$languages_id.' ORDER BY information_title')

    or die(mysql_error());

 while ($page = tep_db_fetch_array($information_query))
 {
      $rows++;
      $status_query = mysql_query('SELECT visible FROM ' . TABLE_INFORMATION .' WHERE information_id='.$page['information_id'])

      or die(mysql_error());


      $status = tep_db_fetch_array($status_query);

      if($status['visible'])
      {
            $link = FILENAME_INFORMATION . '?information_id=' . $page['information_id'];
          echo '<li><a href="' . tep_href_link($link) . '">' . $page['information_title'] . '</a></li>';
      }
  }
// Extra Info Pages
// END
?>
              </ul>
            </td>
          </tr></table>


  </div>

  <div class="buttonSet">
    <span class="buttonAction"><?php echo tep_draw_button(IMAGE_BUTTON_CONTINUE, 'triangle-1-e', tep_href_link(FILENAME_DEFAULT)); ?></span>
  </div>
</div>

<?php
  require(DIR_WS_INCLUDES . 'template_bottom.php');
  require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
