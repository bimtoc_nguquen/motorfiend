<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
 */

require('includes/application_top.php');

require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_PRODUCT_INFO);

$product_check_query = tep_db_query("select count(*) as total from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION . " pd where p.products_status = '1' and p.products_id = '" . (int) $HTTP_GET_VARS['products_id'] . "' and pd.products_id = p.products_id and pd.language_id = '" . (int) $languages_id . "'");
$product_check = tep_db_fetch_array($product_check_query);
if ($product_check['total'] > 0) {
    $product_info_query = tep_db_query("select p.products_id, pd.products_code_video, pd.products_name, pd.products_description, p.products_model, p.products_quantity, p.products_image, pd.products_url, p.products_price, p.products_tax_class_id, p.products_date_added, p.products_date_available, p.manufacturers_id, p.products_status from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION . " pd where p.products_status = '1' and p.products_id = '" . (int) $HTTP_GET_VARS['products_id'] . "' and pd.products_id = p.products_id and pd.language_id = '" . (int) $languages_id . "'");
    $product_info = tep_db_fetch_array($product_info_query);
}
require(DIR_WS_INCLUDES . 'template_top_popup.php');

if ($product_check['total'] < 1) {
    ?>

    <div class="contentContainer">
        <div class="contentText">
            <?php echo TEXT_PRODUCT_NOT_FOUND; ?>
        </div>

        <div style="float: right;">
            <?php echo tep_draw_button(IMAGE_BUTTON_CONTINUE, 'triangle-1-e', tep_href_link(FILENAME_DEFAULT)); ?>
        </div>
    </div>

    <?php
} else {
    tep_db_query("update " . TABLE_PRODUCTS_DESCRIPTION . " set products_viewed = products_viewed+1 where products_id = '" . (int) $HTTP_GET_VARS['products_id'] . "' and language_id = '" . (int) $languages_id . "'");

    if ($new_price = tep_get_products_special_price($product_info['products_id'])) {
        $products_price = '<del>' . $currencies->display_price($product_info['products_price'], tep_get_tax_rate($product_info['products_tax_class_id'])) . '</del> <span class="productSpecialPrice">' . $currencies->display_price($new_price, tep_get_tax_rate($product_info['products_tax_class_id'])) . '</span>';
    } else {
        $products_price = $currencies->display_price($product_info['products_price'], tep_get_tax_rate($product_info['products_tax_class_id']));
    }

    if (tep_not_null($product_info['products_model'])) {
        $products_name = $product_info['products_name'] . '<br />' . star_reviews($product_info['products_id']) .
                '<span class="smallText">SKU: ' . $product_info['products_model'] . '</span>';
    } else {
        $products_name = $product_info['products_name'] . '<br />' . star_reviews($product_info['products_id']);
    }
    ?>

    <?php echo tep_draw_form('cart_quantity', tep_href_link(FILENAME_PRODUCT_INFO, tep_get_all_get_params(array('action')) . 'action=add_product')); ?>

    <div class="contentContainer">
        <div class="pd_img_wrap_popup">
            <?php
            if (tep_not_null($product_info['products_image'])) {
                $pi_query = tep_db_query("select image from " . TABLE_PRODUCTS_IMAGES . " where products_id = '" . (int) $product_info['products_id'] . "' order by sort_order");

                if (tep_db_num_rows($pi_query) > 0) {
                    ?>
                    <div style="float: left;position: relative;">
                        <div id="piGalNew" class="svwp">
                            <ul id="player_images_popup" style="left: -<?php echo 600*($HTTP_GET_VARS['img'] - 1); ?>px; ">

                                <?php
                                $pi_counter = 0;
                                while ($pi = tep_db_fetch_array($pi_query)) {
                                    $pi_counter++;
                                        $pi_entry = '<li>' . tep_image(DIR_WS_IMAGES . $pi['image'], $product_info['products_name'], '600', '554');
                                        $pi_entry .= '</li>';
                                        echo $pi_entry;
                                        ?>
                                
                                <?php
                                }
                                ?>
                            </ul>                    
                        </div>
                        <
                        <div id="show_counter_images" style="position: absolute; top:56px; text-align: center; width: 100%"><?php echo 'image '.$HTTP_GET_VARS['img'].' of '.$pi_counter ; ?></div>
                    </div>
                    <?php
                } else {
                    ?>
                    <div style="float: left; margin-top: 9px; position: relative;">
                        <div id="piGalNew" class="svwp">
                            <ul id="player_images_popup">
                                <?php
                                $pi_entry = '<a href="' . DIR_WS_IMAGES . $product_info['products_image'] . '" title="' . $product_info['products_name'] . '" rel="lightbox"><li>' . tep_image(DIR_WS_IMAGES . $product_info['products_image'], $product_info['products_name'], '308', '210');
                                $pi_entry .= '</li></a>';
                                echo $pi_entry;
                                ?>
                            </ul>                        
                        </div>

                    </div>

                    <?php
                }
                ?>
            </div>
            <?php
        }
        ?>
        <div style="clear: both;"></div>
        <?php /*         * * Begin Header Tags SEO ** */ ?>

        <?php
//        if ((USE_CACHE == 'true') && empty($SID)) {
//            echo tep_cache_also_purchased(3600);
//        } else {
//            include(DIR_WS_MODULES . FILENAME_ALSO_PURCHASED_PRODUCTS);
//        }
//        include(DIR_WS_MODULES . FILENAME_RELATED_PRODUCTS);
//        
        ?>
    </div>



    <?php
}

require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
