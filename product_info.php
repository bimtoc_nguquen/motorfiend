<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
 */

require('includes/application_top.php');

require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_PRODUCT_INFO);

$product_check_query = tep_db_query("select count(*) as total from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION . " pd where p.products_status = '1' and p.products_id = '" . (int) $HTTP_GET_VARS['products_id'] . "' and pd.products_id = p.products_id and pd.language_id = '" . (int) $languages_id . "'");
$product_check = tep_db_fetch_array($product_check_query);
if ($product_check['total'] > 0) {
    $product_info_query = tep_db_query("select p.products_id, pd.products_code_video, pd.products_name, pd.products_description, p.products_model, p.products_quantity, p.products_image, pd.products_url, p.products_price, p.products_tax_class_id, p.products_date_added, p.products_date_available, p.manufacturers_id, p.products_status from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION . " pd where p.products_status = '1' and p.products_id = '" . (int) $HTTP_GET_VARS['products_id'] . "' and pd.products_id = p.products_id and pd.language_id = '" . (int) $languages_id . "'");
    $product_info = tep_db_fetch_array($product_info_query);
}
require(DIR_WS_INCLUDES . 'template_top.php');

if ($product_check['total'] < 1) {
    ?>

    <div class="contentContainer">
        <div class="contentText">
            <?php echo TEXT_PRODUCT_NOT_FOUND; ?>
        </div>

        <div style="float: right;">
            <?php echo tep_draw_button(IMAGE_BUTTON_CONTINUE, 'triangle-1-e', tep_href_link(FILENAME_DEFAULT)); ?>
        </div>
    </div>

    <?php
} else {
    tep_db_query("update " . TABLE_PRODUCTS_DESCRIPTION . " set products_viewed = products_viewed+1 where products_id = '" . (int) $HTTP_GET_VARS['products_id'] . "' and language_id = '" . (int) $languages_id . "'");

    if ($new_price = tep_get_products_special_price($product_info['products_id'])) {
        $products_price = '<del>' . $currencies->display_price($product_info['products_price'], tep_get_tax_rate($product_info['products_tax_class_id'])) . '</del> <span class="productSpecialPrice">' . $currencies->display_price($new_price, tep_get_tax_rate($product_info['products_tax_class_id'])) . '</span>';
    } else {
        $products_price = $currencies->display_price($product_info['products_price'], tep_get_tax_rate($product_info['products_tax_class_id']));
    }

    if (tep_not_null($product_info['products_model'])) {
        $products_name = $product_info['products_name'] . '<br />' . star_reviews($product_info['products_id']) .
                '<span class="smallText">SKU: ' . $product_info['products_model'] . '</span>';
    } else {
        $products_name = $product_info['products_name'] . '<br />' . star_reviews($product_info['products_id']);
    }
    ?>

    <?php echo tep_draw_form('cart_quantity', tep_href_link(FILENAME_PRODUCT_INFO, tep_get_all_get_params(array('action')) . 'action=add_product')); ?>

    <div class="contentContainer">
        <div class="pd_img_wrap">
        <?php
        if (tep_not_null($product_info['products_image'])) {
            $pi_query = tep_db_query("select image from " . TABLE_PRODUCTS_IMAGES . " where products_id = '" . (int) $product_info['products_id'] . "' order by sort_order");

            if (tep_db_num_rows($pi_query) > 0) {
                ?>
                <div style="float: left; position: relative;">
                    <div id="piGalNew" class="svwp">
                        <ul id="player_images">

                            <?php
                            $pi_counter = 0;
                            while ($pi = tep_db_fetch_array($pi_query)) {
                                $pi_counter++;
                                $pi_entry = '<li onclick="viewImage('.$pi_counter.')">' . tep_image(DIR_WS_IMAGES . $pi['image'], $product_info['products_name'], '350', '322');
                                $pi_entry .= '</li>';
                                echo $pi_entry;
                            }
                            ?>
                        </ul>                    
                    </div>
                    <?php
                    if (tep_not_null($product_info['products_code_video'])) {
						$pattern_w = "/width=\"+[\d]+\"/i";
                        $pattern_h = "/height=\"+[\d]+\"/i";
                        $product_info['products_code_video'] = preg_replace($pattern_w, 'width="340" ', $product_info['products_code_video']);
                        $product_info['products_code_video'] = preg_replace($pattern_h, 'height="203" ', $product_info['products_code_video']);
                        
                        ?>
                    <script type="text/javascript">
                        
                        $(document).ready(function() {
                        $('.slideViewer').after('<div class="bar_footer"><ul id="bar_links_footer"><a href="javascript:void();" onclick="play_images()"  style="width: 184px"><img src="http://motorfiend.com/images/layout/view_product_images.png" style="width: 184px; height: 31px"/></a><a href="javascript:void();" onclick="play_video()" class="border_left_footer_bar"><img src="http://motorfiend.com/images/layout/view_product_video.png" style="width: 184px; height: 31px;margin-left: -1px;"/></a></ul></div><div id="player_video" class="dont_show"></div><div id="host_player_video" class="dont_show"><?php echo $product_info['products_code_video']; ?></div>');
                        
                        }); 
                     </script>
                        
                        <?php
                    } 
                    ?>

                </div>
                <script type="text/javascript">
                    
                    $('#piGal ul').bxGallery({
                        maxwidth: 11250,
                        maxheight: 250,
                        thumbcrop: false,
                        thumbwidth: 45,
                        thumbcontainer: 260,
                        load_image: 'ext/jquery/bxGallery/spinner.gif'
                    });
                </script>

                <?php
            } else {
                ?>
                <div style="float: left; margin-top: 9px; position: relative;">
                    <div id="piGalNew" class="svwp">
                        <ul id="player_images">
                            <?php
                             $pi_entry = '<li onclick="viewImage('.$pi_counter.')">' . tep_image(DIR_WS_IMAGES . $pi['image'], $product_info['products_name'], '350', '322');
                             $pi_entry .= '</li>';
                             echo $pi_entry;
                            ?>
                        </ul>                        
                    </div>
                    <?php
                    if (tep_not_null($product_info['products_code_video'])) {
						$pattern_w = "/width=\"+[\d]+\"/i";
                        $pattern_h = "/height=\"+[\d]+\"/i";
                        $product_info['products_code_video'] = preg_replace($pattern_w, 'width="340" ', $product_info['products_code_video']);
                        $product_info['products_code_video'] = preg_replace($pattern_h, 'height="203" ', $product_info['products_code_video']);
                        
                        ?>
                       
                        <div class="bar_footer">
                            <ul id="bar_links_footer">
                                <a href="javascript:void();" onclick="play_images()"  style="width: 160px"><img src="http://motorfiend.com/view_product_images.png" style="width: 160px; height: 15px; margin-top: 6px;"/></a>
                                <a href="javascript:void();" onclick="play_video()" class="border_left_footer_bar"><img src="http://motorfiend.com/view_product_video.png" style="width: 160px; height: 15px; margin-top: 6px;"/></a>
                            </ul>
                        </div>
                        <div id="player_video" class="dont_show"></div>
                        <div id="host_player_video" class="dont_show"><?php echo $product_info['products_code_video']; ?></div>
                        <?php
                    } 
                    ?>
                </div>
                
                <?php
            }
            ?>
    </div>
            <script type="text/javascript">
                function viewImage(pi_counter){
                            TINY.box.show({
                                iframe:'<?php echo HTTP_SERVER . DIR_WS_HTTP_CATALOG .'/product_images_popup.php?products_id='.$HTTP_GET_VARS['products_id'].'&img=';?>'+pi_counter,
                                boxid:'frameless',
                                width:720,
                                height:665,
                                fixed:false,
                                maskid:'black',
                                maskopacity:20,
                                closejs:function(){closeJS()}})
                        }
                $("#piGal a[rel^='fancybox']").fancybox({
                    cyclic: true
                });
            </script>

            <?php
        }
        ?>
        <div class="contentText" id="product-info">


            <div class="product-info-name">
                <?php echo $products_name; ?>
            </div>
            <div id="product-info-social">
                <span class="fb-like"><iframe src="//www.facebook.com/plugins/like.php?href=<?php echo tep_href_link(FILENAME_PRODUCT_INFO, "products_id=" . $product_info['products_id'], 'NONSSL'); ?>&amp;send=false&amp;layout=button_count&amp;width=90&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21&amp;appId=272870882743663" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:90px; height:21px;" allowTransparency="true"></iframe></span>               
                <span class="pi_twitter"><a href="https://twitter.com/share" class="twitter-share-button">Tweet</a>
                <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
                <script type="text/javascript">stLight.options({publisher: "b230ac4a-ee67-4330-88b7-8c480f477b42"}); </script>
                </span>
                <span class="pi_pinit">                   
                    <a href="http://pinterest.com/pin/create/button/?url=<?php echo tep_href_link(FILENAME_PRODUCT_INFO, "products_id=" . $product_info['products_id'], 'NONSSL'); ?>" class="pin-it-button" count-layout="horizontal" always-show-count="1"><img border="0" src="//assets.pinterest.com/images/PinExt.png" title="Pin It" /></a>
                    <script type="text/javascript" src="http://assets.pinterest.com/js/pinit.js"></script>
                </span>
                <span class="st_plusone_hcount" displayText="Google +1"></span>
            </div>
            
            <?php
            $products_attributes_query = tep_db_query("select count(*) as total from " . TABLE_PRODUCTS_OPTIONS . " popt, " . TABLE_PRODUCTS_ATTRIBUTES . " patrib where patrib.products_id='" . (int) $HTTP_GET_VARS['products_id'] . "' and patrib.options_id = popt.products_options_id and popt.language_id = '" . (int) $languages_id . "'");
            $products_attributes = tep_db_fetch_array($products_attributes_query);
            if ($products_attributes['total'] > 0) {
                ?>

                <div id="product-info-options">
                    <table border="0" cellspacing="0" cellpadding="0">
                        <?php
                        //clr 030714 update query to pull option_type
                        $products_options_name_query = tep_db_query("select distinct popt.products_options_id, popt.products_options_name, popt.products_options_type, popt.products_options_length, popt.products_options_comment from " . TABLE_PRODUCTS_OPTIONS . " popt, " . TABLE_PRODUCTS_ATTRIBUTES . " patrib where patrib.products_id='" . (int) $HTTP_GET_VARS['products_id'] . "' and patrib.options_id = popt.products_options_id and popt.language_id = '" . (int) $languages_id . "' order by popt.products_options_sort_order, popt.products_options_name");
                        while ($products_options_name = tep_db_fetch_array($products_options_name_query)) {
                            //clr 030714 add case statement to check option type
                            switch ($products_options_name['products_options_type']) {
                                case PRODUCTS_OPTIONS_TYPE_TEXT:
                                    //CLR 030714 Add logic for text option
                                    $products_attribs_query = tep_db_query("select distinct patrib.options_values_price, patrib.price_prefix from " . TABLE_PRODUCTS_ATTRIBUTES . " patrib where patrib.products_id='" . (int) $HTTP_GET_VARS['products_id'] . "' and patrib.options_id = '" . $products_options_name['products_options_id'] . "'");
                                    $products_attribs_array = tep_db_fetch_array($products_attribs_query);
                                    $tmp_html = '<input type="text" name ="id[' . TEXT_PREFIX . $products_options_name['products_options_id'] . ']" size="' . $products_options_name['products_options_length'] . '" maxlength="' . $products_options_name['products_options_length'] . '" value="' . $cart->contents[$HTTP_GET_VARS['products_id']]['attributes_values'][$products_options_name['products_options_id']] . '">  ' . $products_options_name['products_options_comment'];
                                    if ($products_attribs_array['options_values_price'] != '0') {
                                        $tmp_html .= '(' . $products_attribs_array['price_prefix'] . $currencies->display_price($products_attribs_array['options_values_price'], tep_get_tax_rate($product_info['products_tax_class_id'])) . ')';
                                    }
                                    ?>
                                    <tr>
                                        <td class="main"><?php echo $products_options_name['products_options_name'] . ': '; ?><?php echo $tmp_html; ?></td>
                                    </tr>
                                    <?php
                                    break;

                                case PRODUCTS_OPTIONS_TYPE_TEXTAREA:
                                    //CLR 030714 Add logic for text option
                                    $products_attribs_query = tep_db_query("select distinct patrib.options_values_price, patrib.price_prefix from " . TABLE_PRODUCTS_ATTRIBUTES . " patrib where patrib.products_id='" . (int) $HTTP_GET_VARS['products_id'] . "' and patrib.options_id = '" . $products_options_name['products_options_id'] . "'");
                                    $products_attribs_array = tep_db_fetch_array($products_attribs_query);
                                    $tmp_html = '<textarea onKeyDown="textCounter(this,\'progressbar' . $products_options_name['products_options_id'] . '\',' . $products_options_name['products_options_length'] . ')"
                                   onKeyUp="textCounter(this,\'progressbar' . $products_options_name['products_options_id'] . '\',' . $products_options_name['products_options_length'] . ')"
                                   onFocus="textCounter(this,\'progressbar' . $products_options_name['products_options_id'] . '\',' . $products_options_name['products_options_length'] . ')"
                                   wrap="soft"
                                   name="id[' . TEXT_PREFIX . $products_options_name['products_options_id'] . ']"
                                   rows=5
                                   id="id[' . TEXT_PREFIX . $products_options_name['products_options_id'] . ']"
                                   value="' . $cart->contents[$HTTP_GET_VARS['products_id']]['attributes_values'][$products_options_name['products_options_id']] . '"></textarea>
                        <div id="progressbar' . $products_options_name['products_options_id'] . '" class="progress"></div>
                        <script>textCounter(document.getElementById("id[' . TEXT_PREFIX . $products_options_name['products_options_id'] . ']"),"progressbar' . $products_options_name['products_options_id'] . '",' . $products_options_name['products_options_length'] . ')</script>';
                                    ?>    <!-- DDB - 041031 - Form Field Progress Bar //-->
                                    <tr>
                                        <?php
                                        if ($products_attribs_array['options_values_price'] != '0') {
                                            ?>
                                            <td class="main"><?php echo $products_options_name['products_options_name'] . '<br>(' . $products_options_name['products_options_comment'] . ' ' . $products_attribs_array['price_prefix'] . $currencies->display_price($products_attribs_array['options_values_price'], tep_get_tax_rate($product_info['products_tax_class_id'])) . ')'; ?></td>
                                        <?php } else { ?>
                                            <td class="main"><?php echo $products_options_name['products_options_name'] . '<br>(' . $products_options_name['products_options_comment'] . ')'; ?></td>
                                        <?php }
                                        ?>
                                        <td class="main"><?php echo $tmp_html; ?></td>
                                    </tr>
                                    <?php
                                    break;

                                case PRODUCTS_OPTIONS_TYPE_RADIO:
                                    //CLR 030714 Add logic for radio buttons
                                    $tmp_html = '<table>';
                                    $products_options_query = tep_db_query("select pov.products_options_values_id, pov.products_options_values_name, pa.options_values_price, pa.price_prefix from " . TABLE_PRODUCTS_ATTRIBUTES . " pa, " . TABLE_PRODUCTS_OPTIONS_VALUES . " pov where pa.products_id = '" . (int) $HTTP_GET_VARS['products_id'] . "' and pa.options_id = '" . $products_options_name['products_options_id'] . "' and pa.options_values_id = pov.products_options_values_id and pov.language_id = '" . $languages_id . "'");
                                    $checked = true;
                                    while ($products_options_array = tep_db_fetch_array($products_options_query)) {
                                        $tmp_html .= '<tr><td class="main">';
                                        $tmp_html .= tep_draw_radio_field('id[' . $products_options_name['products_options_id'] . ']', $products_options_array['products_options_values_id'], $checked);
                                        $checked = false;
                                        $tmp_html .= $products_options_array['products_options_values_name'];
                                        $tmp_html .=$products_options_name['products_options_comment'];
                                        if ($products_options_array['options_values_price'] != '0') {
                                            $tmp_html .= '(' . $products_options_array['price_prefix'] . $currencies->display_price($products_options_array['options_values_price'], tep_get_tax_rate($product_info['products_tax_class_id'])) . ')&nbsp';
                                        }
                                        $tmp_html .= '</tr></td>';
                                    }
                                    $tmp_html .= '</table>';
                                    ?>
                                    <tr>
                                        <td class="main"><?php echo $products_options_name['products_options_name'] . ': '; ?><?php echo $tmp_html; ?></td>
                                    </tr>
                                    <?php
                                    break;
                                case PRODUCTS_OPTIONS_TYPE_CHECKBOX:
                                    //CLR 030714 Add logic for checkboxes
                                    $products_attribs_query = tep_db_query("select distinct patrib.options_values_id, patrib.options_values_price, patrib.price_prefix from " . TABLE_PRODUCTS_ATTRIBUTES . " patrib where patrib.products_id='" . (int) $HTTP_GET_VARS['products_id'] . "' and patrib.options_id = '" . $products_options_name['products_options_id'] . "'");
                                    $products_attribs_array = tep_db_fetch_array($products_attribs_query);
                                    echo '<tr><td class="main">' . $products_options_name['products_options_name'] . ': ';
                                    echo tep_draw_checkbox_field('id[' . $products_options_name['products_options_id'] . ']', $products_attribs_array['options_values_id']);
                                    echo $products_options_name['products_options_comment'];
                                    if ($products_attribs_array['options_values_price'] != '0') {
                                        echo '(' . $products_attribs_array['price_prefix'] . $currencies->display_price($products_attribs_array['options_values_price'], tep_get_tax_rate($product_info['products_tax_class_id'])) . ')&nbsp';
                                    }
                                    echo '</td></tr>';
                                    break;
                                default:
                                    //clr 030714 default is select list
                                    //clr 030714 reset selected_attribute variable
                                    $selected_attribute = false;
                                    $products_options_array = array();
                                    $products_options_query = tep_db_query("select pov.products_options_values_id, pov.products_options_values_name, pa.options_values_price, pa.price_prefix from " . TABLE_PRODUCTS_ATTRIBUTES . " pa, " . TABLE_PRODUCTS_OPTIONS_VALUES . " pov where pa.products_id = '" . (int) $HTTP_GET_VARS['products_id'] . "' and pa.options_id = '" . (int) $products_options_name['products_options_id'] . "' and pa.options_values_id = pov.products_options_values_id and pov.language_id = '" . (int) $languages_id . "'");
                                    while ($products_options = tep_db_fetch_array($products_options_query)) {
                                        $products_options_array[] = array('id' => $products_options['products_options_values_id'], 'text' => $products_options['products_options_values_name']);
                                        if ($products_options['options_values_price'] != '0') {
                                            $products_options_array[sizeof($products_options_array) - 1]['text'] .= ' (' . $products_options['price_prefix'] . $currencies->display_price($products_options['options_values_price'], tep_get_tax_rate($product_info['products_tax_class_id'])) . ') ';
                                        }
                                    }

                                    if (isset($cart->contents[$HTTP_GET_VARS['products_id']]['attributes'][$products_options_name['products_options_id']])) {
                                        $selected_attribute = $cart->contents[$HTTP_GET_VARS['products_id']]['attributes'][$products_options_name['products_options_id']];
                                    } else {
                                        $selected_attribute = false;
                                    }
                                    ?>
                                    <tr>
                                        <td class="main"><?php echo $products_options_name['products_options_name'] . ': '; ?><?php echo tep_draw_pull_down_menu('id[' . $products_options_name['products_options_id'] . ']', $products_options_array, $selected_attribute) . $products_options_name['products_options_comment']; ?></td>
                                    </tr>
                                <?php
                            }  //clr 030714 end switch
                        } //clr 030714 end while
                        ?>
                    </table>
                </div>
                <?php
            } //clr 030714 end if
            ?>
            <!-- quatily-->
            <div id="product-options-qty">
                <div style="float: left;">Quantity: </div>
                <div class="quantity_desc" onclick="
                    var sl = $('input:text[name=products_qty]').val();	
                
                        var sl_sub = parseInt(sl)-1;
                        if(sl_sub>=0){
                                $('input:text[name=products_qty]').val(sl-1);
                        }else{
                                $('input:text[name=products_qty]').val(0);
                        }
		
                
                event.returnValue=false; return false;"></div>
                <div style="float: left; margin: -3px 7px 0 0;" id="quantityI">
                    <input type="text" class="inputQ" size="4" name="products_qty" value="1" />
                </div>
                <div class="quantity_asc" onclick="
                    var sl = $('input:text[name=products_qty]').val();
                    var sl_add = parseInt(sl)+1;
                    $('input:text[name=products_qty]').val(sl_add);
                    event.returnValue=false; return false;"></div>
                <span class="qunits">Units</span>
            </div>
            <!--end quaitly-->
            <div class="content-box" id="product-info-price">
                Price: <span class="price"><?php echo $products_price; ?></span>
                <div id="product-info-status">
                    <?php
                    if ($product_info['products_status'] == 1)
                        echo '<span style="color: #595959; font-size:12px;">In Stock & Ready To Ship</span>';
                    else
                        echo '<span class="motored">Temporarily out of stock.</span>';
                    ?>
                    <div id="product-info-buy">
                        <?php echo tep_draw_hidden_field("products_id", $product_info['products_id']) . tep_draw_input_field("add-to-cart", 'true', 'id="Btn_add-to-cart"', 'submit'); ?>
                        <?php echo tep_image(DIR_WS_IMAGES . "layout/cc-types-horizontal.png", ''); ?>
                    </div>
                </div>
            </div>

            <?php
            if ($product_info['products_date_available'] > date('Y-m-d H:i:s')) {
                ?>

                <p style="text-align: center;"><?php echo sprintf(TEXT_DATE_AVAILABLE, tep_date_long($product_info['products_date_available'])); ?></p>

                <?php
            }
            ?>
            <?php
            $reviews_query = tep_db_query("select count(*) as count from " . TABLE_REVIEWS . " where products_id = '" . (int) $HTTP_GET_VARS['products_id'] . "' and reviews_status = 1");
            $reviews = tep_db_fetch_array($reviews_query);
            ?>
            <?php
            if (HEADER_TAGS_DISPLAY_SOCIAL_BOOKMARKS == 'true') {
                echo '<div style="margin-top:5px;">';
                include(DIR_WS_MODULES . 'header_tags_social_bookmarks.php');
                echo '</div>';
            }
            ?>

            <script type="text/javascript">
                $(document).ready(function(){
                    $("#product-info-tabs").tabs();
                });
            </script>
            <div id="product-info-tabs">
                <ul>
                    <li><a id="pi_des" href="#product-info-tab1">Description</a></li>
                    <li><a id="pi_reviews" href="#product-info-tab2">Customer Reviews</a></li>
                    <li><a id="pi_ask" href="#product-info-tab3">Why Order From Us?</a></li>
                    <li><a id="pi_whosale" href="#product-info-tab4">WHOLESALE</a></li>
                </ul>
                <div id="product-info-tab1">
                    <?php echo stripslashes($product_info['products_description']); ?>
                </div>
                <div id="product-info-tab2">
                    <div id="reviews-win">
                        <p>Submit a review before the end of the month and automatically enter
                            Upload an image or video to better your chance of winning! <a href="http://www.motorfiend.com/help-grow-i-33.html" target="_blank">See Details</a></p>
                    </div>
                    <div id="product-info-write-review">
                        Average rating: <?php echo star_reviews_large($product_info['products_id']); ?> <a href="<?php echo tep_href_link(FILENAME_PRODUCT_REVIEWS_WRITE, "products_id=" . $product_info['products_id'], 'NONSSL'); ?>" id="btn-reviews-write"></a>
                    </div>
                    <div id="product-info-tab-reviews">

                    </div>
                    <script type="text/javascript">
                        $.ajax({
                            url: 'ajaxCalls.php',
                            type: 'post',
                            data: {'call-type':'product-reviews','products_id':<?php echo $product_info['products_id']; ?>},
                            success: function(html){
                                $("#product-info-tab-reviews").html(html);
                            }
                        });
                    </script>
                </div>
                <?php //================== TAB 3 ============  ?>
                <div id="product-info-tab3">
                    <?php
                    $information_query = tep_db_query("SELECT information_title, information_description FROM " . TABLE_INFORMATION . " WHERE visible='1' AND information_id='35' AND language_id='" . (int) $languages_id . "'");
                    $information = tep_db_fetch_array($information_query);
                    $title = stripslashes($information['information_title']);
                    $page_description = stripslashes($information['information_description']);
                    ?>
                    <h2><?php echo $title; ?></h2>
                    <?php echo $page_description; ?>
                </div>
                <?php //================== TAB 4 ============  ?>
                <div id="product-info-tab4">
                    <?php
                    $information_query = tep_db_query("SELECT information_title, information_description FROM " . TABLE_INFORMATION . " WHERE visible='1' AND information_id='35' AND language_id='" . (int) $languages_id . "'");
                    $information = tep_db_fetch_array($information_query);
                    $title = stripslashes($information['information_title']);
                    $page_description = stripslashes($information['information_description']);
                    ?>
                    <h2><?php echo $title; ?></h2>
                    <?php echo $page_description; ?>
                </div>
            </div>
        </div>
        <div style="clear: both;"></div>
        <?php /*         * * Begin Header Tags SEO ** */ ?>
        <?php
        if (!tep_not_null($header_tags_array['title'])) {
            $header_tags_array['title'] = $product_info['products_name'];
        }
        echo '<div class="contentText" style="margin-top:5px; text-align:center;">' . TEXT_VIEWING . '&nbsp';
        echo '<a title="' . $header_tags_array['title'] . '" href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $product_info['products_id'], 'NONSSL') . '"/# ' . $header_tags_array['title'] . '">' . $header_tags_array['title'] . '</a></div>';
        /*         * * End Header Tags SEO ** */
        ?>

        <?php
        if ((USE_CACHE == 'true') && empty($SID)) {
            echo tep_cache_also_purchased(3600);
        } else {
            include(DIR_WS_MODULES . FILENAME_ALSO_PURCHASED_PRODUCTS);
        }
        include(DIR_WS_MODULES . FILENAME_RELATED_PRODUCTS);
        ?>
    </div>

    </form>

    <?php
}

require(DIR_WS_INCLUDES . 'template_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
