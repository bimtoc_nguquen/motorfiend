<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

require('includes/application_top.php');

require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_CONTACT_US);
require(DIR_WS_INCLUDES . 'template_top.php');
?>
<script type="text/javascript">
    $().ready(function(){
        $('#breadcrumb-home').hide();
    });
</script>
    <div style="width: 900px; margin: -3px auto; margin-bottom: 100px; overflow: hidden;">
        <div style="float:left;">
            <img style=" margin-top:28px; margin-left: 110px;" src="images/404_error.png" />
        </div>
        <div style="margin: 10px 0 0 439px; width: 415px">
            <div>
                <div style="font-size: 150px;color: #2f2f2f; height: 155px; font-weight: bold">404:</div>
                <div style="font-size: 21px;color: #2f2f2f; font-weight: bold">It looks like you took a wrong turn</div>
                <div style="font-size: 19px;color: #2f2f2f; font-weight: bold">What was here isn't here anymore.. (sorries)</div>
                <div style="font-size: 12px; color: #2f2f2f; margin-top: 10px;">To find your way back simply click on one of the links below tog get back on the right direction...</div>

            </div>
            <div style="margin-top: 20px;">
                <div><a style="color: #ddb116; font-size: 14px;" href="http://www.motorfiend.com/">Home page</a></div>
                <!--<div><a style="color: #ddb116; font-size: 14px;" href="http://www.motorfiend.com/interior-accessories-c-7573.html">Interior Accessories</a></div>-->
                <div><a style="color: #ddb116; font-size: 14px;" href="http://www.motorfiend.com/exterior-accessories-c-254.html">Exterior Accessories</a></div>
                <div><a style="color: #ddb116; font-size: 14px;" href="http://www.motorfiend.com/electronics-audio-c-256.html">Car Electronics & Audio</a></div>
            </div>
        </div>
    </div>


<?php
require(DIR_WS_INCLUDES . 'template_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>


