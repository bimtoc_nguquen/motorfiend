<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/
	
  require('includes/application_top.php');
define('FILENAME_CHECKOUT', 'checkout.php');
		
		if (tep_session_is_registered('customer_id')) {
		 tep_redirect(tep_href_link('checkout.php'));
		}
		
// redirect the customer to a friendly cookie-must-be-enabled page if cookies are disabled (or the session has not started)
  if ($session_started == false) {
    tep_redirect(tep_href_link(FILENAME_COOKIE_USAGE));
  }

  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_LOGIN);

  $error = false;
  if (isset($HTTP_GET_VARS['action']) && ($HTTP_GET_VARS['action'] == 'process') && isset($HTTP_POST_VARS['formid']) && ($HTTP_POST_VARS['formid'] == $sessiontoken)) {
    $email_address = tep_db_prepare_input($HTTP_POST_VARS['email_address']);
    $password = tep_db_prepare_input($HTTP_POST_VARS['password']);

// Check if email exists
    $check_customer_query = tep_db_query("select customers_id, customers_firstname, customers_password, customers_email_address, customers_default_address_id from " . TABLE_CUSTOMERS . " where customers_email_address = '" . tep_db_input($email_address) . "'");
    if (!tep_db_num_rows($check_customer_query)) {
      $error = true;
    } else {
      $check_customer = tep_db_fetch_array($check_customer_query);
// Check that password is good
      if (!tep_validate_password($password, $check_customer['customers_password'])) {
        $error = true;
      } else {
        if (SESSION_RECREATE == 'True') {
          tep_session_recreate();
        }

// migrate old hashed password to new phpass password
        if (tep_password_type($check_customer['customers_password']) != 'phpass') {
          tep_db_query("update " . TABLE_CUSTOMERS . " set customers_password = '" . tep_encrypt_password($password) . "' where customers_id = '" . (int)$check_customer['customers_id'] . "'");
        }

        $check_country_query = tep_db_query("select entry_country_id, entry_zone_id from " . TABLE_ADDRESS_BOOK . " where customers_id = '" . (int)$check_customer['customers_id'] . "' and address_book_id = '" . (int)$check_customer['customers_default_address_id'] . "'");
        $check_country = tep_db_fetch_array($check_country_query);

        $customer_id = $check_customer['customers_id'];
        $customer_default_address_id = $check_customer['customers_default_address_id'];
        $customer_first_name = $check_customer['customers_firstname'];
        $customer_country_id = $check_country['entry_country_id'];
        $customer_zone_id = $check_country['entry_zone_id'];
        tep_session_register('customer_id');
        tep_session_register('customer_first_name');
        if($customer_default_address_id>0){
								tep_session_register('customer_default_address_id');
        tep_session_register('customer_country_id');
        tep_session_register('customer_zone_id');
								}

        tep_db_query("update " . TABLE_CUSTOMERS_INFO . " set customers_info_date_of_last_logon = now(), customers_info_number_of_logons = customers_info_number_of_logons+1 where customers_info_id = '" . (int)$customer_id . "'");

// reset session token
        $sessiontoken = md5(tep_rand() . tep_rand() . tep_rand() . tep_rand());

// restore cart contents
        $cart->restore_contents();

        if (sizeof($navigation->snapshot) > 0) {
          $origin_href = tep_href_link($navigation->snapshot['page'], tep_array_to_string($navigation->snapshot['get'], array(tep_session_name())), $navigation->snapshot['mode']);
          $navigation->clear_snapshot();
          tep_redirect($origin_href);
        } else {
          tep_redirect(tep_href_link(FILENAME_CHECKOUT));
        }
      }
    }
  }

  if ($error == true) {
    $messageStack->add('login', TEXT_LOGIN_ERROR);
  }

$error = false;
if (isset($HTTP_GET_VARS['action']) && ($HTTP_GET_VARS['action'] == 'create') && isset($HTTP_POST_VARS['formid']) && ($HTTP_POST_VARS['formid'] == $sessiontoken)) {
//value form start
    if (ACCOUNT_GENDER == 'true') {
      if (isset($HTTP_POST_VARS['gender'])) {
        $gender = tep_db_prepare_input($HTTP_POST_VARS['gender']);
      } else {
        $gender = false;
      }
    }


        $firstname = tep_db_prepare_input($HTTP_POST_VARS['firstname']);
        $lastname = tep_db_prepare_input($HTTP_POST_VARS['lastname']);
        $email_address = tep_db_prepare_input($HTTP_POST_VARS['email_address']);
        $email_address_confirm = tep_db_prepare_input($HTTP_POST_VARS['email_address_confirm']);
        $password = tep_db_prepare_input($HTTP_POST_VARS['password']);
        //$confirmation = tep_db_prepare_input($HTTP_POST_VARS['confirmation']);
        //$telephone = tep_db_prepare_input($HTTP_POST_VARS['telephone']);
				
			if (ACCOUNT_GENDER == 'true') {
      if ( ($gender != 'm') && ($gender != 'f') ) {
        $error = true;

        $messageStack->add('create_account', ENTRY_GENDER_ERROR);
      }
    }
			if (strlen($firstname) < ENTRY_FIRST_NAME_MIN_LENGTH) {
      $error = true;

      $messageStack->add('create_account', ENTRY_FIRST_NAME_ERROR);
    }

   if (strlen($lastname) < ENTRY_LAST_NAME_MIN_LENGTH) {
        $error = true;

        $messageStack->add('create_account', ENTRY_LAST_NAME_ERROR);
    }
    /*if (strlen($telephone) < ENTRY_TELEPHONE_MIN_LENGTH) {
      $error = true;

      $messageStack->add('create_account', ENTRY_TELEPHONE_NUMBER_ERROR);
    }*/
   if (strlen($email_address) < ENTRY_EMAIL_ADDRESS_MIN_LENGTH) {
      $error = true;

      $messageStack->add('create_account', ENTRY_EMAIL_ADDRESS_ERROR);
    }elseif ($email_address != $email_address_confirm) {
       $error = true;

       $messageStack->add('create_account', 'xxxxx');
   } elseif (tep_validate_email($email_address) == false) {
      $error = true;

      $messageStack->add('create_account', ENTRY_EMAIL_ADDRESS_CHECK_ERROR);
    } else {
      $check_email_query = tep_db_query("select count(*) as total from " . TABLE_CUSTOMERS . " where customers_email_address = '" . tep_db_input($email_address) . "'");
      $check_email = tep_db_fetch_array($check_email_query);
      if ($check_email['total'] > 0) {
        $error = true;

        $messageStack->add('create_account', ENTRY_EMAIL_ADDRESS_ERROR_EXISTS);
      }
    }			
			
			 if (strlen($password) < ENTRY_PASSWORD_MIN_LENGTH) {
      $error = true;

      $messageStack->add('create_account', ENTRY_PASSWORD_ERROR);
    } //elseif ($password != $confirmation) {
      //$error = true;

      //$messageStack->add('create_account', ENTRY_PASSWORD_ERROR_NOT_MATCHING);
    //}
//value form start
    if ($error == false) {
        $sql_data_array = array('customers_firstname' => $firstname,
            'customers_lastname' => $lastname,
            'customers_email_address' => $email_address,
            'customers_password' => tep_encrypt_password($password));
        if (ACCOUNT_GENDER == 'true')
            $sql_data_array['customers_gender'] = $gender;
        tep_db_perform(TABLE_CUSTOMERS, $sql_data_array);
        $customer_id = tep_db_insert_id();
        tep_db_query("insert into " . TABLE_CUSTOMERS_INFO . " (customers_info_id, customers_info_number_of_logons, customers_info_date_account_created) values ('" . (int) $customer_id . "', '0', now())");
        if (SESSION_RECREATE == 'True') {
            tep_session_recreate();
        }
        $customer_first_name = $firstname;
        tep_session_register('customer_id');
        tep_session_register('customer_first_name');
    }
    tep_redirect(tep_href_link('checkout.php'));
}
$breadcrumb->add('Checkout', tep_href_link('direct_checkout.php', '', 'SSL'));

?>






<!--?????????????????????????????????-->


<?php


$oscTemplate->buildBlocks();
$fullpages = array("index.php", "product_info.php", "checkout.php");

if (!$oscTemplate->hasBlocks('boxes_column_left')) {
    $oscTemplate->setGridContentWidth($oscTemplate->getGridContentWidth() + $oscTemplate->getGridColumnWidth());
}

if (!$oscTemplate->hasBlocks('boxes_column_right')) {
    $oscTemplate->setGridContentWidth($oscTemplate->getGridContentWidth() + $oscTemplate->getGridColumnWidth());
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php echo HTML_PARAMS; ?>>
    <head profile="http://www.w3.org/2005/10/profile">
        <?php
        /*         * * Begin Header Tags SEO ** */
        if (file_exists(DIR_WS_INCLUDES . 'header_tags.php')) {
            require(DIR_WS_INCLUDES . 'header_tags.php');
        } else {
            ?>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                <title><?php echo TITLE; ?></title>
                <?php
            }
            /*             * * End Header Tags SEO ** */
            ?>
            <base href="<?php echo (($request_type == 'SSL') ? HTTPS_SERVER : HTTP_SERVER) . DIR_WS_CATALOG; ?>" />
            <link rel="stylesheet" type="text/css" href="ext/jquery/ui/overcast/jquery-ui-1.8.6.css" />
            <script type="text/javascript" src="ext/jquery/jquery-1.7.1.min.js"></script>
            <script type="text/javascript" src="ext/jquery/ui/jquery-ui-1.8.6.min.js"></script>

            <?php
            if (tep_not_null(JQUERY_DATEPICKER_I18N_CODE)) {
                ?>
                <script type="text/javascript" src="ext/jquery/ui/i18n/jquery.ui.datepicker-<?php echo JQUERY_DATEPICKER_I18N_CODE; ?>.js"></script>
                <script type="text/javascript">
                    $.datepicker.setDefaults($.datepicker.regional['<?php echo JQUERY_DATEPICKER_I18N_CODE; ?>']);
                </script>
                <?php
            }
            ?>

            <script type="text/javascript" src="ext/jquery/bxGallery/jquery.bxGallery.1.1.min.js"></script>
            <link rel="stylesheet" type="text/css" href="ext/jquery/fancybox/jquery.fancybox-1.3.4.css" />
            <script type="text/javascript" src="ext/jquery/jquery.selectbox/js/jquery.selectbox-0.1.3.min.js"></script>
            <script type="text/javascript" src="ext/slider_thumb/jquery.slideViewerPro.1.5.js"></script>
            <script type="text/javascript" src="ext/slider_thumb/jquery.timers.js"></script>
            <script type="text/javascript" src="ext/slider_thumb/jquery.lightbox-0.5.js"></script>
            <link rel="stylesheet" type="text/css" href="ext/jquery/jquery.selectbox/css/jquery.selectbox.css" />
            <link rel="stylesheet" type="text/css" href="ext/slider_thumb/svwp_style.css" />
            <link rel="stylesheet" type="text/css" href="ext/slider_thumb/jquery.lightbox-0.5.css" />
            <link rel="stylesheet" type="text/css" href="stylesheet.css" />
            <!--phone tooltip
            <link rel="stylesheet" type="text/css" href="ext/jquery/tooltip/tipsy.css"></link> 
            <script type="text/javascript" src="ext/jquery/tooltip/jquery.tipsy.js"></script>           
           end phone tooltip-->
            <script type="text/javascript">
                $(document).ready(function(){
                    $('#gender').hide();
                    //$('#tooltip_phone').tipsy({gravity: 'se'});
                    $("#sign_up").mouseover(function(){
                        $(this).css("opacity",0.5);

                        //$(this).css{"opacity":0.5};
                    });
                    $("#sign_up").mouseout(function(){
                        $(this).css("opacity",1);

                        //$(this).css{"opacity":0.5};
                    });

                    $("#login").mouseover(function(){
                        $(this).css("opacity",0.5);

                        //$(this).css{"opacity":0.5};
                    });
                    $("#login").mouseout(function(){
                        $(this).css("opacity",1);

                        //$(this).css{"opacity":0.5};
                    });
                })
            </script>
            <script type="text/javascript">

                $(function() {
                    $('#player_images a').lightBox();
                });


                $(window).bind("load", function() {
                    $("div#piGalNew").slideViewerPro({
                        thumbs: 4,
                        thumbsPercentReduction: 20,
                        galBorderWidth: 1,
                        galBorderColor: "#e3e3e3",
                        thumbsTopMargin: 10,
                        thumbsRightMargin: 10,
                        thumbsBorderWidth: 1,
                        thumbsActiveBorderColor: "red",
                        thumbsActiveBorderOpacity: 0.8,
                        thumbsBorderOpacity: 1,
                        leftButtonInner: "<img src='http://motorfiend.com/ext/slider_thumb/images/larw.gif' />",
                        rightButtonInner: "<img src='http://motorfiend.com/ext/slider_thumb/images/rarw.gif' />",
                        autoslide: false,
                        typo: false
                    });
                });

                function play_video(){
                    $("div#div_icon_vdeo").hide();
                    $("ul#player_images").hide();
                    var content_video = $("div#host_player_video").html();
                    $("div#player_video").html(content_video);
                    $("div#player_video").show();
                }

                function play_images(){
                    $("div#div_icon_vdeo").show();
                    $("ul#player_images").show();
                    $("div#player_video").hide();
                    $("div#player_video").html('');
                }
                setInterval(checkScreen, 1);
                checkScreen();





            </script>
            <!--[if IE]>
             <link rel="stylesheet" type="text/css" href="stylesheet-ie.css" />
            <![endif]-->
            <?php echo $oscTemplate->getBlocks('header_tags'); ?>
            <script type="text/javascript" src="ext/jquery/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
            <script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>
            <link rel="icon" type="image/png" href="favicon.png" />

        <style type="text/css">

            #img img{
                float:left;

            }
            #topMenu_c{
                margin-left: 62px;
                margin-top: 7px;
            }
            #bodyContent{
                float: left;
            }
            .contentContainer1{
                padding-top: 0px;
                margin-top: 0px;
            }

            .fieldKey{
                text-align: right;
            }
            .input input{
                height: 20px;
                width: 195px;
            }
            .text-input{
                font-size: 14px;
                font-weight: bold;
                color: #000000;
            }
            .td-input{
                padding: -1px 0 0 5px; text-align: right; width: 160px;
            }

            body{
                font-size: 0px;
                line-height: 0px;
            }

            #bodyContent{
                width: 863px;

            }
            #bodyWrapper{
                margin: 0 auto;

            }




        </style>

        <!--[if IE]>
        <style type="text/css">
            #bodyContent{
                width: 890px;

            }
            #topMenu_c{
                margin-left: 60px;
                margin-top: -5px;
            }
            #bodyWrapper{
                margin-left: 165px;
            }
            .contentContainer2{
                height: 290px;
            }

        </style>
        <![endif]-->

    </head>
    <body style="">
        <div id="fullwrap" style="height: 100%" >
            <div id="mainWrapper" >

                <div id="bodyWrapper" class="container_<?php echo $oscTemplate->getGridContainerWidth(); ?>" style="width: 986px;">
					<!--cuongnm edit 23/5/2012 begin-->
	                <div class="checkoutstep1" style="height: 120px;">
                        <div id="Logo" style="margin-left: 379px; height: 35px; margin-top: 9px;"><img src="images/top_menu.JPG" /></div>

                        <div id="topMenu_c" >
                            <img src="images/menu.jpg" alt=""/>
                        </div>
                    </div>                    
                    <div id="bodyContent" style="margin-left: 55px; margin-top: 17px;" class="grid_<?php echo $oscTemplate->getGridContentWidth();  ?>

                         <?php
// if (!in_array(basename($_SERVER['SCRIPT_NAME']),$fullpages) || (isset($category_depth) && $category_depth != 'top'))
                         if ((isset($category_depth) && $category_depth != 'top')) {
                             echo ($oscTemplate->hasBlocks('boxes_column_left') ? 'push_' . $oscTemplate->getGridColumnWidth() : '');
                         }
                         ?>
                         ">


<!--?????????????????????????????????-->



<?php require('includes/form_check.js.php'); ?>
<div class="newcheckout1">
<div class="contentContainer1" style="float: left; border:1px solid #dddddd; width: 421px">
  <div class="" style="margin-left: 80px;  margin-top: 25px;"><span style="font-size: 20px;color: #000000; font-family: arial; ">Sign-In To Your Account Now</span></div>
  <div style="width: 235px; margin-left: 95px; margin-top: 17px; margin-bottom: -4px;"><img src="images/line_login.JPG"/></div>
  <div class="contentText1">

    <?php echo tep_draw_form('login', tep_href_link('direct_checkout.php', 'action=process', 'SSL'), 'post', 'onsubmit="return check_form(login);"', true); ?>
<?php
  if ($messageStack->size('login') > 0) {
    echo $messageStack->output('login');
  }
?>  <div style="margin-left: 15px; margin-bottom:12px;">
    <table border="0" cellspacing="0" cellpadding="2" width="100%">
      <tr style="">
        <td class="" style="padding: 10px 0 0 5px; text-align: right; width: 160px;"><span style="font-size: 14px; font-weight: bold; color: #000000;">My e-mail address is:</span></td>
        <td class="input" style="padding: 10px 0 4px 5px; "><?php echo tep_draw_input_field('email_address'); ?></td>
      </tr>

      <tr>
        <td class="" style="padding: 0 0 0 5px; text-align: right; width: 160px;" ><span style="font-size: 14px; font-weight: bold; color: #000000;">Password:</span> </td>
        <td class="input" style="padding: 0 0 0 5px; "><?php echo tep_draw_password_field('password'); ?></td>
      </tr>
    </table>


  </div>

      <div>

          <?php echo '<a style="color:#0249a0;font-size:12px; margin-left:184px; "href="' . tep_href_link(FILENAME_PASSWORD_FORGOTTEN, '', 'SSL') . '">' . "Forgot your password?" . '</a>'; ?></div>
      <p style="margin-left: 103px; margin-top: 17px">
      <input id="login" type="image" src="images/login.jpg"/>
      <!-- <?php echo tep_draw_button(IMAGE_BUTTON_LOGIN, 'key', null, 'primary'); ?>--></p>

    </form>
<?php
          // *** BEGIN GOOGLE CHECKOUT ***
          if (defined('MODULE_PAYMENT_GOOGLECHECKOUT_STATUS') && MODULE_PAYMENT_GOOGLECHECKOUT_STATUS == 'True') {
            include_once('googlecheckout/gcheckout.php');
          }
          // *** END GOOGLE CHECKOUT ***
?>
  </div>
</div>

<div class="contentContainer2" style="width: 420px; float: right; border:1px solid #dddddd;">
  <div  class="" style="font-size: 20px; font-family: arial; color: #000000; margin-left: 23px; margin-top: 25px;">New To MotorFiend.com? Register Below.</div>
    <div style="width: 313px; margin-left: 46px; margin-top: 17px; margin-bottom: -4px; "><img src="images/line_signup.JPG"/></div>
  <div class="contentText1">

<?php echo tep_draw_form('create_account', tep_href_link('direct_checkout.php', 'action=create', 'SSL'), 'post', 'onsubmit="return check_form(create_account);"', true); ?>
<?php
  if ($messageStack->size('create_account') > 0) {
    echo $messageStack->output('create_account');
  }
?>
      <div style="margin-left: 17px;">
<table border="0" cellspacing="0" cellpadding="2" width="100%">
    <?php
    if (ACCOUNT_GENDER == 'true') {
        if (isset($gender)) {
            $male = ($gender == 'm') ? true : false;
            $female = ($gender == 'f') ? true : false;
        } else {
            $male = false;
            $female = false;
        }
        ?>

        <tr>
            <td id="gender"><?php echo tep_draw_radio_field('gender', 'm', true , $male) . '&nbsp;&nbsp;' . MALE . '&nbsp;&nbsp;' . tep_draw_radio_field('gender', 'f', $female) . '&nbsp;&nbsp;' . FEMALE . '&nbsp;' . (tep_not_null(ENTRY_GENDER_TEXT) ? '<span class="inputRequirement">' . ENTRY_GENDER_TEXT . '</span>': ''); ?></td>
        </tr>

        <?php
    }
    ?>
    <tr>
                                                <td class="td-input" style="padding: 10px 0 0 5px;" ><span class="text-input"> First name:</span></td>
                                                <td class="input" style="padding: 10px 0 4px 5px; "><?php echo tep_draw_input_field('firstname') . '&nbsp;' . (tep_not_null(ENTRY_FIRST_NAME_TEXT) ? '<span class="inputRequirement">' . '</span>' : ''); ?></td>
                                            </tr>

                                            <tr>
                                                <td class="td-input"><span class="text-input"> Last name:</span></td>
                                                <td class="input" style="padding: 0 0 4px 5px; "><?php echo tep_draw_input_field('lastname') . '&nbsp;' . (tep_not_null(ENTRY_LAST_NAME_TEXT) ? '<span class="inputRequirement">' . '</span>' : ''); ?></td>
                                            </tr>
    <tr>
      <td class="td-input"><span class="text-input"> My e-mail address is:</span>
      <td class="input" style="padding: 0 0 4px 5px;"><?php echo tep_draw_input_field('email_address') . '&nbsp;' . (tep_not_null(ENTRY_EMAIL_ADDRESS_TEXT) ? '<span class="inputRequirement">' .  '</span>': ''); ?></td>
    </tr>
    <tr>
        <td class="td-input"><span class="text-input"> Type it again:</span></td>
        <td class="input" style="padding: 0 0 4px 5px;"><?php echo tep_draw_input_field('email_address_confirm') . '&nbsp;' . (tep_not_null(ENTRY_EMAIL_ADDRESS_TEXT) ? '<span class="inputRequirement">' .  '</span>': ''); ?></td>
      </tr>
      <tr>
        <td class="td-input"><span class="text-input"> Enter a new password:</span></td>
        <td class="input" style="padding: 0 0 4px 5px;"><?php echo tep_draw_password_field('password') . '&nbsp;' . (tep_not_null(ENTRY_PASSWORD_TEXT) ? '<span class="inputRequirement">' .  '</span>': ''); ?></td>
      </tr>
      <!--<tr>
        <td class="td-input"><span class="text-input"> Type it again:</span></td>
        <td class="input" style="padding: 0 0 4px 5px;"><?php echo tep_draw_password_field('confirmation') . '&nbsp;' . (tep_not_null(ENTRY_PASSWORD_CONFIRMATION_TEXT) ? '<span class="inputRequirement">' .  '</span>': ''); ?></td>
      </tr>-->
        <!--<tr>
            <td class="td-input">
                <div style="position: relative;">
                    <span class="text-input" style="position: absolute; top:-12px; left:30px;"> Contact Number: </span>
                
                <img style="position: absolute;left:150px;top:-9px;" id="tooltip_phone" title='We ask for your number because we like to provide tracking by giving you a call. You can choose to leave this blank if you like' src="images/tool-tip-icon.png"/>
                </div>
            </td>
            <td class="input" style="padding: 0 0 4px 5px;"><?php echo tep_draw_input_field('telephone') . '&nbsp;' . (tep_not_null(ENTRY_TELEPHONE_NUMBER_TEXT) ? '<span class="inputRequirement">' .  '</span>': ''); ?></td>
        </tr>-->
    </table>
      </div>
    <p style="margin-left: 120px;margin-bottom: 28px;" ><input  id="sign_up" type="image" src="images/sign_up.jpg"/><?php //echo tep_draw_button('Sign Up', 'person'); ?></p>
</form>
</div>
</div>


</div>

<style>
    body{background: #ffffff;}
    #mainWrapper {
        background: #ffffff;;
    }
</style>



                    </div> <!-- bodyContent //-->

                    <div class="clear"></div>
                </div> <!-- bodyWrapper //-->
            </div> <!-- mainWrapper //-->
        </div> <!-- fullwrap //-->
        
        <div style="border: 1px solid #f5f5f5; width: 100%; margin-top: 35px;"></div>
        <!--<div style="margin-top: 71px;margin-bottom: 370px;"><center><span style="font-size: 12px; font-family: arial; text-align: center"><p>Copyright &copy; 2010 - 2012 Dealsbase. All Rights Reserved</p></span></center></div>--> 
        <div style="width: 980px;margin: 0 auto;text-align: center;">
        <!--<div style="position: absolute; top: 500px; left: 500px;"><span style="font-size: 12px; font-family: arial; "><p>Copyright &copy; 2010 - 2012 Motorfiend. All Rights Reserved</p></span></div>-->
        <span style="font-size: 12px; font-family: arial; "><p style="margin-top: 25px;">Copyright &copy; 2010 - 2012 Motorfiend. All Rights Reserved</p></span>
        </div>
    </body>
</html>