<?php

/**
 * Short description of file
 *
 * Long description of file (if any)...
 *
 * @author your name
 * @copyright  2012 CYASoft
 * @license    CYASoft Licence 1.0
 * @version    1.0
 * @link       http://cyasoft.com 
 */

require('includes/application_top.php');
$order_mail=cya_get_email('CUSTOMER_ORDER');
chdir('../..');

@ini_set('zlib.output_compression', 'Off');

define('CHECKOUT_ONE_PAGE', true);

include('includes/checkout/checkout.config.php');

/* These are payment modules that require the user to go to the processors' websites
 * to complete the payment, so they should be done at the very end
 */
$suspended_payment = array(
    'paypal', 'paypal_ipn', 'paypal_standard', 'paypal_ec', 'worldpay',
    'cybersource', 'protx_direct', 'gochargeit', 'securehosting', 'esec',
    'protx_form', 'alertpay', 'gateway_services_cc', 'gateway_services_ck',
    'eWayPayment', 'paypal_express', 'paypal_uk_express', 'sagepay_form',
    'bankpass', 'epaywindow', 'pmgate2shop', 'wiegant_ideal'
);

/* These are the PayPal IPN modules where we want the order to be saved as normal, 
 * but we don't want the order receipt to be sent until IPN confirmation of payment
 * has been received.
 */
$paypal_modules = array('paypal', 'paypal_ipn', 'paypal_standard', 'epaywindow');

define('FILENAME_CHECKOUT', 'checkout.php');
define('FILENAME_CHECKOUT_CONFIRMATION', CHECKOUT_WS_INCLUDES . 'checkout_process.php');
define('FILENAME_CHECKOUT_PAYMENT', CHECKOUT_WS_INCLUDES . 'checkout_process.php');
define('FILENAME_CHECKOUT_PAYMENT_ADDRESS', CHECKOUT_WS_INCLUDES . 'checkout_process.php');
define('FILENAME_CHECKOUT_PROCESS', CHECKOUT_WS_INCLUDES . 'checkout_process.php');
define('FILENAME_CHECKOUT_SHIPPING', CHECKOUT_WS_INCLUDES . 'checkout_process.php');
define('FILENAME_CHECKOUT_SHIPPING_ADDRESS', CHECKOUT_WS_INCLUDES . 'checkout_process.php');
define('NO_REDIRECT', true);

if (!ini_get('safe_mode')) {
    @set_time_limit(0);
    @ini_set("max_execution_time", 300);
    @ini_set("default_socket_timeout", 300);
    @ini_set("magic_quotes_gpc", "0");
    @ini_set("output_buffering", "0");
    //@set_magic_quotes_runtime(1);
}

require_once('includes/application_top.php');

//Protx Form Support
if ($_SESSION['checkout_payment'] == 'protx_form' && !empty($_GET['crypt'])) {
    $redirect = tep_href_link(CHECKOUT_WS_INCLUDES . 'checkout_notification.php', 'module=protx_form&crypt=' . $_GET['crypt'], 'SSL');

    //Avoid Ultimate SEO URL problems
    $redirect = preg_replace('/&amp;/', '&', $redirect);

    header("Location: " . $redirect);

    die();
}

require_once(CHECKOUT_WS_LIB . 'checkout.class.php');

$shipping_enabled = false;

if (($cart->show_weight() > 0 || !CHECKOUT_DYNAMIC_SHIPPING) && strpos($cart->content_type, 'virtual') === false) {
    $shipping_enabled = true;
}

@error_reporting(E_ALL ^ E_NOTICE);
@ini_set('error_reporting', E_ALL ^ E_NOTICE);
@ini_set('display_errors', '1');

$checkout = new checkout;

set_error_handler(array($checkout, 'error_handler'));

if (CHECKOUT_CONTRIB_STS === true) {
    ob_end_clean();
}

include(DIR_WS_LANGUAGES . $language . '/checkout_process.php');
if (file_exists(CHECKOUT_WS_INCLUDES . 'languages/' . $language . '/checkout.php')) {
    require_once(CHECKOUT_WS_INCLUDES . 'languages/' . $language . '/checkout.php');
} else {
    require_once(CHECKOUT_WS_INCLUDES . 'languages/english/checkout.php');
}
require_once(DIR_WS_CLASSES . 'order.php');
require_once(DIR_WS_CLASSES . 'order_total.php');
require_once(DIR_WS_CLASSES . 'shipping.php');
require_once(DIR_WS_CLASSES . 'payment.php');

$email_data = array();
$email_data = array();

    $email_data['STORE_NAME'] = STORE_NAME;
    $email_data['STORE_OWNER_EMAIL_ADDRESS'] = STORE_OWNER_EMAIL_ADDRESS;
    $email_data['NAME'] = 'huyen';
    $email_data['EMAIL_ADDRESS'] = 'thuhuyen1142@gmail.com';
    $email_data['EMAIL_RECIPIENT'] = 'thuhuyen1142@gmail.com';
    $email_data['ORDER_ID'] = '9802';
    $email_data['INVOICE_URL'] = '';
    $email_data['DATE_LONG'] = strftime(DATE_FORMAT_LONG);
    $email_data['DATE_SHORT'] = date(NEW_DATE_FORMAT_SHORT);
    $email_data['TELEPHONE'] = '8788787';
    $email_data['COMMENTS'] = 'test';
    $email_data['PRODUCTS'] = '<li>
                                <span style="color: #1b1b1b; font-style: italic;">
                                    1x 30,000K Xenon HID Kit Single Beam
                                </span>
                                <ul style="padding: 4px 0 5px 5px;">
                                    <li style="color: #b2b1b1;font-style: italic;list-style: none;">- Select Rulb Size 880                                       
                                    </li>
                                    <li style="color: #b2b1b1;font-style: italic;list-style: none;">- Year Make and Model Of Your                                       
                                    </li>
                                </ul>                 
                            </li>
                            <li>
                                <hr style="background-color: #f2f2f2; height: 1px; border: 0;">
                            </li>
                            <li>
                                <span style="color: #1b1b1b; font-style: italic;">
                                    1x 30,000K Xenon HID Kit Single Beam
                                </span>
                                <ul style="padding: 4px 0 5px 5px;">
                                    <li style="color: #b2b1b1;font-style: italic; list-style: none;">- Select Rulb Size 880                                       
                                    </li>
                                    <li style="color: #b2b1b1;font-style: italic;list-style: none;">- Year Make and Model Of Your                                       
                                    </li>
                                </ul>                 
                            </li>
                            <li>
                                <hr style="background-color: #f2f2f2; height: 1px; border: 0;">
                            </li>
                            <li>
                                <span style="color: #1b1b1b; font-style: italic;">
                                    1x 30,000K Xenon HID Kit Single Beam
                                </span>
                                <ul style="padding: 4px 0 5px 5px;">
                                    <li style="color: #b2b1b1;font-style: italic; list-style: none;">- Select Rulb Size 880                                       
                                    </li>
                                    <li style="color: #b2b1b1;font-style: italic;list-style: none;">- Year Make and Model Of Your                                       
                                    </li>
                                </ul>                 
                            </li>
                            <li>
                                <hr style="background-color: #f2f2f2; height: 1px; border: 0;">
                            </li>';
    $email_data['SENDER_NAME'] = STORE_OWNER;
    //huyen edit mail
    $email_data['SENDER_EMAIL'] = 'orders@motorfiend.com';
    $email_data['DELIVERY_ADDRESS']='uduh475847548';
    $email_data['SHIPPING_COST'] = '$4.95';
    $email_data['SHIPPING_METHOD'] = 'Anticipated Arrival 8-16 Days';
    //$email_data['SUB_TOTAL'] = $currencies->format($cart->show_total());
    $email_data['SUB_TOTAL'] ='$5';
    $email_data['TOTAL'] = '$4.95';
    $email_data['DISCOUNT'] = '$5';
    $order_mail=cya_get_email('CUSTOMER_ORDER');
    $email_data['SUBJECT'] = ($order_mail['email_header']!='')?$order_mail['email_header']:EMAIL_TEXT_SUBJECT;
    
    if($order_mail['email_content']!='')
            $checkout->send_email('', $email_data,$order_mail['email_content']);
        else
            $checkout->send_email('invoice', $email_data,'');
?>
