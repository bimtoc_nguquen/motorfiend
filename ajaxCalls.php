<?php
  require('includes/application_top.php');

switch ($_REQUEST['call-type']){

  //====================== PRODUCT REVIEWS for Product Info tab ==================
  case 'product-reviews':
        $product_info['products_id'] = $_REQUEST['products_id'];
        // include the language translations
        require(DIR_WS_LANGUAGES . $language . '/'.FILENAME_PRODUCT_INFO);
        $reviews_query_raw = "select r.reviews_id, left(rd.reviews_text, 100) as reviews_text, r.reviews_rating, r.date_added, r.customers_id, r.customers_name from " . TABLE_REVIEWS . " r, " . TABLE_REVIEWS_DESCRIPTION . " rd where r.products_id = '" . (int)$product_info['products_id'] . "' and r.reviews_id = rd.reviews_id and rd.languages_id = '" . (int)$languages_id . "' and r.reviews_status = 1 order by r.reviews_id desc";
            $reviews_split = new splitPageResults($reviews_query_raw, MAX_DISPLAY_NEW_REVIEWS);
            if ($reviews_split->number_of_rows > 0) {
               $reviews_query = tep_db_query($reviews_split->sql_query);
            ?>
         <table width="100%" class="reviews-tab-review" cellspacing="0">
           <tr>
            <th style="text-align:left">Customer Reviews</th>
            <th style="text-align: right">Viewing <?php echo tep_db_num_rows($reviews_query); ?> of <?php echo $reviews_split->number_of_rows; ?> Review<?php echo ($reviews_split->number_of_rows != 1 ? "s" : "");?></th>
           </tr>
         <?php
               while ($reviews = tep_db_fetch_array($reviews_query)) {
         ?>
           <tr>
            <td class="reviews-tab-review-info">
             <h2><?php echo '<a href="' . tep_href_link(FILENAME_PRODUCT_REVIEWS_INFO, 'products_id=' . $product_info['products_id'] . '&reviews_id=' . $reviews['reviews_id']) . '">' . sprintf(TEXT_REVIEW_BY, tep_output_string_protected(initial_last($reviews['customers_name']))) . '</a>'; ?></h2>
             <div class="motogrey"><?php echo customers_location($reviews['customers_id']); ?></div>
             <div style="color: #ffba10"><?php echo tep_image(DIR_WS_IMAGES . 'stars_' . $reviews['reviews_rating'] . '.gif', sprintf(TEXT_OF_5_STARS, $reviews['reviews_rating'])) . sprintf(TEXT_OF_5_STARS, $reviews['reviews_rating']); ?></div>
           </td>
           <td class="reviews-tab-review-review">
             <div class="review-tab-lquote"></div>
             <div class="contentText">
              <?php echo tep_break_string(tep_output_string_protected($reviews['reviews_text']), 60, '-<br />') . ((strlen($reviews['reviews_text']) >= 100) ? '..' : ''); ?>
             </div>
             <div class="review-tab-rquote"></div>
           </td>
          </tr>
          <tr><td colspan="2" style="height: 10px"> </td></tr>
         <?php
            }
            echo '         </table>';
            if ( ($reviews_split->number_of_rows > 0) && ($reviews_split->number_of_pages > 1) && ((PREV_NEXT_BAR_LOCATION == '2') || (PREV_NEXT_BAR_LOCATION == '3')) ) { ?>
        <span class="product-info-pagination-reviews"><?php echo TEXT_RESULT_PAGE . ' ' . $reviews_split->display_links(MAX_DISPLAY_PAGE_LINKS, tep_get_all_get_params(array('page', 'info', 'x', 'y'))); ?></span>
        <script type="text/javascript">
          $(document).ready(function(){
            $(".product-info-pagination-reviews a").each(function(){
              $(this).click(function(){
                  var qstr = parseUri($(this).attr('href')).query;
                  $.ajax({
                    url: 'ajaxCalls.php?'+qstr,
                    type: 'post',
                    data: {'call-type':'product-reviews','products_id':<?php echo $product_info['products_id'];?>},
                    success: function(html){
                      $("#product-info-tab-reviews").html(html);
                    }
                  });
                  return false;
              });
            });
          });

// parseUri 1.2.2
// (c) Steven Levithan <stevenlevithan.com>
// MIT License
function parseUri (str) {
    var    o   = parseUri.options,
        m   = o.parser[o.strictMode ? "strict" : "loose"].exec(str),
        uri = {},
        i   = 14;

    while (i--) uri[o.key[i]] = m[i] || "";

    uri[o.q.name] = {};
    uri[o.key[12]].replace(o.q.parser, function ($0, $1, $2) {
        if ($1) uri[o.q.name][$1] = $2;
    });

    return uri;
};

parseUri.options = {
    strictMode: false,
    key: ["source","protocol","authority","userInfo","user","password","host","port","relative","path","directory","file","query","anchor"],
    q:   {
        name:   "queryKey",
        parser: /(?:^|&)([^&=]*)=?([^&]*)/g
    },
    parser: {
        strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
        loose:  /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/
    }
};
        </script>
      <?php }

          } else {
         ?>
            <div class="contentText content-box text-no-reviews">
              <?php echo TEXT_NO_REVIEWS; ?>
            </div>
         <?php
          }
     exit();
   break;

}
?>