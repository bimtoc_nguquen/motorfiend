<?php
/*
  $Id: checkout_success.php 1749 2007-12-21 04:23:36Z hpdl $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2007 osCommerce

  Released under the GNU General Public License
*/

  require('includes/application_top.php');

// if the customer is not logged on, redirect them to the shopping cart page
  //if (!tep_session_is_registered('customer_id')) {
    //tep_redirect(tep_href_link(FILENAME_SHOPPING_CART));
  //}

  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_CHECKOUT_SUCCESS);

  $breadcrumb->add(NAVBAR_TITLE_1);
  //$breadcrumb->add(NAVBAR_TITLE_2);
  //$breadcrumb->add('Pay Now');
  
if (isset($_SESSION['payment']) && ($_SESSION['payment'] == 'globebill')){
	// load the selected order module
	require(DIR_WS_CLASSES . 'order.php');
	$order = new order;
	
	// load the selected payment module
  require(DIR_WS_CLASSES . 'payment.php');
  $payment_modules = new payment($_SESSION['payment']);
  $payment_modules->update_status();
	if (is_array($payment_modules->modules)) 
	{
		$payment_modules->pre_confirmation_check();
	}	
	$orders_id = $_SESSION['order_number_created'];
}
else{	
  if (isset($HTTP_GET_VARS['action']) && ($HTTP_GET_VARS['action'] == 'update')) {
    $notify_string = '';

    if (isset($HTTP_POST_VARS['notify']) && !empty($HTTP_POST_VARS['notify'])) {
      $notify = $HTTP_POST_VARS['notify'];

      if (!is_array($notify)) {
        $notify = array($notify);
      }

      for ($i=0, $n=sizeof($notify); $i<$n; $i++) {
        if (is_numeric($notify[$i])) {
          $notify_string .= 'notify[]=' . $notify[$i] . '&';
        }
      }

      if (!empty($notify_string)) {
        $notify_string = 'action=notify&' . substr($notify_string, 0, -1);
      }
    }

    tep_redirect(tep_href_link(FILENAME_DEFAULT, $notify_string));
  }

//  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_CHECKOUT_SUCCESS);
//
//  $breadcrumb->add(NAVBAR_TITLE_1);
//  $breadcrumb->add(NAVBAR_TITLE_2);

  $global_query = tep_db_query("select global_product_notifications from " . TABLE_CUSTOMERS_INFO . " where customers_info_id = '" . (int)$customer_id . "'");
  $global = tep_db_fetch_array($global_query);

  if ($global['global_product_notifications'] != '1') {
    	//HungNM new update
		$orders_query = tep_db_query("select * from " . TABLE_ORDERS . " where customers_id = '" . (int) $customer_id . "' order by date_purchased desc limit 1");
        $orders = tep_db_fetch_array($orders_query);

        $cya_order_id = (int) $orders['orders_id'];
        $orders_total_query = tep_db_query("select value from " . TABLE_ORDERS_TOTAL . " where orders_id = '" . (int) $orders['orders_id'] . "' and class = 'ot_total'");
        $orders_total = tep_db_fetch_array($orders_total_query);

        $orders_total_shipping_query = tep_db_query("select value from " . TABLE_ORDERS_TOTAL . " where orders_id = '" . (int) $orders['orders_id'] . "' and class = 'ot_shipping'");
        $orders_total_shipping = tep_db_fetch_array($orders_total_shipping_query);


        $products_array = array();
        $products_query = tep_db_query("select * from " . TABLE_ORDERS_PRODUCTS . " where orders_id = '" . (int) $orders['orders_id'] . "' order by products_name");
        while ($products = tep_db_fetch_array($products_query)) {
            $products_array[] = array('id' => $products['products_id'],
                'text' => $products['products_name'],
                'products_model' => $products['products_model'],
                'final_price' => $products['final_price'],
                'products_quantity' => $products['products_quantity']);
        }
		//end of new update
  }
}

  require(DIR_WS_INCLUDES . 'template_top.php');

if (isset($_SESSION['payment']) && ($_SESSION['payment'] == 'globebill')){
?>
    	<?php

		$parems = 'id="checkout_confirmation" target="iframe1"'; 

		echo tep_draw_form('checkout_confirmation', MODULE_PAYMENT_GLOBEBILL_HANDLER, 'post',$parems);
    	if (is_array($payment_modules->modules)) {
    		echo $payment_modules->process_button();
  		}
  		?>
<!--<h1>Step 4:<?php echo "Pay Now"; ?></h1>-->
    	<table border="0" width="100%" cellspacing="0" cellpadding="0">
      <tr>
        <td><table border="0" width="100%" cellspacing="4" cellpadding="2">
          <tr>
            <td valign="top">
            	<?php //echo tep_image(DIR_WS_IMAGES . 'table_background_man_on_board.gif', HEADING_TITLE); ?></td>
            <td valign="top" class="main">
            	<?php //echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?>
            	<div align="center" class="pageHeading">
            		<?php //echo HEADING_TITLE; ?></div>
<?php
    //echo TEXT_SEE_ORDERS . '<br><br>' . TEXT_CONTACT_STORE_OWNER;
?>
            </td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
      </tr>
      <tr>
        <td align="center" class="main">
        	<input type="image" src="<?php echo MODULE_PAYMENT_GLOBEBILL_PAY_BUTTON_IMG; ?>" border="0" alt="Goto Pay Now" title=" Goto Pay " style="display: none;"></td>
      </tr>
      <tr>
        <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
      </tr>
      <tr>
        <td></td>
      </tr>
    </table></form>
	<iframe id="iframe1" name="iframe1" frameborder="0" scrolling="no" width="100%" height="1000"  ></iframe>
<script type="text/javascript">
	function form_ready(){ 
		if(document.getElementById("checkout_confirmation")) {	  
			 document.getElementById("checkout_confirmation").submit(); 
		} else {
			setTimeout('form_ready()',50);
		}
	}
	form_ready();			
</script>
<!--<table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td width="25%"><table border="0" width="100%" cellspacing="0" cellpadding="0">
              <tr>
                <td width="50%" align="right"><?php echo tep_draw_separator('pixel_silver.gif', '1', '5'); ?></td>
                <td width="50%"><?php echo tep_draw_separator('pixel_silver.gif', '100%', '1'); ?></td>
              </tr>
            </table></td>
            <td width="25%"><?php echo tep_draw_separator('pixel_silver.gif', '100%', '1'); ?></td>
            <td width="25%"><?php echo tep_draw_separator('pixel_silver.gif', '100%', '1'); ?></td>
            <td width="25%"><table border="0" width="100%" cellspacing="0" cellpadding="0">
              <tr>
                <td width="50%"><?php echo tep_draw_separator('pixel_silver.gif', '100%', '1'); ?></td>
                <td width="50%"><?php echo tep_image(DIR_WS_IMAGES . 'checkout_bullet.gif'); ?></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td align="center" width="25%" class="checkoutBarFrom"><?php echo CHECKOUT_BAR_DELIVERY; ?></td>
            <td align="center" width="25%" class="checkoutBarFrom"><?php echo CHECKOUT_BAR_PAYMENT; ?></td>
            <td align="center" width="25%" class="checkoutBarFrom"><?php echo CHECKOUT_BAR_CONFIRMATION; ?></td>
            <td align="center" width="25%" class="checkoutBarCurrent"><?php echo CHECKOUT_BAR_FINISHED; ?></td>
          </tr>
        </table>-->
	</td>
<?php
}
else {
?>
    	<?php echo tep_draw_form('order', tep_href_link(FILENAME_CHECKOUT_SUCCESS, 'action=update', 'SSL')); ?>
    	<table border="0" width="100%" cellspacing="0" cellpadding="0">
      <tr>
        <td><table border="0" width="100%" cellspacing="4" cellpadding="2">
          <tr>
            <td valign="top"><?php echo tep_image(DIR_WS_IMAGES . 'table_background_man_on_board.gif', HEADING_TITLE); ?></td>
            <td valign="top" class="main">
            	<?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?>
            	<div align="center" class="pageHeading">
            		<?php echo HEADING_TITLE; ?><?php echo $_SESSION['payment']; ?></div><br><?php echo TEXT_SUCCESS; ?><br><br>
<?php
  if ($global['global_product_notifications'] != '1') {
    echo TEXT_NOTIFY_PRODUCTS . '<br><p class="productsNotifications">';

    $products_displayed = array();
    for ($i=0, $n=sizeof($products_array); $i<$n; $i++) {
      if (!in_array($products_array[$i]['id'], $products_displayed)) {
        echo tep_draw_checkbox_field('notify[]', $products_array[$i]['id']) . ' ' . $products_array[$i]['text'] . '<br>';
        $products_displayed[] = $products_array[$i]['id'];
      }
    }

    echo '</p>';
?>
                                <script type="text/javascript">
                                    var gaJsHost = (("https:" == document.location.protocol ) ? "https://ssl." : "http://www.");
                                    document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
                                </script>
                                <?php
                                $gooogle_tracker = '<script type="text/javascript">
                                try{
                                  var pageTracker = _gat._getTracker("UA-12174222-5");
                                  pageTracker._trackPageview();
                                  pageTracker._addTrans(
                                      "' . $cya_order_id . '",            // order ID - required
                                      "' . STORE_NAME . '",  // affiliation or store name
                                      "' . $orders_total['value'] . '",           // total - required
                                      "0",            // tax
                                      "' . $orders_total_shipping['value'] . '",           // shipping
                                      "' . $orders['customers_city'] . '",        // city
                                      "' . $orders['customers_state'] . '",      // state or province
                                      "' . $orders['customers_country'] . '"              // country
                                    );';


                                // add item might be called for every item in the shopping cart
                                // where your ecommerce engine loops through each item in the cart and
                                // prints out _addItem for each 
                                foreach ($products_array as $product) {
                                    $gooogle_tracker.= 'pageTracker._addItem(
                                      "' . $cya_order_id . '",           // order ID - necessary to associate item with transaction
                                      "' . $product['products_model'] . '",           // SKU/code - required
                                      "' . $product['text'] . '",        // product name
                                      "' . STORE_NAME . '",   // category or variation
                                      "' . $product['final_price'] . '",          // unit price - required
                                      "' . $product['products_quantity'] . '"               // quantity - required
                                   );';
                                }


                                $gooogle_tracker.= 'pageTracker._trackTrans(); //submits transaction to the Analytics servers
                                } catch(err) {}
                                </script>';
                                echo $gooogle_tracker;
  } else {
    echo TEXT_SEE_ORDERS . '<br><br>' . TEXT_CONTACT_STORE_OWNER;
  }
?>
            <h3><?php echo TEXT_THANKS_FOR_SHOPPING; ?></h3></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
      </tr>
      <tr>
        <td align="right" class="main"><?php echo tep_image_submit('button_continue.gif', IMAGE_BUTTON_CONTINUE); ?></td>
      </tr>
      <tr>
        <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
      </tr>
      <!--<tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td width="25%"><table border="0" width="100%" cellspacing="0" cellpadding="0">
              <tr>
                <td width="50%" align="right"><?php echo tep_draw_separator('pixel_silver.gif', '1', '5'); ?></td>
                <td width="50%"><?php echo tep_draw_separator('pixel_silver.gif', '100%', '1'); ?></td>
              </tr>
            </table></td>
            <td width="25%"><?php echo tep_draw_separator('pixel_silver.gif', '100%', '1'); ?></td>
            <td width="25%"><?php echo tep_draw_separator('pixel_silver.gif', '100%', '1'); ?></td>
            <td width="25%"><table border="0" width="100%" cellspacing="0" cellpadding="0">
              <tr>
                <td width="50%"><?php echo tep_draw_separator('pixel_silver.gif', '100%', '1'); ?></td>
                <td width="50%"><?php echo tep_image(DIR_WS_IMAGES . 'checkout_bullet.gif'); ?></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td align="center" width="25%" class="checkoutBarFrom"><?php echo CHECKOUT_BAR_DELIVERY; ?></td>
            <td align="center" width="25%" class="checkoutBarFrom"><?php echo CHECKOUT_BAR_PAYMENT; ?></td>
            <td align="center" width="25%" class="checkoutBarFrom"><?php echo CHECKOUT_BAR_CONFIRMATION; ?></td>
            <td align="center" width="25%" class="checkoutBarCurrent"><?php echo CHECKOUT_BAR_FINISHED; ?></td>
          </tr>
        </table></td>
      </tr>-->
<?php if (DOWNLOAD_ENABLED == 'true') include(DIR_WS_MODULES . 'downloads.php'); ?>
    </table></form>
<?php 
} 
?>
<?php
  require(DIR_WS_INCLUDES . 'template_bottom.php');
  require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
<!-- Google Code for Sale Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1053573984;
var google_conversion_language = "en";
var google_conversion_format = "2";
var google_conversion_color = "ffffff";
var google_conversion_label = "BhA4CLSx5gIQ4Iax9gM";
var google_conversion_value = 0;
/* ]]> */
</script>
<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/1053573984/?label=BhA4CLSx5gIQ4Iax9gM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
